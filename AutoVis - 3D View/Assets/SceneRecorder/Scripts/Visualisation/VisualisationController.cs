using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Replay;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine.Networking;



namespace Replay
{
    /// <summary>
    /// Class that handles all Visualisations
    /// </summary>
    public class VisualisationController : MonoBehaviour
    {

        public static VisualisationController instanceJS;
        public GameObject Line;
        /// <summary>
        /// GamObject of the Cesium Map
        /// </summary>
        public GameObject CesiumMap;

        ReplayManager compScenePlayer;

        /// <summary>
        /// Reference to the Model of the Avatars
        /// </summary>
        public GameObject avatarmodel;

        /// <summary>
        /// List of all Avatars created
        /// </summary>
        /// <param name="AvatarList"></param>
        /// <typeparam name="(bool"> Boolean to see/handle if Avatar active</typeparam>
        /// <typeparam name="GameObject)"> Gamobject of the Avatar </typeparam>
        /// <returns></returns>
        public List<(bool, GameObject)> AvatarList = new List<(bool, GameObject)>();


        public GameObject teslaModel;

        public GameObject PassengerModel;

        public GameObject PassengerController;

        public RigControl rigcontrol;


        public MapBuilderCustom mapBuilderCustom;

        public List<GameObject> bonesTest;

        public List<Material> AvatarMaterials;

        public Material mergedAvatarMaterial;

        public Heatmap heatmapExterior;

        public int focusedId = 0;

        public int numberOfParticipants = 3;

        private StreamReader reader;

        /// <summary>
        /// List of all Interior Heatmaps of all Participants
        /// </summary>
        /// <typeparam name="InteriorHeatmaps"></typeparam>
        /// <returns></returns>
        private List<InteriorHeatmaps> interiorHeatmaps = new List<InteriorHeatmaps>();

        /// <summary>
        /// List of all Interior Touch Heatmaps of all Participants
        /// </summary>
        /// <typeparam name="TouchHeatmaps"></typeparam>
        /// <returns></returns>
        private List<TouchHeatmaps> interiorTouchHeatmaps = new List<TouchHeatmaps>();

        /// <summary>
        /// List of all Trajectories of all Participants
        /// </summary>
        /// <typeparam name="Trajectories"></typeparam>
        /// <returns></returns>
        private List<Trajectories> TrajectoriesAll = new List<Trajectories>();

        /// <summary>
        /// List of all Buildings that have Heatmaps
        /// </summary>
        public List<GameObject> exteriorBuildings;

        /// <summary>
        /// List of all Bone Positions inside a List of all Bones in each Frame inside a List of all Participants
        /// </summary>
        /// <returns></returns>
        public List<List<List<Vector3>>> AvatarBonePositionParticipant = new List<List<List<Vector3>>>();

        /// <summary>
        /// List of a bool to handle the Active state of the Passenger and a Vector3 for the positions inside a List for each Frame
        /// </summary>
        /// <param name="PassengerBonePositionParticipant"></param>
        /// <returns></returns>
        public List<List<(bool, List<Vector3>)>> PassengerBonePositionParticipant = new List<List<(bool, List<Vector3>)>>();

        /// <summary>
        /// List of all Bone Positions inside a List of all Bones in each Frame inside a List of all Passengers
        /// </summary>
        /// <returns></returns>
        public List<List<List<Vector3>>> FinalPassengerBonePositionParticipant = new List<List<List<Vector3>>>();

        // Obsolete
        // /// <summary>
        // /// Heatmap of Participant to visualize nearby Vehicles
        // /// </summary>
        // public Heatmap radiusHeatmap;
        // /// <summary>
        // /// Heatmap of Participant to visualize nearby Vehicles
        // /// </summary>
        // public Heatmap radiusHeatmap1;
        // /// <summary>
        // /// Heatmap of Participant to visualize nearby Vehicles
        // /// </summary>
        // public Heatmap radiusHeatmap2;

        public Material basicMaterialOfHeatmap;

        /// <summary>
        /// The Container for all Radius Heatmaps around the Vehicle
        /// </summary>
        public GameObject RadiusContainer;


        /// <summary>
        /// The Prefab for the Radius Heatmap;
        /// </summary>
        public GameObject radiusPrefab;

        /// <summary>
        /// List of all Heatmaps under Main Vehicle to visualize nearby Vehicles
        /// </summary>
        private List<Heatmap> radiuses = new List<Heatmap>();

        /// <summary>
        /// Reference to the Main Vehicle of the Participant
        /// </summary>
        public GameObject mainCar;

        /// <summary>
        /// List of all Positions of the Main Car
        /// </summary>
        public List<Vector3> mainCarPositions;

        /// <summary>
        /// Reference of Heatmap Prefab of Interior Gaze Heatmaps
        /// </summary>
        public GameObject HeatmapInterior;
        /// <summary>
        /// Reference of Heatmap Prefab of Interior Touch Heatmaps
        /// </summary>
        public GameObject HeatmapTouch;

        /// <summary>
        /// Reference of Trajectory Prefab
        /// </summary>
        public GameObject Trajectory;


        /// <summary>
        /// List of Positions of other Vehicles of all Participants in Each Frame
        /// </summary>
        /// <returns></returns>
        public List<List<List<Vector4>>> radiusCar = new List<List<List<Vector4>>>();

        /// <summary>
        /// Rotation of the first Frame of the main Vehicle 
        /// </summary>
        public Vector3 startRotation;

        /// <summary>
        /// Reference to the currently selected Participant
        /// </summary>
        public int selectedParticipant;

        /// <summary>
        /// Reference to the Color Picker GameObject
        /// </summary>
        public GameObject ColorPicker;

        /// <summary>
        /// List of all Gaze Positions
        /// </summary>
        /// <returns></returns>
        public List<List<Vector3>> gazePoints = new List<List<Vector3>>();


        [Obsolete]
        /// <summary>
        /// Reference to Plane to handle Height of Cesium Map
        /// </summary>
        public GameObject plane;


        private void Awake()
        {
            compScenePlayer = this.GetComponent<ReplayManager>();
            instanceJS = this;
        }


        public List<GameObject> Avatar;
        // Start is called before the first frame update
        void Start()
        {


        }

        /// <summary>
        /// Loads Neares Frame from Timestamp of type string
        /// </summary>
        /// <param name="stamp">Timestamp with type string</param>
        public void jumptoFrame(string stamp)
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            //Destroy(compScenePlayer.ReplayContainer); 
            // compScenePlayer.AllWorldObjectsInFrame.Clear();
            double time = Convert.ToDouble(stamp, provider);
            compScenePlayer.GoToNearestTimeStamp(time);
        }




        [Obsolete("See ModelManager.ToggleHighlightModelType(string key)")]
        public void OutlineAllType(string type)
        {
            List<GameObject> rootObjects = new List<GameObject>();
            Scene scene = SceneManager.GetActiveScene();
            scene.GetRootGameObjects(rootObjects);

            for (int i = 0; i < rootObjects.Count; i++)
            {
                if (rootObjects[i].GetComponent<ModelController>() != null)
                {
                    rootObjects[i].transform.GetComponent<Outline>().enabled = false;
                }
            }
            for (int i = 0; i < rootObjects.Count; i++)
            {
                if (rootObjects[i].GetComponent<ModelController>() != null)
                {
                    if (type == rootObjects[i].GetComponent<ModelController>().Type)
                    {
                        Debug.Log("test");
                        rootObjects[i].transform.GetComponent<Outline>().enabled = true;
                    }
                }
                //Debug.Log(rootObjects[i]);
            }

        }

        [Obsolete("See ModelManager.HighlightModel(string key)")]
        public void Outlinesingle(string key)
        {
            List<GameObject> rootObjects = new List<GameObject>();
            Scene scene = SceneManager.GetActiveScene();
            scene.GetRootGameObjects(rootObjects);

            for (int i = 0; i < rootObjects.Count; i++)
            {
                if (rootObjects[i].GetComponent<ModelController>() != null)
                {
                    rootObjects[i].transform.GetComponent<Outline>().enabled = false;
                }
            }
            for (int i = 0; i < rootObjects.Count; i++)
            {
                if (rootObjects[i].GetComponent<ModelController>() != null)
                {

                    if (key == rootObjects[i].GetComponent<ModelController>().Key)
                    {

                        rootObjects[i].transform.GetComponent<Outline>().enabled = true;
                    }
                }
                //Debug.Log(rootObjects[i]);
            }

        }


        /// <summary>
        /// Initializes a Gaze Heatmap
        /// </summary>
        /// <param name="points">List of tuple with type of gaze heatmap and position</param>
        /// <param name="participantId">id of participant</param>
        public void InitInteriorHeatmap(List<(string, Vector4)> points, int participantId)
        {
            GameObject Heatmaps = Instantiate(HeatmapInterior);
            Heatmaps.transform.position = mainCar.transform.position;
            Heatmaps.transform.rotation = mainCar.transform.rotation;
            Heatmaps.transform.parent = mainCar.transform;
            InteriorHeatmaps thisHeatmap = Heatmaps.GetComponent<InteriorHeatmaps>();
            thisHeatmap.SetupTextures(participantId, points);
            interiorHeatmaps.Add(thisHeatmap);
        }

        /// <summary>
        /// Draws all Gaze Heatmaps
        /// </summary>
        /// <param name="index">Frame index</param>
        public void DrawHeatmaps(int index)
        {
            //heatmaps[0].drawHeatmap(index);
            //heatmaps[1].drawHeatmap(index);
            //heatmaps[2].drawHeatmap(index);

            for (int i = 0; i < interiorHeatmaps.Count; i++)
            {
                interiorHeatmaps[i].DrawInteriorHeatmap(index);
            }
            //mapBuilderCustom.DrawPointingHeatmap(index);
        }

        /// <summary>
        /// Initializes a Touch Heatmap
        /// </summary>
        /// <param name="points">List of tuple with type of touch heatmap and position</param>
        /// <param name="participantId">id of participant</param>
        public void InitTouchInteriorHeatmap(List<(string, Vector4)> points, int participantId)
        {
            GameObject Heatmaps = Instantiate(HeatmapTouch);
            Heatmaps.transform.position = mainCar.transform.position;
            Heatmaps.transform.rotation = mainCar.transform.rotation;
            Heatmaps.transform.parent = mainCar.transform;
            TouchHeatmaps thisHeatmap = Heatmaps.GetComponent<TouchHeatmaps>();
            thisHeatmap.SetupTextures(participantId, points);
            interiorTouchHeatmaps.Add(thisHeatmap);
        }
        /// <summary>
        /// Draws all Touch Heatmaps
        /// </summary>
        /// <param name="index">Frame index</param>
        public void DrawTouchHeatmaps(int index)
        {
            //heatmaps[0].drawHeatmap(index);
            //heatmaps[1].drawHeatmap(index);
            //heatmaps[2].drawHeatmap(index);
            for (int i = 0; i < interiorTouchHeatmaps.Count; i++)
            {
                //                Debug.Log(i + "HIER");
                interiorTouchHeatmaps[i].DrawTouchInteriorHeatmap(index);
            }
            // mapBuilderCustom.DrawPointingHeatmap(index);
        }

        /// <summary>
        /// Initializes heatmaps for exterior buildings
        /// </summary>
        /// <param name="points">List of all gaze hit positions</param>
        /// <param name="participantId">id of participant </param>
        public void InitExteriorBuildingHeatmap(List<Vector4> points, int participantId)
        {
            mapBuilderCustom.SetupTextures(points);
            // GameObject Heatmaps = Instantiate(HeatmapInterior);
            // Heatmaps.transform.position = mainCar.transform.position;
            // Heatmaps.transform.rotation = mainCar.transform.rotation;
            // Heatmaps.transform.parent = mainCar.transform;
            // InteriorHeatmaps thisHeatmap = Heatmaps.GetComponent<InteriorHeatmaps>();
            // thisHeatmap.SetupTextures(participantId, points);
            // interiorHeatmaps.Add(thisHeatmap);


        }
        /// <summary>
        /// Initializes avatar of participant
        /// </summary>
        /// <param name="ParticipantAvatarList">List of List of all bone positions for each frame</param>
        /// <param name="partIndex">id of participant</param>
        public void InitAvatar(List<List<Vector3>> ParticipantAvatarList, int partIndex)
        {
            GameObject avatar = Instantiate(avatarmodel);
            Vector3 posAvatar = avatar.transform.localPosition;
            avatar.transform.parent = mainCar.transform;
            avatar.transform.rotation = mainCar.transform.rotation;
            avatar.transform.localPosition = posAvatar;


            AvatarBonePositionParticipant.Add(ParticipantAvatarList);
            avatar.GetComponent<RigControl>().mainCar = mainCar;
            avatar.GetComponent<RigControl>().AvatarModel = Avatar;
            avatar.GetComponent<PortalHandler>().SetOutlineColor(AvatarMaterials[partIndex].color);
            //Debug.Log("DONE" + Avatar);

            avatar.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material = AvatarMaterials[partIndex];
            AvatarList.Add((true, avatar));
        }


        /// <summary>
        /// Initializes merged avatar out of all participants
        /// </summary>
        public void InitMergedAvatar()
        {
            GameObject avatar = Instantiate(avatarmodel);
            Vector3 posAvatar = avatar.transform.localPosition;
            avatar.transform.parent = mainCar.transform;
            avatar.transform.rotation = mainCar.transform.rotation;
            avatar.transform.localPosition = posAvatar;

            AvatarList.Add((true, avatar));

            avatar.GetComponent<RigControl>().mainCar = mainCar;
            avatar.GetComponent<RigControl>().AvatarModel = Avatar;

            avatar.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material = mergedAvatarMaterial;
            List<List<Vector3>> MergedPositions = new List<List<Vector3>>();
            int savetest = Math.Min(AvatarBonePositionParticipant[0].Count, AvatarBonePositionParticipant[1].Count);
            int numberOfBones = Math.Min(savetest, AvatarBonePositionParticipant[2].Count);
            for (int i = 0; i < numberOfBones; i++)
            {

                List<Vector3> bones = new List<Vector3>();
                for (int k = 0; k < AvatarBonePositionParticipant[0][0].Count; k++)
                {
                    Vector3 thisVec = new Vector3();

                    for (int j = 0; j < AvatarBonePositionParticipant.Count; j++)
                    {
                        thisVec += AvatarBonePositionParticipant[j][i][k];
                    }
                    Vector3 mergedVec = thisVec / AvatarBonePositionParticipant.Count;
                    bones.Add(mergedVec);

                }
                MergedPositions.Add(bones);

            }
            AvatarBonePositionParticipant.Add(MergedPositions);

        }

        /// <summary>
        /// Sets bone rotation of all avatars
        /// </summary>
        /// <param name="index">Frame index</param>
        public void DrawAvatarMovement(int index)
        {
            // Debug.Log(index);
            // RigControl avatar = AvatarList[0].GetComponent<RigControl>();
            // avatar.positionsBones = AvatarBonePositionParticipant[0][index];
            // avatar.setbonesAll();
            for (int i = 0; i < AvatarBonePositionParticipant.Count; i++)
            {
                if (AvatarList[i].Item1)
                {
                    RigControl avatar = AvatarList[i].Item2.GetComponent<RigControl>();
                    avatar.positionsBones = AvatarBonePositionParticipant[i][index];
                    avatar.setbonesAll();
                }

            }
        }
        /// <summary>
        /// Initializes merged passenger out of all participants
        /// </summary>
        public void InitMergedPassenger()
        {
            GameObject avatar = PassengerModel;
            Vector3 posAvatar = avatar.transform.localPosition;
            avatar.transform.parent = mainCar.transform;
            avatar.transform.rotation = mainCar.transform.rotation;
            avatar.transform.localPosition = posAvatar;


            avatar.GetComponent<RigControl>().mainCar = mainCar;

            avatar.GetComponent<RigControl>().AvatarModel = Avatar;
            PassengerController = avatar;

            //avatar.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material = AvatarMaterials[3];
            List<List<Vector3>> MergedPositions = new List<List<Vector3>>();
            int savetest = Math.Min(PassengerBonePositionParticipant[0].Count, PassengerBonePositionParticipant[1].Count);
            int numberOfBones = Math.Min(savetest, PassengerBonePositionParticipant[2].Count);
            for (int i = 0; i < numberOfBones; i++)
            {


                List<Vector3> bones = new List<Vector3>();
                for (int k = 0; k < PassengerBonePositionParticipant[0][0].Item2.Count; k++)
                {
                    Vector3 thisVec = new Vector3();
                    int counterForPassengerTimestamp = 0;
                    for (int j = 0; j < PassengerBonePositionParticipant.Count; j++)
                    {
                        if (PassengerBonePositionParticipant[j][i].Item1)
                        {
                            counterForPassengerTimestamp++;
                        }
                        thisVec += PassengerBonePositionParticipant[j][i].Item2[k];
                    }
                    Vector3 mergedVec = new Vector3();
                    if (counterForPassengerTimestamp == 0)
                    {
                        mergedVec = new Vector3(-1000f, -1000f, -1000f);
                    }
                    else
                    {
                        mergedVec = thisVec / counterForPassengerTimestamp;
                    }

                    bones.Add(mergedVec);

                }
                MergedPositions.Add(bones);

            }
            FinalPassengerBonePositionParticipant.Add(MergedPositions);

        }

        /// <summary>
        /// Sets bone rotation of passenger
        /// </summary>
        /// <param name="index">Frame index</param>
        public void DrawPassenger(int index)
        {

            RigControl avatar = PassengerModel.GetComponent<RigControl>();
            avatar.positionsBones = FinalPassengerBonePositionParticipant[0][index];
            avatar.setbonesAll();

        }
        /// <summary>
        /// Initializes Trajectories of each Participant
        /// </summary>
        /// <param name="participantId">id of participant</param>
        /// <param name="head">List of all positions of the head of the participant</param>
        /// <param name="leftHand">List of all positions of the left hand of the participant</param>
        /// <param name="rightHand">List of all positions of the right hand of the participant/param>
        public void InitTrajectories(int participantId, List<Vector3> head, List<Vector3> leftHand, List<Vector3> rightHand)
        {
            GameObject Trajectories = Instantiate(Trajectory);
            Trajectories.transform.position = mainCar.transform.position;
            Trajectories.transform.rotation = mainCar.transform.rotation;
            Trajectories.transform.parent = mainCar.transform;
            //Trajectories.transform.localPosition = new Vector3(0, 1f, 1.86399996f);
            Trajectories.transform.localPosition = new Vector3(0, 1.125f, 1.76999998f);
            Trajectories.transform.localRotation = new Quaternion(0f, 1f, 0f, 0f);
            Trajectories thisTrajectory = Trajectories.GetComponent<Trajectories>();
            thisTrajectory.SetupTrajectories(participantId, head, leftHand, rightHand);
            TrajectoriesAll.Add(thisTrajectory);
        }

        /// <summary>
        /// Sets all Trajectories List
        /// </summary>
        /// <param name="index">Frame index</param>
        public void DrawTrajectories(int index)
        {
            for (int i = 0; i < interiorHeatmaps.Count; i++)
            {
                TrajectoriesAll[i].DrawTrajectories(index);
            }
        }
        /// <summary>
        /// Adds all radius Gameobject to List
        /// </summary>
        public void InitRadiusus()
        {
            GameObject newRadius = Instantiate(radiusPrefab);
            newRadius.transform.parent = RadiusContainer.transform;
            newRadius.transform.localPosition = Vector3.zero;
            Heatmap radiusHeatmap = newRadius.GetComponent<Heatmap>();
            Material newRadiusHeatmapMaterial = Instantiate(basicMaterialOfHeatmap);
            // Vector4[] arr = new Vector4[200];
            // newRadiusHeatmapMaterial.SetInt("_Points_Length", arr.Length);
            // newRadiusHeatmapMaterial.SetVectorArray("_Points", arr);
            // newRadiusHeatmapMaterial.SetVectorArray("_Properties", arr);
            newRadius.GetComponent<MeshRenderer>().material = newRadiusHeatmapMaterial;
            radiusHeatmap.material = newRadiusHeatmapMaterial;


            radiuses.Add(radiusHeatmap);
            // radiuses.Add(radiusHeatmap);
            // radiuses.Add(radiusHeatmap1);
            // radiuses.Add(radiusHeatmap2);
        }

        /// <summary>
        /// Draws all heatmap points of other vehicles on heatmap
        /// </summary>
        /// <param name="index"></param>
        public void updateHeatmapCarRadius(int index)
        {
            // int NumberOfPoints = 100;
            // List<Vector4> newList = new List<Vector4>();
            // List<Vector4> propertiesForCar = new List<Vector4>();
            // for (int i = 0; i < radiusCar[index][0].Count; i++)
            // {
            //     Vector4 point = new Vector4(radiusCar[index][0][i].x, radiusHeatmap.gameObject.transform.position.y, radiusCar[index][0][i].z, radiusCar[index][0][i].w);
            //     //Vector4 newPos = (Vector4)RotatePointAroundPivot(point, radiusHeatmap.gameObject.transform.position, new Vector3(radiusHeatmap.gameObject.transform.rotation.eulerAngles.x, 0, radiusHeatmap.gameObject.transform.rotation.eulerAngles.z));
            //     newList.Add(point);

            //     propertiesForCar.Add(new Vector4(5f, 1f));
            // }
            // if (newList.Count > 0)
            // {
            //     Material material = radiusHeatmap.material;
            //     material.SetInt("_Points_Length", newList.ToArray().Length);
            //     material.SetVectorArray("_Points", newList.ToArray());
            //     material.SetVectorArray("_Properties", propertiesForCar.ToArray());
            // }
            // else
            // {
            //     Material material = radiusHeatmap.material;
            //     material.SetInt("_Points_Length", 0);

            // }
            // List<Vector4> newList1 = new List<Vector4>();
            // List<Vector4> propertiesForCar1 = new List<Vector4>();
            // for (int i = 0; i < radiusCar[index][1].Count; i++)
            // {
            //     Vector4 point = new Vector4(radiusCar[index][1][i].x, radiusHeatmap.gameObject.transform.position.y, radiusCar[index][1][i].z, radiusCar[index][1][i].w);
            //     //Vector4 newPos = (Vector4)RotatePointAroundPivot(point, radiusHeatmap.gameObject.transform.position, new Vector3(radiusHeatmap.gameObject.transform.rotation.eulerAngles.x, 0, radiusHeatmap.gameObject.transform.rotation.eulerAngles.z));
            //     newList1.Add(point);

            //     propertiesForCar1.Add(new Vector4(10f, 1f));
            // }
            // if (newList1.Count > 0)
            // {
            //     Material material = radiusHeatmap1.material;
            //     material.SetInt("_Points_Length", newList1.ToArray().Length);
            //     material.SetVectorArray("_Points", newList1.ToArray());
            //     material.SetVectorArray("_Properties", propertiesForCar1.ToArray());
            // }
            // else
            // {
            //     Material material = radiusHeatmap1.material;
            //     material.SetInt("_Points_Length", 0);

            // }
            // List<Vector4> newList2 = new List<Vector4>();
            // List<Vector4> propertiesForCar2 = new List<Vector4>();
            // for (int i = 0; i < radiusCar[index][2].Count; i++)
            // {
            //     Vector4 point = new Vector4(radiusCar[index][2][i].x, radiusHeatmap.gameObject.transform.position.y, radiusCar[index][2][i].z, radiusCar[index][2][i].w);
            //     //Vector4 newPos = (Vector4)RotatePointAroundPivot(point, radiusHeatmap.gameObject.transform.position, new Vector3(radiusHeatmap.gameObject.transform.rotation.eulerAngles.x, 0, radiusHeatmap.gameObject.transform.rotation.eulerAngles.z));
            //     newList2.Add(point);

            //     propertiesForCar2.Add(new Vector4(10f, 1f));
            // }
            // if (newList2.Count > 0)
            // {
            //     Material material = radiusHeatmap2.material;
            //     material.SetInt("_Points_Length", newList2.ToArray().Length);
            //     material.SetVectorArray("_Points", newList2.ToArray());
            //     material.SetVectorArray("_Properties", propertiesForCar2.ToArray());
            // }
            // else
            // {
            //     Material material = radiusHeatmap2.material;
            //     material.SetInt("_Points_Length", 0);

            // }
            for (int k = 0; k < numberOfParticipants; k++)
            {
                List<Vector4> newList = new List<Vector4>();
                List<Vector4> propertiesForCar = new List<Vector4>();
                for (int i = 0; i < radiusCar[index][k].Count; i++)
                {
                    Vector4 point = new Vector4(radiusCar[index][k][i].x, radiuses[k].gameObject.transform.position.y, radiusCar[index][k][i].z, radiusCar[index][k][i].w);
                    //Vector4 newPos = (Vector4)RotatePointAroundPivot(point, radiusHeatmap.gameObject.transform.position, new Vector3(radiusHeatmap.gameObject.transform.rotation.eulerAngles.x, 0, radiusHeatmap.gameObject.transform.rotation.eulerAngles.z));
                    newList.Add(point);

                    propertiesForCar.Add(new Vector4(5f, 1f));
                }
                if (newList.Count > 0)
                {
                    Material material = radiuses[k].material;
                    material.SetInt("_Points_Length", newList.ToArray().Length);
                    material.SetVectorArray("_Points", newList.ToArray());
                    material.SetVectorArray("_Properties", propertiesForCar.ToArray());
                }
                else
                {
                    Material material = radiuses[k].material;
                    material.SetInt("_Points_Length", 0);

                }
            }



        }



        /// <summary>
        /// Initializes a recording of other participants from a json string. Different Method that InitRecording in Replaymanager because no models needed for other participants
        /// </summary>
        /// <param name="s">already loaded json string </param>
        /// <param name="part">id of participant</param>
        /// <param name="webgl">bool to know if WebGL loaded or not</param>
        public void LoadOtherParticipantData(string s, int part, bool webgl)
        {
            string content;
            if (webgl)
            {
                content = s;
            }
            else
            {
                // reader = new StreamReader("./Recordings/" + s, Encoding.UTF8, false, 65536);

                // content = reader.ReadToEnd();

                //For Build
                TextAsset mytxtData = (TextAsset)Resources.Load("MyText");
                string filePath = "Recordings/" + s.Replace(".json", "");

                TextAsset targetFile = Resources.Load<TextAsset>(filePath);

                string txt = targetFile.text;
                content = txt;


            }

            //string content = s;
            JObject main = JObject.Parse(content);
            //ReplayManager.Instance.main = JObject.Parse(content);

            JEnumerable<JToken> tokens = main["snapshots"].Children();
            List<List<Vector4>> carPositions = new List<List<Vector4>>();
            List<Vector3> positionThisCar = new List<Vector3>();
            List<Vector3> headList = new List<Vector3>();
            List<Vector3> left = new List<Vector3>();
            List<Vector3> right = new List<Vector3>();
            List<(string, Vector4)> HeatmapPoints = new List<(string, Vector4)>();
            List<Vector4> ExteriorHeatmapPoints = new List<Vector4>();
            List<(string, Vector4)> TouchHeatmapPoints = new List<(string, Vector4)>();
            List<List<Vector3>> AvatarBonePosition = new List<List<Vector3>>();
            List<(bool, List<Vector3>)> PassengerBonePosition = new List<(bool, List<Vector3>)>();

            for (int i = 0; i < tokens.Count<JToken>(); i++)
            {


                JToken snapshots = tokens.ElementAt(i)["other"]["Gaze"]["interior"];
                string tag = (string)snapshots["tag"];
                JToken localCoords = snapshots["localCoords"];
                float x = (float)localCoords["x"];
                float y = (float)localCoords["y"];
                float z = (float)localCoords["z"];
                Vector3 addingvector = new Vector3(x, y, z);
                HeatmapPoints.Add((tag, addingvector));


                JToken snapshotsEx = tokens.ElementAt(i)["other"]["Gaze"]["exterior"];
                JToken localCoordsEx = snapshotsEx["worldCoords"];
                float xEx = (float)localCoordsEx["x"];
                float yEx = (float)localCoordsEx["y"];
                float zEx = (float)localCoordsEx["z"];
                Vector3 addingvectorEx = new Vector3(xEx, yEx, zEx);
                ExteriorHeatmapPoints.Add(addingvectorEx);


                JToken snapshotsTouch = tokens.ElementAt(i)["other"]["HandTracking"];
                string tagTouch = (string)snapshotsTouch["tag"];
                if (snapshotsTouch["localTouchLocation"] != null)
                {

                    JToken localCoordsTouch = snapshotsTouch["localTouchLocation"];
                    float xTouch = (float)localCoordsTouch["x"];
                    float yTouch = (float)localCoordsTouch["y"];
                    float zTouch = (float)localCoordsTouch["z"];
                    Vector3 addingvectorTouch = new Vector3(xTouch, yTouch, zTouch);
                    TouchHeatmapPoints.Add((tagTouch, addingvectorTouch));
                }
                else
                {
                    TouchHeatmapPoints.Add(("InteriorDisplay", new Vector4(1000, 1000, 1000, 1000)));
                }

                JEnumerable<JToken> armature = tokens.ElementAt(i)["other"]["Kinect"]["Bodies"].ElementAt(0)["joints"].Children();
                List<Vector3> AvatarBones = new List<Vector3>();
                foreach (JToken bone in armature)
                {
                    if ((string)bone["joint"] != "WristLeft" && (string)bone["joint"] != "WristRight" && (string)bone["joint"] != "SpineShoulder" && (string)bone["joint"] != "HandTipLeft" && (string)bone["joint"] != "ThumbLeft" && (string)bone["joint"] != "HandTipRight" && (string)bone["joint"] != "ThumbRight")
                    {
                        AvatarBones.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                    }
                    if ((string)bone["joint"] == "Head")
                    {
                        headList.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                    }
                    if ((string)bone["joint"] == "HandLeft")
                    {
                        left.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                    }
                    if ((string)bone["joint"] == "HandRight")
                    {
                        right.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                    }
                }
                AvatarBonePosition.Add(AvatarBones);

                List<Vector3> PassengerBones = new List<Vector3>();
                JEnumerable<JToken> passengerArmature = tokens.ElementAt(i)["other"]["Kinect"]["Bodies"].Children();
                if (passengerArmature.Count<JToken>() > 1)
                {
                    JEnumerable<JToken> passengerarmatureBigger = tokens.ElementAt(i)["other"]["Kinect"]["Bodies"].ElementAt(1)["joints"].Children();
                    foreach (JToken bone in passengerarmatureBigger)
                    {
                        if ((string)bone["joint"] != "WristLeft" && (string)bone["joint"] != "WristRight" && (string)bone["joint"] != "SpineShoulder" && (string)bone["joint"] != "HandTipLeft" && (string)bone["joint"] != "ThumbLeft" && (string)bone["joint"] != "HandTipRight" && (string)bone["joint"] != "ThumbRight")
                        {
                            PassengerBones.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                        }
                    }
                    PassengerBonePosition.Add((true, PassengerBones));
                }
                else
                {
                    for (int o = 0; o < 18; o++)
                    {
                        PassengerBones.Add(new Vector3(0, 0, 0));
                    }
                    PassengerBonePosition.Add((false, PassengerBones));
                }


                JEnumerable<JToken> objects = tokens.ElementAt(i)["objects"].Children();
                List<Vector4> cars = new List<Vector4>();
                foreach (JToken obj in objects) //foreach object
                {
                    //generate the key consisting of id type name (combination of those 3 should be unique)

                    if (obj["type"].ToString() == "Car" || obj["type"].ToString() == "Pickup" || obj["type"].ToString() == "SUV")
                    {
                        JToken position = obj["position"];
                        float xCar = (float)position["x"];
                        float yCar = (float)position["y"];
                        float zCar = (float)position["z"];
                        cars.Add(new Vector3(xCar, yCar, zCar));
                    }

                    if (obj["type"].ToString() == "Tesla")
                    {
                        JToken position = obj["position"];
                        float xCar = (float)position["x"];
                        float yCar = (float)position["y"];
                        float zCar = (float)position["z"];
                        positionThisCar.Add(new Vector3(xCar, yCar, zCar));
                    }
                }
                carPositions.Add(cars);
                // List<Vector4> oldlist = radiusCar[i];
                // oldlist.AddRange(cars);
                // radiusCar[i] = oldlist;
            }


            for (int j = 0; j < mainCarPositions.Count; j++)
            {
                float distanceold = 10000000000f;
                int index = 0;
                for (int k = 0; k < positionThisCar.Count; k++)
                {
                    float distancenew = Vector3.Distance(mainCarPositions[j], positionThisCar[k]);
                    if (distancenew < distanceold)
                    {
                        distanceold = distancenew;
                        index = k;
                    }
                    //float distance =
                }
                List<List<Vector4>> newList = new List<List<Vector4>>();
                newList.Add(carPositions[index]);
                List<List<Vector4>> oldlist = radiusCar[j];
                oldlist.AddRange(newList);
                // for (int i = 0; i < oldlist.Count; i++)
                // {
                //     Debug.Log("i = " + oldlist[i]);
                // }

                radiusCar[j] = oldlist;
            }

            List<Vector3> gazePointsPart = new List<Vector3>();
            for (int i = 0; i < tokens.Count<JToken>(); i++)
            {
                JToken gazeInt = tokens.ElementAt(i)["other"]["Gaze"]["interior"];
                JToken gazeEx = tokens.ElementAt(i)["other"]["Gaze"]["exterior"];
                Vector3 interriorGazepoint = new Vector3((float)gazeInt["localCoords"]["x"], (float)gazeInt["localCoords"]["y"], (float)gazeInt["localCoords"]["z"]);
                Vector3 exterriorGazepoint = new Vector3((float)gazeEx["worldCoords"]["x"], (float)gazeEx["worldCoords"]["y"], (float)gazeEx["worldCoords"]["z"]);

                if (exterriorGazepoint == new Vector3(0f, 0f, 0f))
                {
                    gazePointsPart.Add(interriorGazepoint + mainCarPositions[i]);
                }
                else
                {
                    gazePointsPart.Add(exterriorGazepoint);
                }
            }
            gazePoints.Add(gazePointsPart);

            PassengerBonePositionParticipant.Add(PassengerBonePosition);
            InitRadiusus();
            InitInteriorHeatmap(HeatmapPoints, part);
            InitTrajectories(part, headList, left, right);
            InitExteriorBuildingHeatmap(ExteriorHeatmapPoints, part);
            InitAvatar(AvatarBonePosition, part);
            InitTouchInteriorHeatmap(TouchHeatmapPoints, part);
            Debug.Log("End of Loading Participant" + part);
            //UnityManager.INSTANCE.ProgressBar();
            main = null;
            GC.Collect();
            if (part == 2)
            {
                Debug.Log("starting InitMergedAvatar");
                //                UnityManager.INSTANCE.ProgressBar();
                InitMergedAvatar();
                InitMergedPassenger();
                jumptoFrame("0");
                EventController.Instance.SetupPointsOfEventline();
                //                UnityManager.INSTANCE.sendDoneLoading();
            }
            else
            {
#if UNITY_WEBGL && !UNITY_EDITOR
                ReplayManager.Instance.LoadWebFile2("p3-neu.json", 2);
#endif
            }
        }





        /// <summary>
        /// Rotates point around other point with angle
        /// </summary>
        /// <param name="point"> point that will be rotated</param>
        /// <param name="pivot"> pivot point</param>
        /// <param name="angles"> rotation angle</param>
        /// <returns> rotated point </returns>
        Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
        {
            Vector3 dir = point - pivot; // get point direction relative to pivot
            dir = Quaternion.Euler(angles) * dir; // rotate it
            point = dir + pivot; // calculate rotated point
            return point; // return it
        }


        // [Obsolete("See ArrayExtension.ClosestTo")]
        // /// <see cref="ArrayExtension.ClosestTo(IEnumerable{double}, double)"/>
        // int nearestFrame(double stamp)
        // {
        //     // Debug.Log("timestamp: " + compScenePlayer.snapshots[0].timeStamp);
        //     // Debug.Log("stamp: " + stamp);

        //     for (int i = 0; i < compScenePlayer.snapshots.Count; i++)
        //     {
        //         if (compScenePlayer.snapshots[0].timeStamp > stamp)
        //         {
        //             // Debug.Log("Ausgabe 0");
        //             return 0;
        //         }
        //         else if (compScenePlayer.snapshots[i].timeStamp == stamp)
        //         {
        //             // Debug.Log("Ausgabe i");
        //             return i;
        //         }
        //         else if (compScenePlayer.snapshots[i].timeStamp > stamp)
        //         {
        //             // Debug.Log("Ausgabe i-1");
        //             // Debug.Log(i - 1);
        //             return (i - 1);
        //         }

        //     }
        //     // Debug.Log("default");
        //     return (compScenePlayer.snapshots.Count - 1);
        // }

        /// <summary>
        /// toggles heatmap status
        /// </summary>
        /// <param name="index"> heatmap id</param>
        public void toggleHeatmap(int index)
        {
            if (index < 4)
            {
                for (int i = 0; i < interiorHeatmaps.Count; i++)
                {
                    interiorHeatmaps[i].gameObject.transform.GetChild(index).gameObject.SetActive(!interiorHeatmaps[i].gameObject.transform.GetChild(index).gameObject.activeInHierarchy);
                    interiorTouchHeatmaps[i].gameObject.transform.GetChild(index).gameObject.SetActive(!interiorTouchHeatmaps[i].gameObject.transform.GetChild(index).gameObject.activeInHierarchy);
                }
            }
            else if (index == 4)
            {
                foreach (GameObject heatmap in exteriorBuildings)
                {
                    if (heatmap.name == "Heatmap")
                    {
                        heatmap.SetActive(!heatmap.activeInHierarchy);
                    }
                    else
                    {
                        heatmap.transform.GetChild(1).gameObject.SetActive(!heatmap.transform.GetChild(1).gameObject.activeInHierarchy);
                    }

                }
            }
            else
            {
                for (int k = 0; k < radiuses.Count; k++)
                {
                    radiuses[k].gameObject.SetActive(!radiuses[k].gameObject.activeInHierarchy);
                }
                // radiusHeatmap.gameObject.SetActive(!radiusHeatmap.gameObject.activeInHierarchy);
                // radiusHeatmap1.gameObject.SetActive(!radiusHeatmap1.gameObject.activeInHierarchy);
                // radiusHeatmap2.gameObject.SetActive(!radiusHeatmap2.gameObject.activeInHierarchy);
            }
        }

        /// <summary>
        /// toggles trajectory on/off
        /// </summary>
        /// <param name="index"> id of participant</param>
        public void toggleTrajectoy(int index)
        {
            for (int i = 0; i < TrajectoriesAll.Count; i++)
            {
                TrajectoriesAll[i].transform.GetChild(index).gameObject.SetActive(!TrajectoriesAll[i].transform.GetChild(index).gameObject.activeInHierarchy);
            }
        }

        /// <summary>
        /// toggles Passenger gameobject on/off
        /// </summary>
        public void togglePassenger()
        {
            PassengerController.SetActive(!PassengerController.activeInHierarchy);
        }

        /// <summary>
        /// toggles all driver on/off
        /// </summary>
        public void toggleDriver()
        {
            for (int i = 0; i < 4; i++)
            {
                AvatarList[i].Item2.SetActive(!AvatarList[i].Item2.activeInHierarchy);
                AvatarList[i] = (!AvatarList[i].Item1, AvatarList[i].Item2);
            }

        }

        /// <summary>
        /// disables all visualisations of participant
        /// </summary>
        /// <param name="index"> id of participant</param>
        public void disableParticipant(int index)
        {
            TrajectoriesAll[index].gameObject.SetActive(false);
            radiuses[index].gameObject.SetActive(false);
            AvatarList[index].Item2.SetActive(false);
            AvatarList[index] = (false, AvatarList[index].Item2);

            interiorHeatmaps[index].gameObject.SetActive(false);
        }

        /// <summary>
        /// enables all visualisations of participant
        /// </summary>
        /// <param name="index"> id of participant</param>
        public void enableParticipant(int index)
        {
            TrajectoriesAll[index].gameObject.SetActive(true);
            radiuses[index].gameObject.SetActive(true);
            AvatarList[index].Item2.SetActive(true);
            AvatarList[index] = (true, AvatarList[index].Item2);
            interiorHeatmaps[index].gameObject.SetActive(true);
        }

        /// <summary>
        /// changes color of trajectory of participant. originally created for webgl communcation but adjusted for unity-only version.
        /// </summary>
        /// <param name="s">string of participantid + hexadecimal code of color separated with ";" Example: "1;#FFFFF"</param>
        public void changeTrajectoyColor(string s)
        {
            string[] arr;
            arr = s.Split(';');
            Color colorOfString;
            ColorUtility.TryParseHtmlString(arr[1], out colorOfString);
            Color colorAvatar;
            colorAvatar = new Color(colorOfString.r, colorOfString.g, colorOfString.b, 0.4f);
            Color trajectory;
            trajectory = new Color(colorOfString.r, colorOfString.g, colorOfString.b, 1f);
            Trajectories selectedTrajectory = TrajectoriesAll[Int32.Parse(arr[0])].GetComponent<Trajectories>();
            for (int i = 0; i < 3; i++)
            {
                selectedTrajectory.TrajectoriesList[i].startColor = trajectory;
                selectedTrajectory.TrajectoriesList[i].endColor = trajectory;
            }
            AvatarList[Int32.Parse(arr[0])].Item2.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material.SetColor("_Color", colorAvatar);
            Debug.Log(arr[1]);
            WebBrowserHandling.Instance.webViewPrefab.WebView.PostMessage("{\"type\": \"Color\",\"participant\": \"" + selectedParticipant + "\", \"message\": \"" + (string)arr[1] + "\"}");
        }


        /// <summary>
        /// changes color of trajectory
        /// </summary>
        /// <param name="input"></param>
        public void handleChangedColor(InputField input)
        {
            changeTrajectoyColor(selectedParticipant + ";" + input.text);

        }

        /// <summary>
        /// toggle color picker on/off
        /// </summary>
        public void toggleColorPicker()
        {
            ColorPicker.SetActive(!ColorPicker.activeInHierarchy);
        }

        /// <summary>
        /// hit point of raycast for detection of height difference of cesium map
        /// </summary>
        Vector3 point;
        /// <summary>
        ///  updates the car height position depending on the difference of cesium map height
        /// </summary>
        public void UpdateCarPosition()
        {
            Vector3 newPos = new Vector3(mainCar.transform.position.x, mainCar.transform.position.y + 500f, mainCar.transform.position.z + 2f);

            //layerMask = ~layerMask; 
            //int layer_mask = LayerMask.GetMask("Car");
            int layer_mask = LayerMask.GetMask("Environment");
            Physics.Raycast(newPos, Vector3.up * -1, out RaycastHit hitinfo, Mathf.Infinity, layer_mask);
            //Physics.Raycast(newPos, Vector3.up * -1, out RaycastHit hitinfo, Mathf.Infinity, ~layer_mask);

            point = hitinfo.point;
            float yDiff = mainCar.transform.position.y - point.y;
            //if (yDiff >= mainCar.transform.position.y)
            //{
            //    yDiff = 0f;
            //}
            // if (yDiff >= mainCar.transform.position.y - 1f && point.y > 0)
            // {
            //     yDiff = 0f;
            // }


            float differntpos = CesiumMap.transform.position.y + yDiff;
            float differntPlanepos = plane.transform.position.y + yDiff;

            CesiumMap.transform.position = new Vector3(CesiumMap.transform.position.x, differntpos, CesiumMap.transform.position.z);
            plane.transform.position = new Vector3(plane.transform.position.x, differntPlanepos, plane.transform.position.z);
            // cube.transform.position = newPos;
        }

        [Obsolete]
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawCube(point, new Vector3(1, 1, 1));
        }


        public void LoadMaterials()
        {
            AvatarMaterials = new List<Material>();
            Material[] materials = Resources.LoadAll<Material>("Materials/Avatar/");

            foreach (Material m in materials)
            {
                AvatarMaterials.Add(m);
            }
        }


    }


}
