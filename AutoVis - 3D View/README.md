# AutoVis - 3D View

AutoVis is a Unity-based program for analyzing recorded data in JSON format to display spatial and temporal data. The program is designed to show the data of a recorded participant, including their positions, gaze heatmaps (interior and exterior), and touch locations inside the car. The program has two main features: a browser view and a VR view.

## Features
- Display spatial data of recorded participant, including positions, gaze heatmaps, and touch locations
- Browser view and VR view
- Support for JSON data format

## Built With
- Unity
- Node.js
- D3

## Getting Started

To use AutoVis, you will need to have Unity installed on your machine. Once Unity is installed, you can download the AutoVis project files and open the project in Unity.

### Prerequisites

- Unity
- Node.js (if you want to use the browser or VR view features)
- Serveral bought Assets

### SetUp
1. Register to [Cesium] (https://ion.cesium.com/signup/) to be able to see the San Francisco Map
2. Buy and Download the following Assets and import them into the Project
* [Urban Traffic System](https://assetstore.unity.com/packages/templates/systems/urban-traffic-system-89133)
* [3D WebView for Windows and macOS (Web Browser)](https://assetstore.unity.com/packages/tools/gui/3d-webview-for-windows-and-macos-web-browser-154144)
* [RealisticMobileCars - Pro 3D Models](https://assetstore.unity.com/publishers/31689)

### Installing

1. Download the AutoVis project files from Gitlab.
2. Open Unity and select "Open" from the File menu.
3. Navigate to the folder where you downloaded the AutoVis project files, and select the "AutoVis" folder.

### Usage

To use AutoVis, you will need to first start the project server. For instructions on how to run the server, please refer to the [AutoVis - Web application](../Web%20application/README.md)

Once the server is running, you can use the browser view or VR view features as described below:

#### Browser View

- In Unity, navigate to the "Assets/Scenes" folder and open the "Browser" scene.
- Press the play button in Unity to start the scene.
- A controllable HTML page will be displayed in Unity, which can be used to view the data from a JSON file.

#### VR View

- In Unity, navigate to the "Assets/Scenes" folder and open the "VR" scene.
- Press the play button in Unity to start the scene.
- A HTML page with 2D graphs on the left controller that are made with D3 will be displayed in Unity, which can be used to view the data from a JSON file.




<!-- ## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
- Inspiration
- etc -->

## Importing Data:

To load other data into the program the following changes must be made:

- The structure of the individual JSONS for the models can be found in Assets/Resources/Recordings/Main/Models and for the Events in Assets/Resources/Recordings/Main/Events.

- In the Main folder(Assets/Resources/Recordings/Main) there are two folders each one for the Events and one for the other Data.The JSONS in this folder are
responsible for the models. This means that there can only be a maximum of one record in each of the two folders.

- The Other folder (Assets/Resources/Recordings/Other) contains the data sets of the participants whose models are not loaded.
Here only the data like avatar bone position and their heatmap positions are evaluated. To ensure a correct assignment for the
the Events of the Participants the data in the folder Events and Models must have the same alphabetical order.

- Currently the program supports a maximum number of 5 participants due to the fact that a color assignment is necessary for things like events and avatar colors.


## File Structure


│ ├── Assets/<br>
│ │ ├── Resources<br>
│ │ │ ├── Avatars<br>
│ │ │ ├── EventIcons<br>
│ │ │ ├── Recordings<br>
│ │ │ └── ReplayModels<br>
│ │ ├── SceneRecoder/<br>
│ │ │ ├── ...<br>
│ │ │ ├── Prefabs<br>
│ │ │ ├── Scripts<br>
│ │ │ │ ├── ...<br>
│ │ │ │ ├── Replay<br>
│ │ │ │ ├── UI<br>
│ │ │ │ └── Visualisation<br>
│ │ │ └── Textures<br>
│ │ └── Scenes/<br>
│ │ │ ├── ...<br>
│ │ │ ├── Browser.unity<br>
│ │ │ └── VR.unity<br>
└── README.md<br>

- `Assets/`: Contains all Assets in the Project
- `Assets/Resources/`: Contains all Resources loaded into the Program
- `Assets/Resources/Avatars`: Contains prefab of Custom Avatar Positions.
- `Assets/Resources/EventIcons`: Contains all Icons for the Eventlines.
- `Assets/Resources/Recordings`: Contains all recorded JSON data of the different participants.
- `Assets/Resources/ReplayModels`: Contains all models.
- `Assets/SceneRecoder/`: Contains all prefabs, materials and scripts in the project.
- `Assets/SceneRecoder/Prefabs`: Contains all prefabs that are not loaded.
- `Assets/SceneRecoder/Scripts/`: Contains all used scripts in the project
- `Assets/SceneRecoder/Scripts/Replay/`: Contains all scripts with functionality around playing back the recorded data.
- `Assets/SceneRecoder/Scripts/UI/`: Contains all scripts with functions around the display and use of the user interface
- `Assets/SceneRecoder/Scripts/Visualisation/`: Contains all scripts with functions around the different visualization forms 
- `Assets/SceneRecoder/Textures/`: Contains the render textures for the different camera views.
- `Assets/Scenes/`: Contains all scenes in the project
- `Assets/Scenes/Browser.unity`: The main browser scene.
- `Assets/Scenes/VR.unity`: The main vr scene.
- `README.md`: This readme file

## Free Unity assets used in the project
- [EzySlice](https://github.com/DavidArayan/ezy-slice)
- [Vive Input Utility](https://assetstore.unity.com/packages/tools/integration/vive-input-utility-64219)
- [Mirror](https://assetstore.unity.com/packages/tools/network/mirror-129321)
- [QuickOutline](https://assetstore.unity.com/packages/tools/particles-effects/quick-outline-115488)
- [SteamVR](https://assetstore.unity.com/packages/tools/integration/steamvr-plugin-32647)
- [FlexibleColorPicker](https://assetstore.unity.com/packages/tools/gui/flexible-color-picker-150497)

## Authors
- Alexander Häusele
- Thilo Segschneider