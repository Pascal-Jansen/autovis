using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarController : MonoBehaviour
{


    public GameObject avatarmodel;
    public GameObject AvatarPrefab;
    public GameObject mainCar;

    public List<List<List<Vector3>>> AvatarBonePositionParticipant = new List<List<List<Vector3>>>();

    public List<GameObject> Avatar = new List<GameObject>() ;

    public List<(bool, GameObject)> AvatarList = new List<(bool, GameObject)>();



    public static AvatarController Instance;

    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void InitAvatar(List<List<Vector3>> ParticipantAvatarList, int partIndex)
    {
        //GameObject avatar = Instantiate(avatarmodel);
        //Vector3 posAvatar = avatar.transform.localPosition;
        //avatar.transform.parent = mainCar.transform;
        //avatar.transform.rotation = mainCar.transform.rotation;
        //avatar.transform.localPosition = posAvatar;
        GameObject avatar = AvatarPrefab;

        AvatarBonePositionParticipant.Add(ParticipantAvatarList);
        avatar.GetComponent<RigControl>().mainCar = mainCar;
        avatar.GetComponent<RigControl>().AvatarModel = Avatar;

        //avatar.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material = AvatarMaterials[partIndex];
        AvatarList.Add((true, avatar));
    }

    public void DrawAvatarMovement(int index)
    {
        Debug.Log("NextFrame" + index);

        for (int i = 0; i < AvatarBonePositionParticipant.Count; i++)
        {
            if (AvatarList[i].Item1)
            {
                RigControl avatar = AvatarList[i].Item2.GetComponent<RigControl>();
                avatar.positionsBones = AvatarBonePositionParticipant[i][index];
                avatar.setbonesAll();
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void DisableAvatar()
    {
        AvatarPrefab.SetActive(!AvatarPrefab.activeInHierarchy);
    }
}
