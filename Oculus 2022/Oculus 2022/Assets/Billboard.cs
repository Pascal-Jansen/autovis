using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private GameObject vrCam;
    // Start is called before the first frame update
    void Start()
    {
        vrCam = GameObject.Find("CenterEyeAnchor");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(2 * transform.position - vrCam.transform.position);
    }
}
