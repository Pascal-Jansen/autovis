using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SingleHeatmap : MonoBehaviour, IPointerClickHandler
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        //int index = transform.GetSiblingIndex();
        //ReplayManagerOculus.Instance.ToggleHeatmapMesh(index);
        if (ReplayManagerOculus.Instance.SeethroughInstanciated != null)
        {
            Destroy(ReplayManagerOculus.Instance.SeethroughInstanciated);
        }

            GameObject see = Instantiate(ReplayManagerOculus.Instance.SeethroughPrefab);
            see.transform.position = pointerEventData.pointerCurrentRaycast.worldPosition;
            ReplayManagerOculus.Instance.SeethroughInstanciated = see;
        

    }

}
