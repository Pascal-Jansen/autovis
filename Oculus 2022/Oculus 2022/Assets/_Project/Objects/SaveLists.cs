using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Save", menuName = "ScriptableObjects/SaveJsonData", order = 1)]
public class SaveLists: ScriptableObject
{
    public List<(double, Vector3, Vector3)> CarPositions = new List<(double, Vector3, Vector3)>();

    public List<(string, Vector4)> InteriorHeatmap = new List<(string, Vector4)>();

    public List<(string, Vector4)> TouchHeatmap = new List<(string, Vector4)>();

    public int test;

    public List<double> CarPositionsTime = new List<double>();
    public List<Vector3> CarPositionsPos = new List<Vector3>();
    public List<Vector3> CarPositionsRot = new List<Vector3>();

    public List<string> InteriorHeatmapString = new List<string>();
    public List<Vector4> InteriorHeatmapVec4 = new List<Vector4>();

    public List<string> TouchHeatmapString = new List<string>();
    public List<Vector4> TouchHeatmapVec4 = new List<Vector4>();

    public List<List<Vector3>> AvatarBonePositions = new List<List<Vector3>>();

    public List<Vector3> bone1 = new List<Vector3>();
    public List<Vector3> bone2 = new List<Vector3>();
    public List<Vector3> bone3 = new List<Vector3>();
    public List<Vector3> bone4 = new List<Vector3>();
    public List<Vector3> bone5 = new List<Vector3>();
    public List<Vector3> bone6 = new List<Vector3>();
    public List<Vector3> bone7 = new List<Vector3>();
    public List<Vector3> bone8 = new List<Vector3>();
    public List<Vector3> bone9 = new List<Vector3>();
    public List<Vector3> bone10 = new List<Vector3>();
    public List<Vector3> bone11= new List<Vector3>();
    public List<Vector3> bone12 = new List<Vector3>();
    public List<Vector3> bone13 = new List<Vector3>();
    public List<Vector3> bone14 = new List<Vector3>();
    public List<Vector3> bone15= new List<Vector3>();
    public List<Vector3> bone16 = new List<Vector3>();
    public List<Vector3> bone17 = new List<Vector3>();
    public List<Vector3> bone18 = new List<Vector3>();


}