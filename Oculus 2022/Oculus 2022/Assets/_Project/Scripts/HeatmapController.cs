using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatmapController : MonoBehaviour
{

    public static HeatmapController Instance;
    public List<(double time, Vector3 position, Vector3 rotation)> allCarPos = new List<(double time, Vector3 position, Vector3 rotation)>();

    private List<InteriorHeatmaps> interiorHeatmaps = new List<InteriorHeatmaps>();

    private List<TouchHeatmaps> interiorTouchHeatmaps = new List<TouchHeatmaps>();

    public GameObject InteriorHeatmap;

    public GameObject TouchHeatmap;

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void InitInteriorHeatmap(List<(string, Vector4)> points, int participantId)
    {
        ReplayManagerOculus.Instance.InteriorPoints = points;
        InteriorHeatmaps thisHeatmap = InteriorHeatmap.GetComponent<InteriorHeatmaps>();
        thisHeatmap.SetupTextures(participantId, points);
        interiorHeatmaps.Add(thisHeatmap);
    }

    public void DrawHeatmaps(int index)
    {
        for (int i = 0; i < interiorHeatmaps.Count; i++)
        {
            interiorHeatmaps[i].DrawInteriorHeatmap(index);
        }
    }


    public void InitTouchInteriorHeatmap(List<(string, Vector4)> points, int participantId)
    {

        List<(string, Vector4)> pointsNew = new List<(string, Vector4)>();
        //for (int i = 0; i < points.Count; i++)
        //{
        //    pointsNew.Add((points[i].Item1, (points[i].Item2 + new Vector4(+5.471f, -0.222f, -2.39f, 0))));
        //}
        ReplayManagerOculus.Instance.TouchPoints = points;
        //ReplayManagerOculus.Instance.TouchPoints = points;
        TouchHeatmaps thisHeatmap = TouchHeatmap.GetComponent<TouchHeatmaps>();
        thisHeatmap.SetupTextures(participantId, points);
        interiorTouchHeatmaps.Add(thisHeatmap);
    }

    public void DrawTouchHeatmaps(int index)
    {
        for (int i = 0; i < interiorTouchHeatmaps.Count; i++)
        {
            interiorTouchHeatmaps[i].DrawTouchInteriorHeatmap(index);
        }
    }



    public void DisableInteriorHeatmap()
    {
        InteriorHeatmap.SetActive(!InteriorHeatmap.activeInHierarchy);
    }

    public void DisableTouchHeatmap()
    {
        TouchHeatmap.SetActive(!TouchHeatmap.activeInHierarchy);
    }





}
