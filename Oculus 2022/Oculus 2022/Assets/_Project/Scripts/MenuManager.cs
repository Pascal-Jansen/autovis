using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{

    public List<Button> HeatmapButtons;

    public Camera MainCameraOculus;

    public OVRCameraRig m_OVRCameraRig;


    public static MenuManager Instance;


    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //transform.localPosition = new Vector3(0, 5f, 0);
    }

    public void TestOnClick()
    {
        Debug.Log("YAY");
    }

    public void ResetCamera()
    {
        Debug.Log("is Resetted");
        // OVRManager.display.RecenteredPose();
        // MainCameraOculus.gameObject.transform.rotation = Quaternion.Euler(Vector3.zero);
        // MainCameraOculus.gameObject.transform.position = Vector3.zero;

    }

    public void ResetPosition()
    {
        float currentRotY = m_OVRCameraRig.centerEyeAnchor.eulerAngles.y;
        float targetRotY = 0.0f;
        float difference = targetRotY - currentRotY;
        m_OVRCameraRig.transform.Rotate(0, difference, 0);
        Vector3 currentPosition = m_OVRCameraRig.centerEyeAnchor.localPosition;
        m_OVRCameraRig.trackingSpace.localPosition = new Vector3(OffsetValue(currentPosition.x), OffsetValue(currentPosition.y), OffsetValue(currentPosition.z));
    }


    float OffsetValue(float value)
    {
        if (value != 0)
        {
            value *= -1;
        }
        return value;
    }
}
