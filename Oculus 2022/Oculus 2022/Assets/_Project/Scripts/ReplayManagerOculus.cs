using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;


public class ReplayManagerOculus : MonoBehaviour
{

    private StreamReader reader;

    public GameObject ovrm;

    public GameObject mainCar;

    public List<GameObject> inCarObjects;

    public List<GameObject> ListOfOtherObjects;

    public bool playing = false;

    private float frameTime;

    public int CurrentTimeStampIndex = 0;

    public static ReplayManagerOculus Instance;

    public GameObject SeethroughPrefab;

    public GameObject SeethroughInstanciated;

    public Button Buttontest1;

    public Button Buttontest2;

    public Button Buttontest3;

    public GameObject menu;

    public List<(string, Vector4)> TouchPoints = new List<(string, Vector4)>();

    public List<(string, Vector4)> InteriorPoints = new List<(string, Vector4)>();

    public List<List<List<Vector3>>> avatarDifferentBuild = new List<List<List<Vector3>>>();



    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //LoadFileFromDisk("Recordings/Part1 - All.json", 0);
      LoadFileFromDisk("Demo_Recording_2022-12-13_18-00.json", 0);
       //LoadFromScriptableObject();
    }

    // Update is called once per frame
    void Update()
    {
        ToggleMenu();
    }

    void LoadFromScriptableObject()
    {
        SaveLists savelist = (SaveLists)Resources.Load("SaveParticipant0");
        LoadData(savelist);
        //Debug.Log(savelist.CarPositions);
        //Debug.Log(savelist.CarPositions.Count);
        //Debug.Log(savelist.test);
        //HeatmapController.Instance.allCarPos = savelist.CarPositions;
        //HeatmapController.Instance.InitInteriorHeatmap(savelist.InteriorHeatmap, 0);
        //HeatmapController.Instance.InitTouchInteriorHeatmap(savelist.TouchHeatmap, 0);
    }

    public void LoadFileFromDisk(string path, int participantId)
    {
        TextAsset mytxtData = (TextAsset)Resources.Load("MyText");
        string filePath = "Recordings/" + path.Replace(".json", "");

        TextAsset targetFile = Resources.Load<TextAsset>(filePath);

        string txt = targetFile.text;
        InitRecording(txt, 0);
        // reader = new StreamReader(path, Encoding.UTF8, false, 65536);

        // string content = reader.ReadToEnd();
        // Debug.Log(content);
        // InitRecording(content, 0);
    }

    public void InitRecording(string json, int participantId)
    {

        JObject main = JObject.Parse(json); //Parse json as main object

        // frameTime = 1f / main.Value<float>("tickRate"); //Default playback speed


        JEnumerable<JToken> tokens = main["snapshots"].Children();  //get all recorded frames

        List<double> ts = new List<double>();   //list of timestamps
        List<(string, Vector4)> HeatmapPoints = new List<(string, Vector4)>();
        List<(string, Vector4)> TouchHeatmapPoints = new List<(string, Vector4)>();
        List<Vector3> headList = new List<Vector3>();
        List<Vector3> left = new List<Vector3>();
        List<Vector3> right = new List<Vector3>();
        List<List<Vector3>> AvatarBonePosition = new List<List<Vector3>>();
        List<bool> CurrentlyPointing = new List<bool>();
        List<(bool, List<Vector3>)> PassengerBonePosition = new List<(bool, List<Vector3>)>();
        List<(double time, Vector3 position, Vector3 rotation)> teslaPos = new List<(double time, Vector3 position, Vector3 rotation)>();
        frameTime = 1f / main.Value<float>("tickRate");
        foreach (JToken t in tokens)    //foreach recorded frame
        {


            double timeStamp = t.Value<double>("timeStamp");    //add timestamp to array for easier access
            ts.Add(timeStamp);

            JToken snapshots = t["other"]["Gaze"]["interior"];
            string tag = (string)snapshots["tag"];
            JToken localCoords = snapshots["localCoords"];
            float x = (float)localCoords["x"];
            float y = (float)localCoords["y"];
            float z = (float)localCoords["z"];
            Vector3 addingvector = new Vector3(x, y, z);
            HeatmapPoints.Add((tag, addingvector));

            JToken snapshotsTouch = t["other"]["HandTracking"];
            string tagTouch = (string)snapshotsTouch["tag"];
            if (tagTouch != "None")
            {
            JToken localCoordsTouch = snapshotsTouch["localTouchLocation"];
            float xTouch = (float)localCoordsTouch["x"];
            float yTouch = (float)localCoordsTouch["y"];
            float zTouch = (float)localCoordsTouch["z"];
            Vector3 addingvectorTouch = new Vector3(xTouch, yTouch, zTouch);
            TouchHeatmapPoints.Add((tag, addingvectorTouch));

            }
            else
            {
                TouchHeatmapPoints.Add(("InteriorDisplay", new Vector4(1000, 1000, 1000, 1000)));
            }



            List<Vector3> AvatarBones = new List<Vector3>();
            JEnumerable<JToken> armature = t["other"]["Kinect"]["Bodies"].ElementAt(0)["joints"].Children();
            foreach (JToken bone in armature)
            {
                if ((string)bone["joint"] != "WristLeft" && (string)bone["joint"] != "WristRight" && (string)bone["joint"] != "SpineShoulder" && (string)bone["joint"] != "HandTipLeft" && (string)bone["joint"] != "ThumbLeft" && (string)bone["joint"] != "HandTipRight" && (string)bone["joint"] != "ThumbRight")
                {
                    AvatarBones.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                }
                if ((string)bone["joint"] == "Head")
                {
                    headList.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                }
                if ((string)bone["joint"] == "HandLeft")
                {
                    left.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                }
                if ((string)bone["joint"] == "HandRight")
                {
                    right.Add(new Vector3((float)bone["position"]["x"], (float)bone["position"]["y"], (float)bone["position"]["z"]));
                }
            }
            AvatarBonePosition.Add(AvatarBones);





            JEnumerable<JToken> objects = t["objects"].Children();  //List of all objects in one frame


            List<Vector4> cars = new List<Vector4>();
            foreach (JToken obj in objects) //foreach object
            {



                if (obj["type"].ToString() == "Tesla")
                {
                    double time = timeStamp;
                    Vector3 carPos = new Vector3((float)obj["position"]["x"], (float)obj["position"]["y"], (float)obj["position"]["z"]);
                    Vector3 carRot = new Vector3((float)obj["rotation"]["x"], (float)obj["rotation"]["y"], (float)obj["rotation"]["z"]);
                    teslaPos.Add((time, carPos, carRot));
                }


            }
        }

        HeatmapController.Instance.allCarPos = teslaPos;
        HeatmapController.Instance.InitInteriorHeatmap(HeatmapPoints, participantId);
        HeatmapController.Instance.InitTouchInteriorHeatmap(TouchHeatmapPoints, participantId);
        Debug.Log(AvatarBonePosition.Count);
        AvatarController.Instance.InitAvatar(AvatarBonePosition, 0);
        SaveData(participantId);
        LoadTime(0);

    }


    public void up()
    {
        ovrm.transform.position += new Vector3(0, 0.1f, 0);
    }

    public void down()
    {
        ovrm.transform.position -= new Vector3(0, 0.1f, 0);
    }

    public void Play(float playBackMultiplier)
    {
        StopAllCoroutines();
        playing = true;
        StartCoroutine(PlayScene(frameTime / playBackMultiplier));


    }

    public void Pause()
    {
        Debug.Log("Pause");
        playing = false;
        StopAllCoroutines();
    }

    IEnumerator PlayScene(float playBackSpeed)
    {
        Debug.Log("Play " + playing);
        while (playing)
        {
            CurrentTimeStampIndex++;
            //if (HeatmapController.Instance.allCarPos.Count <= CurrentTimeStampIndex)
            //{
            Debug.Log(AvatarController.Instance.AvatarBonePositionParticipant[0].Count);
                if (AvatarController.Instance.AvatarBonePositionParticipant[0].Count <= CurrentTimeStampIndex)
                {

                CurrentTimeStampIndex = 0;
                }
            //}
            LoadTime(CurrentTimeStampIndex);

            yield return new WaitForSeconds(playBackSpeed);

        }

        yield return null;

    }

    public void LoadTime(int CurrentTimeStampIndex)
    {
        UpdateCar(CurrentTimeStampIndex);

        //TODO DISABLED FOR NOW 
        HeatmapController.Instance.DrawHeatmaps(CurrentTimeStampIndex);
        HeatmapController.Instance.DrawTouchHeatmaps(CurrentTimeStampIndex);
        AvatarController.Instance.DrawAvatarMovement(CurrentTimeStampIndex);

    }


    public void UpdateCar(int CurrentTimeStampIndex)
    {
        //Debug.Log(HeatmapController.Instance.allCarPos.Count);
        //Debug.Log(HeatmapController.Instance.allCarPos[0]);
        //Debug.Log(HeatmapController.Instance.allCarPos[1]);
        //mainCar.transform.position = HeatmapController.Instance.allCarPos[CurrentTimeStampIndex].Item2;
        //mainCar.transform.rotation = Quaternion.Euler(HeatmapController.Instance.allCarPos[CurrentTimeStampIndex].Item3);

    }

    public void ToggleHeatmapMesh(int index)
    {
        inCarObjects[index].SetActive(!inCarObjects[index].activeInHierarchy);
        CheckHeatmaps();


    }

    public void CheckHeatmaps()
    {
        bool allOff = false;
        for (int i = 0; i < inCarObjects.Count; i++)
        {
            if (!inCarObjects[i].activeInHierarchy)
            {
                allOff = true;
            }
        }
        if (allOff)
        {
            for (int j = 0; j < ListOfOtherObjects.Count; j++)
            {
                ListOfOtherObjects[j].SetActive(false);
            }

        }
        else
        {
            for (int j = 0; j < ListOfOtherObjects.Count; j++)
            {
                ListOfOtherObjects[j].SetActive(true);
            }
        }

    }

    public void ToggleMenu()
    {
        if (OVRInput.GetUp(OVRInput.Button.Start))
        {
            menu.SetActive(!menu.activeInHierarchy);
        }
    }

    public void SaveData(int participantid)
    {
       string path = "Assets/SaveParticipant"+participantid+".asset";
#if UNITY_EDITOR
        SaveLists obj = ScriptableObject.CreateInstance<SaveLists>();
        //Debug.Log(HeatmapController.Instance.allCarPos.Count);
       // CompileCarList(obj);
        CompileInteriorHeatmap(obj);
        CompileTouchHeatmap(obj);
        rebuildList();
        CompileAvatar(obj,participantid);
        obj.CarPositions = HeatmapController.Instance.allCarPos;
        //obj.AvatarBonePositions = AvatarController.Instance.AvatarBonePositionParticipant[1];
        obj.InteriorHeatmap = InteriorPoints;
        obj.TouchHeatmap = TouchPoints;
        obj.test = 5;

        UnityEditor.AssetDatabase.CreateAsset(obj, path);
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.AssetDatabase.Refresh();
#endif


    }
    public void CompileCarList(SaveLists s)
    {
        List<double> currCarPositionTime = new List<double>();
        List<Vector3> currCarPositionPos = new List<Vector3>();
        List<Vector3> currCarPositionRot = new List<Vector3>();
        for (int i = 0; i< AvatarController.Instance.AvatarBonePositionParticipant[0].Count; i++)
        {
            currCarPositionTime.Add(HeatmapController.Instance.allCarPos[i].Item1);
            currCarPositionPos.Add(HeatmapController.Instance.allCarPos[i].Item2);
            currCarPositionRot.Add(HeatmapController.Instance.allCarPos[i].Item3);
        }
        s.CarPositionsTime = currCarPositionTime;
        s.CarPositionsPos = currCarPositionPos;
        s.CarPositionsRot = currCarPositionRot;
    }

    public void rebuildList()
    {
        List<List<Vector3>> save = new List<List<Vector3>>();
        for (int j = 0; j < AvatarController.Instance.AvatarBonePositionParticipant[0][1].Count; j++)
        {
            List<Vector3> bone = new List<Vector3>();
            
            
            for (int i = 0; i < AvatarController.Instance.AvatarBonePositionParticipant[0].Count; i++){
                bone.Add(AvatarController.Instance.AvatarBonePositionParticipant[0][i][j]);

            }
            save.Add(bone);

        }
    

    avatarDifferentBuild.Add(save);
}

    public void CompileInteriorHeatmap(SaveLists s)
    {
        List<string> currInteriorHeatmapString = new List<string>();
        List<Vector4> currInteriorHeatmapPos = new List<Vector4>();
        for (int i = 0; i < AvatarController.Instance.AvatarBonePositionParticipant[0].Count; i++)
        {
            currInteriorHeatmapString.Add(InteriorPoints[i].Item1);
            currInteriorHeatmapPos.Add(InteriorPoints[i].Item2);
        }
        Debug.Log(currInteriorHeatmapString.Count);
        s.InteriorHeatmapString = currInteriorHeatmapString;
        s.InteriorHeatmapVec4 = currInteriorHeatmapPos;
    }

    public void CompileTouchHeatmap(SaveLists s)
    {
        List<string> currTouchHeatmapString = new List<string>();
        List<Vector4> currTouchHeatmapPos = new List<Vector4>();
        for (int i = 0; i < AvatarController.Instance.AvatarBonePositionParticipant[0].Count; i++)
        {
            currTouchHeatmapString.Add(TouchPoints[i].Item1);
            currTouchHeatmapPos.Add(TouchPoints[i].Item2);
        }
        s.TouchHeatmapString = currTouchHeatmapString;
        s.TouchHeatmapVec4 = currTouchHeatmapPos;
    }

    public void CompileAvatar(SaveLists s, int participantID)
    {
        List<List<Vector3>> AvatarBonePositions = avatarDifferentBuild[participantID];

        s.bone1 = AvatarBonePositions[0];
        s.bone2 = AvatarBonePositions[1];
        s.bone3 = AvatarBonePositions[2];
        s.bone4 = AvatarBonePositions[3];
        s.bone5 = AvatarBonePositions[4];
        s.bone6 = AvatarBonePositions[5];
        s.bone7 = AvatarBonePositions[6];
        s.bone8 = AvatarBonePositions[7];
        s.bone9 = AvatarBonePositions[8];
        s.bone10 = AvatarBonePositions[9];
        s.bone11 = AvatarBonePositions[10];
        s.bone12 = AvatarBonePositions[11];
        s.bone13 = AvatarBonePositions[12];
        s.bone14 = AvatarBonePositions[13];
        s.bone15 = AvatarBonePositions[14];
        s.bone16 = AvatarBonePositions[15];
        s.bone17 = AvatarBonePositions[16];
        s.bone18 = AvatarBonePositions[17];

    }


    public void LoadData(SaveLists s)
    {
        //AvatarController.Instance.AvatarBonePositionParticipant.Add(s.AvatarBonePositions);
        //DecompileCarList(s);
        DecompileInteriorHeatmap(s);
        DecompileTouchHeatmap(s);
        DecompileAvatar(s);
        Debug.Log("DONE");
    }

    public void DecompileCarList(SaveLists s)
    {
        List<(double, Vector3, Vector3)> CarPositions = new List<(double, Vector3, Vector3)>();
        for (int i = 0; i < s.CarPositionsPos.Count; i++)
        {
            (double, Vector3, Vector3) tuple;
            tuple.Item1 = s.CarPositionsTime[i];
            tuple.Item2 = s.CarPositionsPos[i];
            tuple.Item3 = s.CarPositionsRot[i];
            CarPositions.Add(tuple);
        }
        HeatmapController.Instance.allCarPos = CarPositions;

    }

    public void DecompileInteriorHeatmap(SaveLists s)
    {
     List<(string, Vector4)> InteriorHeatmap = new List<(string, Vector4)>();
        for (int i = 0; i < s.InteriorHeatmapString.Count; i++)
        {
            (string, Vector4) tuple;
            tuple.Item1 = s.InteriorHeatmapString[i];
            tuple.Item2 = s.InteriorHeatmapVec4[i];
            InteriorHeatmap.Add(tuple);
        }

        HeatmapController.Instance.InitInteriorHeatmap(InteriorHeatmap, 0);

    }

    public void DecompileAvatar(SaveLists s)
    {
        List<List<Vector3>> AvatarBonePositions = new List<List<Vector3>>();
        AvatarBonePositions.Add(s.bone1);
        AvatarBonePositions.Add(s.bone2);
        AvatarBonePositions.Add(s.bone3);
        AvatarBonePositions.Add(s.bone4);
        AvatarBonePositions.Add(s.bone5);
        AvatarBonePositions.Add(s.bone6);
        AvatarBonePositions.Add(s.bone7);
        AvatarBonePositions.Add(s.bone8);
        AvatarBonePositions.Add(s.bone9);
        AvatarBonePositions.Add(s.bone10);
        AvatarBonePositions.Add(s.bone11);
        AvatarBonePositions.Add(s.bone12);
        AvatarBonePositions.Add(s.bone13);
        AvatarBonePositions.Add(s.bone14);
        AvatarBonePositions.Add(s.bone15);
        AvatarBonePositions.Add(s.bone16);
        AvatarBonePositions.Add(s.bone17);
        AvatarBonePositions.Add(s.bone18);
        BuildAfterDecompile(AvatarBonePositions);

        //AvatarController.Instance.AvatarBonePositionParticipant.Add(AvatarBonePositions);
       // Debug.Log(AvatarBonePositions.Count);
       // AvatarController.Instance.AvatarBonePositionParticipant.Add(AvatarBonePositions);
        //AvatarController.Instance.InitAvatar(AvatarBonePositions,0);
    }


    public void BuildAfterDecompile(List<List<Vector3>> avat)
    {
        List<List<Vector3>> complete = new List<List<Vector3>>();
        for(int i = 0; i < avat[0].Count; i++)
        {
            List<Vector3> timestamp = new List<Vector3>();
            for(int j = 0; j< avat.Count; j++)
            {
                timestamp.Add(avat[j][i]);
            }
            complete.Add(timestamp);
        }
        AvatarController.Instance.InitAvatar(complete, 0);

    }

    public void DecompileTouchHeatmap(SaveLists s)
    {
     List<(string, Vector4)> TouchHeatmap = new List<(string, Vector4)>();
        for (int i = 0; i < s.TouchHeatmapString.Count; i++)
        {
            (string, Vector4) tuple;
            tuple.Item1 = s.TouchHeatmapString[i];
            tuple.Item2 = s.TouchHeatmapVec4[i];
            TouchHeatmap.Add(tuple);
        }

        HeatmapController.Instance.InitTouchInteriorHeatmap(TouchHeatmap, 0);
    }




}
