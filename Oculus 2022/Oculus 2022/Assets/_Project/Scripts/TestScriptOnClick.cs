using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TestScriptOnClick : MonoBehaviour, IPointerClickHandler
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        //Output to console the clicked GameObject's name and the following message. You can replace this with your own actions for when clicking the GameObject.
        Debug.Log(name + " Game Object Clicked!");
    }

    public void doThing(GameObject g)
    {
        Debug.Log("Game Object Clicked!");
        Debug.Log(g.name);
    }

    public void doThing2()
    {
        Debug.Log("Game Object Clicked!");
        Debug.Log(this.gameObject);
    }
}
