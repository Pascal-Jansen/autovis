# AutoVis - Oculus

AutoVis - Oculus is a Unity-based program for analyzing recorded data in JSON format to display spatial data of an individual inside a car. The program uses Oculus VR headset and provides an immersive experience while inside the car. The user can see touch heatmaps, gaze heatmaps, and avatar movement in different views, and can control the time with a simple menu to see the data at different times. Additionally, the user can also move or reset the position of the camera to manually synchronize with the data.

## Getting Started

To use AutoVis - Oculus, you will need to have Unity and an Oculus VR headset installed on your machine. Once Unity is installed, you can download the AutoVis - Oculus project files and open the project in Unity.

### Prerequisites
- Unity
- Oculus VR headset

### Installing

1. Download the AutoVis - Oculus project files from Gitlab.
2. Open Unity and select "Open" from the File menu.
3. Navigate to the folder where you downloaded the AutoVis - Oculus project files, and select the "AutoVis - Oculus" folder.

### Usage
1. Connect your Oculus VR headset to your computer.
2. In Unity, navigate to the "Assets/Scenes" folder and open the "Main" scene.
3. Change the build settings to "Android" to be able to build the APK on the Oculus.
4. Press the play button in Unity to start the scene.
5. Use your Oculus VR headset to experience the touch heatmaps, gaze heatmaps, and avatar movement while inside the car.
6. Use the simple menu to control the time and switch between different views.
7. Move or reset the position of the camera to manually synchronize with the data.

## File Structure
TODO
<!-- - Assets
  - Scenes
    - Main.unity
  - Materials
  - Prefabs
- Oculus Utilities for Unity
- ProjectSettings -->

## Authors
- Alexander Häusele
- Thilo Segschneider

