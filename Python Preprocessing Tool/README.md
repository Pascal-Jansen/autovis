# AutoVis - Python Preprocessing Tool

This tool is designed to analyze video files using pre-trained machine learning models such as YOLO, DeepFace, etc. The tool provides a list of different machine learning algorithms to select from, takes a video file as input, processes it using the selected algorithm, and exports a JSON file with timestamps and the results of the analysis into the output folder.

## Prerequisites
- Python 3.x
- OpenCV
- TensorFlow
- Keras
- Pre-trained models such as YOLO and DeepFace (links provided below)

## Getting Started
1. Download the AutoVis - Python Preprocessing Tool files from Gitlab.
2. Install the necessary dependencies using pip or another package manager.
3. Download the pre-trained models (YOLO, DeepFace, etc) and place them in the appropriate folder.

## Running the tool
- To start the tool, run the command `python main.py` in the command prompt or terminal window.
- The tool will show a list of different machine learning algorithms.
- Select the desired algorithm and select the video file you want to analyze.
- Click the "Run" button to start the analysis process.
- The output JSON file will be saved in the output folder.


## Pre-trained Models
- [YOLO](https://github.com/AlexeyAB/darknet)
- [DeepFace](https://github.com/serengil/tensorflow-101/tree/master/python/deepface)
- [Other pre-trained models](https://github.com/tensorflow/models)

## File Structure
├── drowsiness_detection/<br>
├── gesture_recognition/<br>
├── input/<br>
├── OpenPose2/<br>
├── output/<br>
├── real_time_driver_state_detection/<br>
├── Recordings/<br>
├── Stress_Detector_master/<br>
├── main.py<br>
└── README.md<br>

- `drowsiness_detection/`: Contains all data for the drowsiness detection algorithm.
- `gesture_recognition/`: Contains all data for the gesture recognition algorithm.
- `input/`: Contains multiple input folder with sample data for easier import into the Program.
- `OpenPose2/`: Contains all data for the OpenPose algorithm.
- `output/`: Output folder. Contains multiple folder for each algorithm.
- `real_time_driver_state_detection/`: The main css file
- `Recordings/`: Sample data for testing purposes.
- `Stress_Detector_master/`: Contains all data for the stress detection algorithm.
- `main.py`: The main python file
- `README.md`: This readme file

## Authors
- Alexander Häusele
- Thilo Segschneider

## Usage

`python main.py`