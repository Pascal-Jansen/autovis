# AutoVis

AutoVis is a suite of tools for analyzing recorded data in JSON format to display spatial data of an individual inside a car. The tools include a Unity program for Oculus VR, a machine learning/deep learning tool, a project server, and a VR/browser application. The Unity program uses Oculus VR headset and the Oculus Integration Toolkit, it allows the user to see touch heatmaps, gaze heatmaps, and avatar movement while inside the car, also the user can move or reset the position of the camera to manually synchronize with the data. The machine learning/deep learning tool uses pre-trained models such as YOLO, DeepFace, etc. to analyze video files and exports a JSON file with timestamps and results. The project server is a Node.js based server that is used to start the Unity program in browser and VR view. The VR/browser application allows the user to view the data in a browser and VR view, it needs the project server to be started to have a html with 2D graphs on the left controller that are made with D3.

## Getting Started

To use AutoVis, you will need to have Unity, Oculus VR headset, and Node.js installed on your machine. 

### Prerequisites
- Unity
- Oculus VR headset (only needed for the Oculus application)
- VR headset (only needed for the VR Scene)
- Node.js

### Usage
1. Download the AutoVis project files from Gitlab.
2. See the specific Gitlab readme files for installation and usage instructions:
- [AutoVis - Main Application](AutoVis%20-%203D%20View/)
- [AutoVis - Oculus](Oculus%202022/Oculus%202022/)
- [AutoVis - Python Preprocessing Tool](Python%20Preprocessing%20Tool/)
- [AutoVis - Web application](Web%20application/)


## Authors
- Alexander Häusele
- Thilo Segschneider
- Julian Britten
- Pascal Jansen
