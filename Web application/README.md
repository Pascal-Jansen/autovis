# AutoVis - Web application


This is a guide for setting up the server for the AutoVis project. The server is used to run the browser and VR views in the AutoVis Unity program.

## Prerequisites
- Node.js

## Getting Started
1. Download the AutoVis project files from Gitlab.
2. Open a command prompt or terminal window, navigate to the folder where you downloaded the AutoVis project files.
3. Run the command `npm install` to install the necessary dependencies.

## File Structure

├── Data/<br>
├── Recordings/<br>
├── resources/<br>
├── Scripts/<br>
├── Videos/<br>
├── WebGL/<br>
├── Scripts/<br>
├── index.html<br>
├── style.css<br>
├── server.js<br>
├── gridstack.css<br>
├── package.json<br>
└── README.md<br>

- `Data/`: Contains JSON files.
- `Recordings/`: Contains JSON files for Unity WebGL (Obsolete).
- `resources/`: Contains static assets such images used.
- `Scripts/`: Contains Javascript files.
- `Videos`: Contains recorded Video files.
- `WebGL`: Contains component for displaying 3D visualization. (Obsolete);
- `index.html`: The main html file
- `style.css`: The main css file
- `server.js`: The main javascript file for running the server.
- `gridstack.css`: css file created by gridstack for gridlayout
- `package.json`: npm package file
- `README.md`: This readme file

## Running the server
- To start the server, run the command `node server.js` in the command prompt or terminal window.
- The server will start and will be listening on port 8000 by default.

## Authors
- Alexander Häusele
- Thilo Segschneider
