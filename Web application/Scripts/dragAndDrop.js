

$(document).ready(function () {
  document.getElementsByClassName("deleteContainer")[0].addEventListener('click', deleteThisContainer);

  initDroppable();

  // function initDroppableLeft() {
  //   $(".dropzoneLeft").droppable({
  //     hoverClass: 'ui-state-highlight',
  //     tolerance: "pointer",
  //     drop: function (ev, ui) {
  //       // console.log(ui.draggable[0].id);
  //       var data = ui.draggable[0].id;
  //       var demoGraph = document.getElementById(data);
  //       var graphContainer = document.getElementById("graphContainer");
  //       // graphContainer.style.gridTemplateColumns = "1fr 1fr";
  //       if (demoGraph.style.gridColumnStart != 2) {
  //         // demoGraph.style.gridColumnStart = 2;
  //         counter++;
  //       }

  //       var graphContainerAll = document.getElementById("graphContainerAll");


  //       graphContainerAll.style.gridTemplateColumns += " 1fr";

  //       let graphDiv = document.createElement("div");
  //       graphDiv.id = "graphContainer" + counter;
  //       graphDiv.setAttribute("class", "graphContainer");
  //       graphDiv.style.gridTemplateColumns = "1fr";

  //       let leftZone = document.createElement("div");
  //       leftZone.setAttribute("class", "dropzoneLeft");

  //       let rightZone = document.createElement("div");
  //       rightZone.setAttribute("class", "dropzoneRight");

  //       graphContainerAll.appendChild(graphDiv);

  //       $(".graphContainer").sortable({
  //         connectWith: ".graphContainer"
  //       });

  //       setTimeout(function () {
  //         graphDiv.appendChild(rightZone);
  //         graphDiv.appendChild(leftZone);
  //         graphDiv.appendChild(demoGraph);
  //         initDroppableLeft();
  //         ev.preventDefault();
  //         return;
  //       }, 1);

  //       // console.log(demoGraph.parentElement);
  //     }
  //   });
  // }
});

function setupSortable() {
  $(".graphContainer").sortable({
    connectWith: ".graphContainer",
  });
  // $(".graphContainer").disableSelection();
  $(".demoGraphDiv").draggable({
    // helper: function (event) {
    //   return $("<div class='ui-widget-header'>I'm a custom helper</div>");
    // },
    helper: 'clone',
    connectToSortable: ".graphContainer",
    // containment: "#container",
    // revert: 'invalid'
  });
}

var counter = 0;
async function initDroppable() {

  $(".dropzone").droppable({
    accept: '.demoGraphDiv',
    hoverClass: 'ui-state-highlight',
    tolerance: "pointer",
    drop: async function (ev, ui) {
      var data = ui.draggable[0].id;
      var demoGraph = document.getElementById(data);
      if (ui.draggable[0].classList.contains("dragSingleId")) {
        return;
      }
      var graphContainer = document.getElementById("graphContainer");
      // graphContainer.style.gridTemplateColumns = "1fr 1fr";
      // if (demoGraph.style.gridColumnStart != 2) {
      //   // demoGraph.style.gridColumnStart = 2;
      //   counter++;
      // }
      counter++;
      var graphContainerAll = document.getElementById("graphContainerAll");


      graphContainerAll.style.gridTemplateColumns += " 1fr";
      document.getElementById("graphContainerFixed").style.gridTemplateColumns += " 1fr";

      let graphDiv = document.createElement("div");
      graphDiv.id = "graphContainer" + counter;
      graphDiv.setAttribute("class", "graphContainer");
      graphDiv.style.gridTemplateColumns = "1fr";

      // let leftZone = document.createElement("div");
      // leftZone.setAttribute("class", "dropzoneLeft dropzone");

      let rightZone = document.createElement("div");
      rightZone.setAttribute("class", "dropzoneRight dropzone");

      let deleteContainer = document.createElement("div");
      deleteContainer.setAttribute("class", "deleteContainer");
      //deleteContainer.setAttribute('onclick', function () { deleteContainer(); });
      deleteContainer.onclick = function (d) { deleteThisContainer(d) };

      let inputText = document.createElement("input");
      inputText.setAttribute("class", "textInputGraphContainer");
      inputText.setAttribute("value", "placeholder");



      if (ev.target.classList.contains("dropzoneLeft")) {
        // graphContainerAll.insertBefore(graphDiv, document.getElementById(ev.target.parentElement.id));
        graphContainerAll.prepend(graphDiv);
        addColumn();
      } else {
        graphContainerAll.insertBefore(graphDiv, document.getElementById(ev.target.parentElement.id).nextSibling);
        addColumn();
      }

      setTimeout(function () {
        graphDiv.appendChild(inputText);
        graphDiv.appendChild(deleteContainer);
        graphDiv.appendChild(rightZone);
        // graphDiv.appendChild(leftZone);
        graphDiv.appendChild(demoGraph);
        initDroppable();
        ev.preventDefault();
        enableDeleteContainer();

      }, 1);

      $(".graphContainer").sortable({
        connectWith: ".graphContainer",
      });

      doDragandDrop();
      draw("E4Data.json", addToGraphsList, true, 0);
      updateMarkers();
      // drawTimeline();

      // $(".graphContainer").sortable({
      //   connectWith: ".graphContainer",
      //   update: function () {
      //     let container = document.getElementsByClassName("graphContainer");
      //     for (let i = 0; i < container.length; i++) {
      //       if ($(container[i].children).hasClass("demoGraphDiv")) {
      //         // console.log(1);
      //       } else {
      //         // console.log(0);
      //       }
      //       if (container[i].getElementsByClassName("demoGraphDiv").length == 1) {
      //         console.log(container[i])
      //       }
      //     }
      //   }
      // });
      //}



    }
  });
}

function enableDeleteContainer() {
  $(".deleteContainer").css("display", "block");
  $(".deleteContainer").hover(
    function () {
      this.parentElement.classList.add("graphContainerHighlited");
    }, function () {
      this.parentElement.classList.remove("graphContainerHighlited");
    }
  );
}

async function deleteThisContainer(d) {
  var graphContainerAll = document.getElementById("graphContainerAll");
  var chosenContainer = document.getElementById(d.target.parentElement.id);
  removeColumn();
  // drawTimeline();

  var currentColums = graphContainerAll.style.gridTemplateColumns.split(' ').length - 1;
  var columns = "";
  for (var i = 1; i <= currentColums; i++) {
    if (i == 1) {
      columns += "1fr";
    }
    else {
      columns += " 1fr";
    }
  }

  let label = chosenContainer.getElementsByClassName("labelText");
  for (let i = 0; i < label.length; i++) {
    let graphName = label[i].innerHTML.split(' ')[0];
    const index = addToGraphsList.indexOf(graphName);
    if (index > -1) {
      addToGraphsList.splice(index, 1);
    }
  }

  chosenContainer.remove();
  let remainingGraphs = document.getElementsByClassName("demoGraphDiv");
  let ids = [];
  for (let i = 0; i < remainingGraphs.length; i++) {
    ids.push(remainingGraphs[i].id);
  }

  ids.sort();
  for (let i = 0; i < remainingGraphs.length; i++) {
    remainingGraphs[i].id = "graph" + ids.indexOf(remainingGraphs[i].id);
  }

  graphContainerAll.style.gridTemplateColumns = columns;
  document.getElementById("graphContainerFixed").style.gridTemplateColumns = columns;

  let pinnedGraphs = document.getElementsByClassName("fixed");

  let newColumn = 1;
  for (let i = 0; i < pinnedGraphs.length; i++) {
    let pinnedGraph = pinnedGraphs[i];
    pinnedGraph.style.gridColumn = newColumn + " / span 1";
    newColumn++;
    if (newColumn == currentColums + 1) {
      newColumn = 1;
    }
  }
  draw("E4Data.json", addToGraphsList, true, 0);


  if (document.getElementById("graphContainerAll").children.length == 2) {
    $(".deleteContainer").css("display", "none");
  }
  updateMarkers();
}

// function allowDrop(ev) {
//   ev.preventDefault();
//   let dropzoneHighlight;
//   switch (ev.target.id) {
//     case "dropzoneAll":
//       dropzoneHighlight = document.getElementById("dropzoneHighlightRight");
//       dropzoneHighlight.style.display = "block";
//       break;
//     case "dropzoneLeft":
//       dropzoneHighlight = document.getElementById("dropzoneHighlightRight");
//       dropzoneHighlight.style.display = "block";
//       break;
//     case "dropzoneRight":
//       console.log("Test");
//       dropzoneHighlight = document.getElementById("dropzoneHighlightRight");
//       dropzoneHighlight.style.display = "block";
//       break;
//   }
// }

// function drag(ev) {
//   ev.dataTransfer.setData("text", ev.target.id);
//   let dropzone = document.getElementById("dropzoneRight");
//   dropzone.style.zIndex = 100;
//   let graph = document.getElementById(ev.target.id);
//   graph.style.zIndex = 102;
// }

// function hideDropzone(ev) {
//   ev.preventDefault();
//   let dropzoneHighlight;
//   switch (ev.target.id) {
//     case "dropzoneAll":
//       dropzoneHighlight = document.getElementById("dropzoneHighlightRight");
//       dropzoneHighlight.style.display = "none";
//       break;
//     case "dropzoneLeft":
//       dropzoneHighlight = document.getElementById("dropzoneHighlightRight");
//       dropzoneHighlight.style.display = "none";
//       break;
//     case "dropzoneRight":
//       dropzoneHighlight = document.getElementById("dropzoneHighlightRight");
//       dropzoneHighlight.style.display = "none";
//       break;
//   }
// }

// function drop(ev) {
//   ev.preventDefault();
//   let dropzone = document.getElementById("dropzoneHighlightRight");
//   dropzone.style.display = "none";

//   if (ev.dataTransfer.getData("text").includes("graph")) {
//     var data = ev.dataTransfer.getData("text");
//     var demoGraph = document.getElementById(data);
//     var graphContainer = document.getElementById("graphContainer");
//     graphContainer.style.gridTemplateColumns = "1fr 1fr";
//     var timeline = document.getElementById("timelineContainer");
//     timeline.style.gridTemplateColumns = "1fr 1fr";
//     var eventline = document.getElementById("eventlineContainer");
//     eventline.style.gridTemplateColumns = "1fr 1fr";
//     demoGraph.style.gridColumnStart = 2;

//     let dropzone = document.getElementById("dropzoneRight");
//     dropzone.style.zIndex = 1;
//     // ev.target.appendChild(document.getElementById(data));
//   }
// }

// function showBorder(d) {
//   console.log("test");
//   console.log(this);
//   console.log(d);
// }


