function makeDraggable() {
    $(".dragSingleId").draggable({
        helper: 'clone',
        connectToSortable: ".graphContainer",
    });
}


var newItem;
function doDragandDrop() {

    $(".graphContainer").sortable({
        connectWith: ".graphContainer",
        hoverClass: 'ui-state-highlight',
        // tolerance: "pointer",
        beforeStop: function (event, ui) {
            if (ui.item[0].classList.contains("dragSingleId")) {
                newItem = ui.item[0];
                newItem.setAttribute("id", "clone");
            }
        },
        receive: async function (ev, ui) {


            if (ui.item[0].classList.contains("dragSingleId")) {
                if (ui.item[0].classList.contains("3d")) {
                    newItem.remove();
                    return;
                }
                var singleList = [];
                singleList = [ui.item[0].getAttribute("nameid")];
                addToGraphsList.push(ui.item[0].getAttribute("nameid"));
                await setupHorizontal(singleList, false);

                var p = new Promise((resolve, reject) => {
                    draw("E4Data.json", singleList, false, 0)
                    resolve();
                });
                p.then(function (v) {
                    // drawid++;
                    var id = "graph" + (idSetup - 1);
                    var createdGraph = document.getElementById("clone");
                    $("#" + id).insertAfter($("#clone"));
                    newItem.remove();
                    return;
                })
            }
        }
    }).droppable({
        accept: '.2d'
    });
}

/**
 * Highlights the Dropdown elements of all graphs that are currently drawn
 */
function loadList() {
    addToGraphsList.forEach(listentry => {
        let graphs = d3.selectAll(".dragSingleId")
            .filter(function () {
                return d3.select(this).attr("nameid") == listentry; // filter by dataType
            })

        var li = (graphs._groups)[0][0];
        li.style.backgroundColor = "green";
    });
    //document.getElementById("Empatica_3d").children[0].style.backgroundColor = "green";
}

/**
 * Generates dropdown elements for all datatypes
 */
function generateDropdown() {
    d3.json("./Data/E4_Part1.json").then(function (data) {
        data.forEach(dataset => {
            createDropdownElement(dataset.dataCategory, dataset.dataType, "2d");
            let category = d3.select(".clickable2d");
            category.on("click", function () {
                selectall(this.innerHTML, "2d");
            });
            if (dataset.dimension == "3d") {
                createDropdownElement(dataset.dataCategory, dataset.dataType, "3d");
                category = d3.select(".clickable3d");
                category.on("click", function () {
                    selectall(this.innerHTML, "3d");
                });
            }
        });
        loadList();
        makeDraggable();
    });
}

/**
 * Creates a single dropdown element  for the given data type within the given dimension and data category
 * @param {String} dataCategory The category of the given dataset stored in the json file
 * @param {String} dataType The data type of the given dataset stored in the json file
 * @param {String} dimension The dimension of the given dataset stored in the json file
 */
function createDropdownElement(dataCategory, dataType, dimension) {
    let category = d3.select("#" + dataCategory + "_" + dimension);
    category.append("li")
        .attr("class", "dragSingleId " + dimension)
        .attr("dimension", dimension)
        .attr("nameid", dataType)
        .on("click", function () {
            if (this.style.backgroundColor != "green") {
                this.style.backgroundColor = "green";
                if (this.getAttribute("dimension") == "2d") {
                    addSingleGraph(dataType);
                } else if (this.getAttribute("dimension") == "3d") {
                    startthis("scatter3d");
                }
            }
            else {
                this.style.backgroundColor = "var(--elementBasic)";
                if (this.getAttribute("dimension") == "2d") {
                    removeSingleGraph(dataType);
                } else if (this.getAttribute("dimension") == "3d") {
                    document.getElementById("myDiv").children[0].remove();
                }
            }
        })
        .html(dataType);
}

/**
 * If not all data types of the given category and dimension are drawn, draws the missing graphs.
 * If all data types of the given category and dimension are drawn, removes all related graphs.
 * @param {String} dataCategory The category of the given dataset stored in the json file
 * @param {String} dimension The dimension of the given dataset stored in the json file
 */
async function selectall(dataCategory, dimension) {
    if (dataCategory != "Personal") {
        let counterforChildren = 0;
        let category;
        if (dimension == "2d") {
            category = document.getElementById(dataCategory + "_2d");
        } else {
            category = document.getElementById(dataCategory + "_3d");
        }

        let children = category.children;
        let dataToAdd = [];
        for (let i = 0; i < children.length; i++) {
            let liElement = children[i];

            if (liElement.style.backgroundColor == "green") {
                counterforChildren++;
            }
            if (!(liElement.style.backgroundColor == "green")) {
                liElement.style.backgroundColor = "green";
                dataToAdd.push(liElement.innerHTML);
            }
        }
        if (counterforChildren == children.length) {
            removeall(dataCategory, dimension);
        } else {
            dataToAdd.forEach(data => {
                addToGraphsList.push(data);
            });
            await setupHorizontal(dataToAdd, false);
            draw("E4Data.json", dataToAdd, false, 0);
        }
    }
}

/**
 * Removes all graphs with the given category and dimension
 * @param {String} dataCategory The category of the given dataset stored in the json file
 * @param {String} dimension The dimension of the given dataset stored in the json file
 */
function removeall(dataCategory, dimension) {
    let category;
    if (dimension == "2d") {
        category = document.getElementById(dataCategory + "_2d");
    } else {
        category = document.getElementById(dataCategory + "_3d");
    }

    let children = category.children;
    for (let i = 0; i < children.length; i++) {
        let liElement = children[i];
        if (liElement.style.backgroundColor == "green") {
            let dataType = liElement.innerHTML;
            liElement.style.backgroundColor = "var(--elementBasic)";
            removeSingleGraph(dataType);
        }
    }
}


/**
 * Adds a single graph of the given data type
 * @param {String} dataType The data type of the given dataset stored in the json file
 */
async function addSingleGraph(dataType) {
    let singleList = [];
    singleList.push(dataType);
    addToGraphsList.push(dataType);
    await setupHorizontal(singleList, false);
    draw("E4_Part1.json", singleList, false, 0);
    draw("E4_Part2.json", singleList, false, 1);
    draw("E4_Part3.json", singleList, false, 2);
}

/**
 * Removes a single graph of the given data type
 * @param {String} dataType The data type of the given dataset stored in the json file
 */
function removeSingleGraph(dataType) {
    if (addToGraphsList.includes(dataType)) {
        let graphs = document.getElementsByClassName("demoGraphDiv");
        for (let i = 0; i < graphs.length; i++) {
            let graph = graphs[i];
            if (graph.getAttribute("datatype") == dataType) {
                graph.remove();
                addToGraphsList = addToGraphsList.filter(function (value, index, arr) {
                    return value != dataType;
                });
            }
        }
    }
}