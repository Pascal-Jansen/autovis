// var width;
// var containerWidth;

// setValues()

// function setValues() {
//     d3.json("./Data/E4Data.json").then(function (data) {
//         // get the max and min x-value of the dataset
//         data.forEach(dataset => {
//             if (dataset.dataType != "Meta Data") {
//                 dataset.data.forEach(value => {
//                     maxX = Math.max(maxX, Math.max(value.valueX));
//                     minX = Math.min(minX, Math.min(value.valueX));
//                 });
//             }
//         });
//     });
// }
var highlightedColor3d = "green";
var dashes = ["5,5", "10,10", "20,10,5,5,5,10", "0"];
var customEvents = [];

// drawEvents("events.json", ["Emotions", "BodyMotion", "Objects"]);

async function drawEventsView(filename, eventNames) {
    return new Promise((resolve, reject) => {
        if (arguments.length < 2) {
            eventNames = eventList;
        }
        d3.selectAll('#eventContainer').selectAll(".eventDiv").remove();
        // d3.selectAll('#eventlineContainer').selectAll(".eventline").remove();


        //d3.selectAll('#eventlineContainer').selectAll("svg").remove();

        var eventlines = document.getElementsByClassName("eventSvg");

        let div = d3.select("#eventContainer")
            .append("div")
            .attr("class", "eventDiv")
            .attr("id", "mainEventLine")
            .style("display", "flex");

        // div.append("div")
        //     .attr("class", "eventLabel")
        //     .html("All");

        containerWidth = document.getElementById("timelineView").getBoundingClientRect().width;
        containerTop = document.getElementById("timelineView").getBoundingClientRect().top;
        containerLeft = document.getElementById("timelineView").offsetLeft;
        let eventWidth = containerWidth - margin.left - margin.right;


        let svg = div
            .append("svg")
            .attr("class", "eventSvg");

        let eventCounter = 0;

        d3.json("./Data/" + filename).then(function (data) {
            data.forEach(dataset => {
                let values = dataset.data;
                let name = dataset.dataType;

                let xScale = d3.scaleLinear()
                    .domain([minX, maxX])
                    .range([margin.left, eventWidth])

                let newTicks;

                newTicks = xScale.ticks()
                newTicks.unshift(xScale.domain()[0]);
                newTicks.push(maxX);

                let xAxis = d3.axisBottom(xScale)
                    .tickValues(newTicks)
                    .tickFormat(function (d, i, n) {

                        let minXnew = minX;

                        var date_beginning = new Date(minXnew * 1000);
                        // Hours part from the timestamp
                        var hours_beginning = date_beginning.getHours();
                        // Minutes part from the timestamp
                        var minutes_beginning = "0" + date_beginning.getMinutes();
                        // Seconds part from the timestamp
                        var seconds_beginning = "0" + date_beginning.getSeconds();

                        let unix_timestamp = d;
                        // Create a new JavaScript Date object based on the timestamp
                        // multiplied by 1000 so that the argument is in milliseconds, not seconds.
                        var date = new Date(unix_timestamp * 1000);
                        // Hours part from the timestamp
                        var hours = date.getHours();
                        // Minutes part from the timestamp
                        var minutes = "0" + date.getMinutes();
                        // Seconds part from the timestamp
                        var seconds = "0" + date.getSeconds();

                        // var date_difference = new Date(minXnew * 1000);
                        // // Hours part from the timestamp
                        // var hours_difference = date.getHours() - date_beginning.getHours();
                        // // Minutes part from the timestamp
                        // var minutes_difference = "0" + (date.getMinutes() - date_beginning.getMinutes());
                        // // Seconds part from the timestamp
                        // var seconds_difference = "0" + (date.getSeconds() - date_beginning.getSeconds());

                        var dateoffset = new Date();
                        var date_difference = new Date((d - minXnew) * 1000);

                        //date_difference = date_difference.getTimezoneOffset();
                        var hours_difference = date_difference.getUTCHours();
                        // Minutes part from the timestamp
                        var minutes_difference = "0" + date_difference.getUTCMinutes();
                        // Seconds part from the timestamp
                        var seconds_difference = "0" + date_difference.getUTCSeconds();


                        // Will display time in 10:30:23 format
                        var formattedTime = hours + ': \n ' + minutes.substr(-2) + ': \n ' + seconds.substr(-2);

                        var formattedTime_difference = hours_difference + ': \n ' + minutes_difference.substr(-2) + ': \n ' + seconds_difference.substr(-2);


                        if (timeset) {
                            return formattedTime_difference;
                        }
                        else {
                            return formattedTime;
                        }

                    });

                for (let i = 0; i < values.length; i++) {
                    d3.select("#mainEventLine").select(".eventSvg").append("rect")
                        .attr("class", function () { return values[i].valueY })
                        .attr("y", -50)
                        .attr("x", function () { return xScale(values[i].valueX) })
                        .attr("width", function () {
                            var value1 = 0;
                            var value2 = 0;
                            if (i < values.length - 1) {
                                value1 = xScale(values[i + 1].valueX);
                                value2 = xScale(values[i].valueX);
                            }
                            return Math.abs(value1 - value2);
                        })
                        .attr("height", 50)
                        .attr("fill", function () {
                            switch (values[i].valueY) {
                                case "Happy":
                                    return "green";
                                case "Anger":
                                    return "red"
                                case "ButtonInteraction":
                                    return "blue";
                                case "StopSign":
                                    return "white";
                                case "Car":
                                    return "black";
                                case "Motorcycle":
                                    return "yellow";

                            }
                        })
                        .attr("onmouseover", "highlight3d(this)")
                        .attr("onmouseout", "unhighlight3d(this)")
                        .style("display", function () {
                            if (eventNames.includes(name)) {
                                return "inline";
                            } else {
                                return "none";
                            }
                        })
                        .style("opacity", 0.5);
                }

                if (eventNames.includes(name)) {


                    let div = d3.select("#eventContainer")
                        .append("div")
                        .attr("class", "eventDiv")
                        .style("position", "absolute")
                        .style("left", 0 + "px")
                        .style("top", containerTop + "px")
                        .style("display", "none");

                    // div.append("div")
                    //     .attr("class", "eventLabel")
                    //     .html(name);

                    let svg = div
                        .append("svg")
                        .attr("class", "eventSvg");

                    d3.selectAll(".eventSvg")
                        .attr("transform", `translate(0, 20)`)
                        .attr("width", eventWidth)
                        .call(xAxis);





                    svg.selectAll("rect")
                        .data(values)
                        .enter()
                        .append("rect")
                        .attr("class", function (d) { return d.valueY })
                        .attr("y", -50)
                        .attr("x", function (d) { return xScale(d.valueX) })
                        .attr("width", function (d, i) {
                            var value1 = 0;
                            var value2 = 0;
                            if (i < values.length - 1) {
                                value1 = xScale(values[i + 1].valueX);
                                value2 = xScale(d.valueX);
                            }
                            return Math.abs(value1 - value2);
                        })
                        .attr("height", 50)
                        .attr("fill", function (d) {
                            switch (d.valueY) {
                                case "Happy":
                                    return "green";
                                case "Anger":
                                    return "red"
                                case "ButtonInteraction":
                                    return "blue";
                                case "StopSign":
                                    return "white";
                                case "Car":
                                    return "black";
                                case "Motorcycle":
                                    return "yellow";

                            }
                        })
                        .attr("onmouseover", "highlight3d(this)")
                        .attr("onmouseout", "unhighlight3d(this)")
                        .style("opacity", 0.5);

                    var singleGraph = d3.selectAll(".eventSvg");
                    singleGraph.each(function (d, i) {
                        if (i == eventCounter && i != 0) {
                            d3.select(this).append("text")
                                .attr("x", (50))
                                .attr("y", -55)
                                .attr("text-anchor", "left")
                                .attr("class", "labelText mediumText")
                                .style("font-size", "20px")
                                .style("fill", "black")
                                .text(function (d) {
                                    return name;
                                });
                            eventCounter++;

                        } else if (i == 0 && eventCounter == 0) {
                            d3.select(this).append("text")
                                .attr("x", (50))
                                .attr("y", -55)
                                .attr("text-anchor", "left")
                                .attr("class", "labelText mediumText")
                                .style("font-size", "20px")
                                .style("fill", "black")
                                .text(function (d) {
                                    return "All";
                                });
                            eventCounter++;
                        }
                        // if (i == eventCounter) {

                        // }
                    })



                    resolve();
                }


            });
        });
    });
    // d3.select("#mainEventLine").on("click", function () {
    //     addonclickforEvents();
    // });

}

var isExploded = false;
function addonclickforEvents() {
    if (isExploded) {
        for (let i = 1; i < containers.length; i++) {
            reconstruct(i)
        }
        isExploded = false;
    } else {
        document.getElementById("mainEventLine").style.backgroundColor = "var(--elementBright)";

        for (let i = 1; i < containers.length; i++) {
            explosion(i)
        }
        isExploded = true;
    }
}

/**
 * draws the events on the event lines
 * @param {string} filename the name of the file that the event data is stored in
 * @param {Array} eventNames the names of the event types that should be drawn
 */
function drawEventsGrid(filename, partNumber) {
    eventNames = eventList;


    d3.selectAll('#eventViewContainer' + partNumber).selectAll("svg").remove();


    eventlineContainer = d3.select("#eventViewContainer" + partNumber);
    let eventline = eventlineContainer.append("svg")
        .attr("id", "eventlineId" + partNumber)
        .attr("class", "eventlineView")
        .attr("overflow", "hidden")
        .style("height", "50px");

    eventline.append("line")
        .style("stroke", "orange")
        .style("stroke-width", 2)
        .style("display", "none")
        .attr("class", "eventMarker")
        .attr("x1", 10)
        .attr("y1", 0)
        .attr("x2", 10)
        .attr("y2", 50);

    eventline.append("line")
        .style("stroke", "red")
        .style("stroke-width", 2)
        .style("display", "block")
        .attr("class", "eventMarkerPlay")
        .attr("x1", margin.left)
        .attr("y1", 0)
        .attr("x2", margin.left)
        .attr("y2", 50);


    // d3.selectAll('#eventlineContainer').selectAll("svg").selectAll("rect").remove();
    d3.json("./Data/" + filename).then(function (data) {
        let dataCounter = 0;
        data.forEach((dataset, f) => {
            let values = dataset.data;
            let name = dataset.dataType;

            // d3.selectAll('#eventlineContainer').selectAll("svg").selectAll("rect").remove();
            let eventViewWidth = document.getElementById("timelineView").getBoundingClientRect().width;
            let eventViewHeight = document.getElementById("timelineView").getBoundingClientRect().height;

            let eventWidth = eventViewWidth - margin.left - margin.right;


            let xScale = d3.scaleLinear()
                .domain([minX, maxX])
                .range([margin.left, eventWidth]);



            // for (let j = 0; j < columnCount; j++) {

            let svg = d3.selectAll(".eventlineView")
                .attr("transform", `translate(0, 20)`)
                .attr("width", eventViewWidth)
                .call(d3.axisBottom(xScale));

            let newRec = true;
            let widthX;
            let startX;


            var usedDashes = [];

            eventline.append("image")
                .attr("width", "10")
                .attr("height", "10")
                .attr("x", "10")
                .attr("y", 10 * f)
                .attr("href", function () {
                    return getComputedStyle(document.documentElement)
                        .getPropertyValue('--' + symbolList[f] + '').split('"')[1]

                });

            let firstValue = values[0].valueX;
            let counter = 0;
            for (let i = 0; i < values.length; i++) {
                if (i < values.length - 1) {
                    if (newRec) {
                        startX = values[i].valueX - firstValue;
                        widthX = 0;
                    }

                    let thisX = values[i].valueX - firstValue;
                    let nextX = values[i + 1].valueX - firstValue;

                    if ((values[i].valueY == values[i + 1].valueY)) {
                        widthX = widthX + Math.abs(xScale(values[i + 1].valueX - firstValue) - xScale(values[i].valueX - firstValue));
                        newRec = false;
                    } else {
                        // if (counter < 10 && ((thisX < voiceEventTimes[0] && nextX > voiceEventTimes[0]) || (thisX < voiceEventTimes[1] && nextX > voiceEventTimes[1])
                        //     || (thisX < passangerEventTimes[0] && nextX > passangerEventTimes[0]) || (thisX < passangerEventTimes[1] && nextX > passangerEventTimes[1])
                        //     || (thisX < crossingEventTimes[0] && nextX > crossingEventTimes[0]) || (thisX < crossingEventTimes[1] && nextX > crossingEventTimes[1])
                        //     || (thisX < pointingEventTimes[0] && nextX > pointingEventTimes[0]) || (thisX < pointingEventTimes[1] && nextX > pointingEventTimes[1]))
                        // ) {
                        if (counter < 10 && ((thisX < usedTimeIntervalStart && nextX > usedTimeIntervalStart) || (thisX < usedTimeIntervalEnd && nextX > usedTimeIntervalEnd))) {
                            let newX = 0;
                            if (thisX < usedTimeIntervalStart) {
                                newX = firstValue + usedTimeIntervalStart;
                            } else {
                                newX = firstValue + usedTimeIntervalEnd;
                            }
                            let newValue = { "valueX": newX, "valueY": values[i].valueY };
                            values.splice(i + 1, 0, newValue);
                            widthX = widthX + Math.abs(xScale(newX) - xScale(values[i].valueX));
                        } else {
                            widthX = widthX + Math.abs(xScale(values[i + 1].valueX - firstValue) - xScale(values[i].valueX - firstValue));
                        }

                        newRec = true;

                        let rect = d3.selectAll("#eventlineId" + partNumber).append("rect")
                            .attr("class", function () { return values[i].valueY + " eventRec" })
                            .attr("y", function () { return dataCounter * 10 })
                            .attr("x", function () {
                                return xScale(startX);
                            })
                            .attr("width", function () {
                                return widthX;
                            })
                            .attr("height", 10)
                            .attr("title", function () {
                                if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                    return values[i].valueY;
                                }
                            })
                            .attr("fill", function () {
                                if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                    //return graphColor;
                                    return userColors[partNumber];
                                }
                            })
                            .attr("opacity", function () {
                                // if ((startX > voiceEventTimes[0] && startX < voiceEventTimes[1]) || (startX > passangerEventTimes[0] && startX < passangerEventTimes[1]) || (startX > crossingEventTimes[0] && startX < crossingEventTimes[1]) || (startX > pointingEventTimes[0] && startX < pointingEventTimes[1])) {
                                if(startX > usedTimeIntervalStart && startX < usedTimeIntervalEnd) {
                                    return 1
                                } else {
                                    return 0.1
                                }
                            })
                            .attr("onmouseover", "highlight3d(this)")
                            .attr("onmouseout", "unhighlight3d(this)")
                            .on("click", function () {
                                markEventWindow(this);
                            })
                            .style("display", function () {
                                if (eventNames.includes(name)) {
                                    return "inline";
                                } else {
                                    return "none";
                                }
                            })
                            // .style("opacity", 0.5)
                            .append("svg:title")
                            .text(values[i].valueY);

                        if (values[i].valueY.length < widthX / 5) {


                            d3.selectAll("#eventlineId" + partNumber).append("text")
                                .attr("y", function () { return dataCounter * 10 + 8 })
                                .attr("x", function () {
                                    return xScale(startX) + widthX / 2;
                                })
                                .attr("fill", function () {
                                    if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                        // if ((startX > voiceEventTimes[0] && startX < voiceEventTimes[1]) || (startX > passangerEventTimes[0] && startX < passangerEventTimes[1]) || (startX > crossingEventTimes[0] && startX < crossingEventTimes[1]) || (startX > pointingEventTimes[0] && startX < pointingEventTimes[1])) {
                                        if(startX > usedTimeIntervalStart && startX < usedTimeIntervalEnd) {
                                            return "black";
                                        } else {
                                            return "white";
                                        }

                                    }
                                })
                                .attr("font-family", "Roboto")
                                .attr("font-weight", function () {
                                    // if ((startX > voiceEventTimes[0] && startX < voiceEventTimes[1]) || (startX > passangerEventTimes[0] && startX < passangerEventTimes[1]) || (startX > crossingEventTimes[0] && startX < crossingEventTimes[1]) || (startX > pointingEventTimes[0] && startX < pointingEventTimes[1])) {
                                    if(startX > usedTimeIntervalStart && startX < usedTimeIntervalEnd) {
                                        return "bold";
                                    } else {
                                        return "normal";
                                    }
                                })
                                .text(function () {
                                    if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                        return values[i].valueY;
                                    }
                                })
                                .style("display", function () {
                                    if (eventNames.includes(name)) {
                                        return "inline";
                                    } else {
                                        return "none";
                                    }
                                });
                        }
                    }
                } else {
                    newRec = true;
                    if (values.length === 1) {
                        startX = 0;
                        widthX = Math.abs(xScale(maxX));
                    }
                    let rect = d3.select("#eventlineId" + partNumber).append("rect")
                        .attr("class", function () { return values[i].valueY + " eventRec" })
                        .attr("y", function () {
                            return dataCounter * 10
                        })
                        .attr("x", function () {
                            return xScale(startX);
                        })
                        .attr("width", function () {
                            return widthX;
                        })
                        .attr("height", 10)
                        .attr("fill", function () {
                            if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                //return graphColor;
                                return userColors[partNumber];
                            }
                        })
                        .attr("onmouseover", "highlight3d(this)")
                        .attr("onmouseout", "unhighlight3d(this)")
                        .on("click", function () {
                            markEventWindow(this);
                        })
                        .style("display", function () {
                            if (eventNames.includes(name)) {
                                return "inline";
                            } else {
                                return "none";
                            }
                        })
                        // .style("opacity", 0.5)
                        .append("svg:title")
                        .text(values[i].valueY);

                    d3.selectAll("#eventlineId" + partNumber).append("text")
                        .attr("y", function () { return dataCounter * 10 + 8 })
                        .attr("x", function () {
                            return xScale(startX) + widthX / 2;
                        })
                        .attr("fill", function () {
                            if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                return "black";
                            }
                        })
                        .attr("font-family", "Roboto")
                        .attr("font-weight", "bold")
                        .text(function () {
                            if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                return values[i].valueY;
                            }
                        })
                        .style("display", function () {
                            if (eventNames.includes(name)) {
                                return "inline";
                            } else {
                                return "none";
                            }
                        });
                }
            }
            dataCounter++;
        });
    });
}

function highlight3d(ele) {
    let eventViewWidth = document.getElementById("timelineView").getBoundingClientRect().width;

    let eventWidth = eventViewWidth - margin.left - margin.right;

    let xScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, eventWidth]);

    highlightedColor3d = ele.getAttribute("fill");

    let x = parseInt(ele.getAttribute("x"));
    let length = x + parseInt(ele.getAttribute("width"));
    // redraw3d(xScale.invert(x), xScale.invert(length));
    d3.select("#mainEventLine").selectAll("." + ele.getAttribute("class"))
        // .attr("border", 1)
        .style("opacity", 1)
        .raise();
}

function unhighlight3d(ele) {
    d3.select("#mainEventLine").selectAll("." + ele.getAttribute("class"))
        .style("opacity", 0.5)
        .lower();
}


function explosion(i) {
    var id = null;
    var elem = containers[i];
    var pos = 80;
    elem.style.display = "flex"
    elem.style.zIndex = 10 - i;
    clearInterval(id);
    id = setInterval(frame, 1);
    function frame() {
        if (pos >= 80 + i * (150)) {
            clearInterval(id);
        } else {
            pos += 8;
            elem.style.top = pos + 'px';
        }
    }
}

function reconstruct(i) {
    var id = null;
    var elem = containers[i];
    var pos = elem.style.top;
    pos = pos.split('px')[0];
    clearInterval(id);
    id = setInterval(frame, 1);
    function frame() {
        if (pos <= 80) {
            clearInterval(id);
            elem.style.display = "none"
            if (i == containers.length - 1) {
                document.getElementById("mainEventLine").style.backgroundColor = "var(--elementBasic)";
            }
        } else {
            pos -= 8;
            elem.style.top = pos + 'px';
        }
    }
}




async function slideoutEventView() {
    var eventview = document.getElementById("timelineView");
    $("#eventViewContainer0").css("height", "100%");
    grid.update(eventview, { h: 6 });


    // return new Promise((resolve, reject) => {
    //     var eventview = document.getElementById("eventlineContainer");
    //     var buttonslider2 = document.getElementById("buttonslider2");
    //     var id = null;
    //     var id2 = null;
    //     var height = 50;
    //     var pos2 = 90;
    //     clearInterval(id);
    //     id = setInterval(frame, 3);
    //     function frame() {
    //         if (height >= 500) {
    //             clearInterval(id);
    //             resolve();
    //         } else {
    //             height += 3;
    //             buttonslider2.style.bottom = height + 'px';
    //             eventview.style.height = height + 'px';
    //         }
    //     }
    //     clearInterval(id2);
    //     id2 = setInterval(frame2, 10);
    //     function frame2() {
    //         if (pos2 >= 270) {
    //             clearInterval(id2);
    //         } else {
    //             pos2 += 3;
    //             buttonslider2.style.transform = "rotate(" + pos2 + "deg)";
    //         }

    //     }

    // });
}

function slideinEventView() {
    var eventview = document.getElementById("timelineView");
    $("#eventViewContainer0").css("height", "50%");
    grid.update(eventview, { h: 2 });
    // var eventview = document.getElementById("eventlineContainer");
    // var id = null;
    // var id2 = null;
    // var height = 500;
    // var pos2 = 270;
    // clearInterval(id);
    // id = setInterval(frame, 3);
    // function frame() {
    //     if (height <= 50) {
    //         clearInterval(id);
    //     } else {
    //         height -= 3;
    //         buttonslider2.style.bottom = height + 'px';
    //         eventview.style.height = height + 'px';
    //     }
    // }
    // id2 = setInterval(frame2, 10);
    // function frame2() {
    //     if (pos2 <= 90) {
    //         clearInterval(id2);
    //     } else {
    //         pos2 -= 3;
    //         buttonslider2.style.transform = "rotate(" + pos2 + "deg)";
    //     }

    // }
}
function addeventclicker() {
    $("#eventContainer").click(function (e) {
        //$("#eventViewContainer").stopPropagation();
        e.stopPropagation();
        // $("#eventViewContainer").off('click');
        addonclickforEvents();
        // $("#eventViewContainer").click(function () { openEventView() });
    });

    // $("#minimizeEventContainer").click(function (e) {
    //     e.stopPropagation();
    //     openEventView();
    // });

}


/* ------------------------------------------------ Event View ---------------------------------------------------- */

async function openEventView() {
    if (eventslided) {
        slideinEventView();
        eventslided = false;
        d3.selectAll('#eventContainer').remove();
        if (isHorizontal) {
            drawEventsGrid("events.json", eventList);
        }

    } else {
        eventslided = true;
        await slideoutEventView();

        $("#eventViewContainer0").load("eventView.html", async function () {
            drawEventsView("events.json", eventList, 0);
            addeventclicker();
        });
    }

}

function drawAudioEventLine(filename, partNumber, redraw) {

    let audioEventLine = d3.select("#audioEvents");

    let eventViewWidth = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width;
    let eventWidth = eventViewWidth - 30;

    let xScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, eventWidth]);

    let eventline;
    if (redraw) {
        audioEventLine.selectAll("svg").remove();
        eventline = audioEventLine.append("svg")
            .attr("id", "audioEventsLine")
            .attr("class", "eventlineView")
            .attr("width", eventViewWidth)
            .style("height", "40px");

        eventline.append("text")
            .attr("x", xScale(voiceEventTimes[0]))
            .attr("y", 35)
            .attr("font-family", "Roboto")
            .attr("fill", function () {
                return getComputedStyle(document.documentElement).getPropertyValue('--font');
            })
            .style("text-anchor", "start")
            .text("Voice Command");
        eventline.append("text")
            .attr("x", xScale(passangerEventTimes[0]))
            .attr("y", 35)
            .attr("font-family", "Roboto")
            .attr("fill", function () {
                return getComputedStyle(document.documentElement).getPropertyValue('--font');
            })
            .style("text-anchor", "start")
            .text("Passanger Enters");
        eventline.append("text")
            .attr("x", xScale(crossingEventTimes[0]))
            .attr("y", 35)
            .attr("font-family", "Roboto")
            .attr("fill", function () {
                return getComputedStyle(document.documentElement).getPropertyValue('--font');
            })
            .style("text-anchor", "start")
            .text("Emergency Brake");
        eventline.append("text")
            .attr("x", xScale(pointingEventTimes[0]))
            .attr("y", 35)
            .attr("font-family", "Roboto")
            .attr("fill", function () {
                return getComputedStyle(document.documentElement).getPropertyValue('--font');
            })
            .style("text-anchor", "start")
            .text("Pointing");
    } else {
        eventline = audioEventLine.select("svg");
    }


    d3.json("./Data/" + filename).then(function (data) {
        let start = -1;
        let end = 0;
        let firstValueX = data[0].timeStamp;
        for (let i = 0; i < data.length; i++) {
            if (data[i].value > -50) {
                if (start === -1) {
                    start = data[i].timeStamp - firstValueX;
                    end = data[i].timeStamp - firstValueX;
                }
                if (i < data.length - 1) {
                    let thisIn = false;
                    let nextIn = false;
                    if ((start > voiceEventTimes[0] && start < voiceEventTimes[1]) || (start > passangerEventTimes[0] && start < passangerEventTimes[1]) || (start > crossingEventTimes[0] && start < crossingEventTimes[1]) || (start > pointingEventTimes[0] && start < pointingEventTimes[1])) {
                        thisIn = true;
                    }
                    if ((end > voiceEventTimes[0] && end < voiceEventTimes[1]) || (end > passangerEventTimes[0] && end < passangerEventTimes[1]) || (end > crossingEventTimes[0] && end < crossingEventTimes[1]) || (end > pointingEventTimes[0] && end < pointingEventTimes[1])) {
                        nextIn = true;
                    }

                    if (data[i + 1].value > -50 && (thisIn === nextIn)) {
                        end = data[i + 1].timeStamp - firstValueX;
                    } else {
                        eventline.append("rect")
                            .attr("class", function () { return "audioP" + partNumber + " eventRec" })
                            .attr("y", (partNumber * 10) + 1)
                            .attr("x", xScale(start))
                            .attr("width", xScale(end) - xScale(start))
                            .attr("height", 10)
                            .attr("title", "Speach")
                            .attr("fill", function () {
                                return userColors[partNumber];
                            })
                            .style("display", function () {
                                return "inline";
                            })
                            .style("opacity", function () {
                                if ((start > voiceEventTimes[0] && start < voiceEventTimes[1]) || (start > passangerEventTimes[0] && start < passangerEventTimes[1]) || (start > crossingEventTimes[0] && start < crossingEventTimes[1]) || (start > pointingEventTimes[0] && start < pointingEventTimes[1])) {
                                    return 1;
                                } else {
                                    return 0.1;
                                }
                            });
                        start = -1;
                    }
                } else {
                    eventline.append("rect")
                        .attr("class", function () { return "audioP" + partNumber + " eventRec" })
                        .attr("y", (partNumber * 10) + 1)
                        .attr("x", xScale(start))
                        .attr("width", xScale(end) - xScale(start))
                        .attr("height", 10)
                        .attr("title", "Speach")
                        .attr("fill", function () {
                            return userColors[partNumber];
                        })
                        .style("display", function () {
                            return "inline";
                        })
                        .style("opacity", 0.5);

                    start = -1;
                }
            }
        }
    });

    eventline.append("image")
        .attr("id", "audioImage" + partNumber)
        .attr("class", function () {
            if (partNumber != 0) {
                return "hidden"
            }
        })
        .style("cursor", "pointer")
        .attr("width", "10")
        .attr("height", "10")
        .attr("x", "10")
        .attr("y", (partNumber * 10) + 1)
        .attr("href", function () {
            return getComputedStyle(document.documentElement)
                .getPropertyValue('--' + symbolList[8] + '').split('"')[1]
        })
        .on("click", function () {
            toggleAudio(this)
        });

    eventline.selectAll("line").remove();
    eventline.append("line")
        .style("stroke", "orange")
        .style("stroke-width", 2)
        .style("display", "none")
        .attr("class", "eventMarker")
        .attr("x1", 10)
        .attr("y1", 0)
        .attr("x2", 10)
        .attr("y2", 50);

    eventline.append("line")
        .style("stroke", "red")
        .style("stroke-width", 2)
        .style("display", "block")
        .attr("class", "eventMarkerPlay")
        .attr("x1", margin.left)
        .attr("y1", 0)
        .attr("x2", margin.left)
        .attr("y2", 50);
}

function toggleAudio(t) {
    let videoFrame = document.getElementById("videoFrame");
    if (videoFrame.muted) {
        videoFrame.muted = false;
        t.setAttribute("href", getComputedStyle(document.documentElement).getPropertyValue('--' + symbolList[8] + '').split('"')[1])
    } else {
        videoFrame.muted = true;
        t.setAttribute("href", getComputedStyle(document.documentElement).getPropertyValue('--' + symbolList[7] + '').split('"')[1])
    }
}

function redrawCustomEvents() {
    d3.selectAll('#customEvents').selectAll("svg").remove();
    eventline = d3.select("#customEvents").append("svg")
        .attr("id", "customEventslineId")
        .attr("class", "eventlineView")
        .style("height", "40px");

    let eventViewWidth = document.getElementById("timelineView").getBoundingClientRect().width;
    let eventWidth = eventViewWidth - margin.left - margin.right;

    let xScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, eventWidth]);


    customEvents.forEach(customEvent => {
        let width = 0;
        if (customEvent.startTime < customEvent.endTime) {
            width = xScale(customEvent.endTime) - xScale(customEvent.startTime);
        }
        else {
            width = xScale(customEvent.startTime) - xScale(customEvent.endTime);
        }

        eventline.append("rect")
            .attr("class", function () { return "customEvent" + " eventRec" })
            .attr("y", 10)
            .attr("x", function () {
                return xScale(customEvent.startTime);
            })
            .attr("width", width)
            .attr("height", 10)
            .attr("title", customEvent.name)
            .attr("fill", function () {
                return "white";
            })
            .style("display", function () {
                return "inline";
            })
            .on("click", function () {
                highlightCustomEvent(this);
            })
            .style("opacity", 0.5);

        d3.selectAll("#customEventslineId").append("text")
            .style("pointer-events", "none")
            .attr("y", function () { return 10 + 8 })
            .attr("x", function () {
                return xScale(customEvent.startTime) + (width / 2)
            })
            .attr("fill", function () {
                return "black";
            })
            .attr("font-family", "Roboto")
            .attr("font-weight", "bold")
            .text(customEvent.eventName)
            .style("display", function () {
                return "inline";
            });
    });

    d3.selectAll("#customEventslineId").append("image")
        .attr("width", "10")
        .attr("height", "10")
        .attr("x", "10")
        .attr("y", 10 * 1)
        .attr("href", function () {
            return getComputedStyle(document.documentElement)
                .getPropertyValue('--' + symbolList[6] + '').split('"')[1]

        });
}

var selectedCustomEvent = null;

function highlightCustomEvent(t) {
    d3.selectAll(".customEvent")
        .style("stroke-width", 0);

    t.style.strokeWidth = 2;
    t.style.stroke = "green";

    selectedCustomEvent = t;
}

function removeCustomEvent() {
    if (selectedCustomEvent != null) {
        let index = Array.prototype.indexOf.call(selectedCustomEvent.parentElement.children, selectedCustomEvent) - 10;
        customEvents.splice(index / 3, 1);
        console.log(index);
        selectedCustomEvent.nextSibling.remove();
        selectedCustomEvent.remove();
        selectedCustomEvent = null;
    } else {
        displayWarningPopUp("Select a custom event first!");
    }
}

function markEventWindow(t) {
    let widthX = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;
    let xScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, widthX]);

    let widthX2d = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width - margin.right - margin.left;
    let xScale2d = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, widthX2d]);

    let startPos = parseFloat(t.getAttribute("x"));
    let endPos = startPos + parseFloat(t.getAttribute("width"));

    startTime = xScale.invert(startPos);
    endTime = xScale.invert(endPos);
    savedTime = xScale.invert(startPos);

    let timeDot1 = d3.select(".timeView").selectAll(".timeDot1");
    let timeDot2 = d3.select(".timeView").selectAll(".timeDot2");
    let timeDotPlay = d3.select(".timeView").selectAll(".timeDotPlay");
    let eventMarkerPlay = d3.selectAll(".eventMarkerPlay");

    isFixed1 = true;
    isFixed2 = true;
    timeDot1.style("display", "block")
        .attr("cx", startPos);

    timeDot2.style("display", "block")
        .attr("cx", endPos);

    timeDotPlay.style("display", "block")
        .attr("cx", startPos);

    eventMarkerPlay.style("display", "block")
        .attr("x1", startPos)
        .attr("x2", startPos);

    let timeDot12D = d3.select(".timeline").selectAll(".timeDot1");
    let timeDot22D = d3.select(".timeline").selectAll(".timeDot2");
    let timeMarker12D = d3.selectAll(".timeMarker1");
    let timeMarker22D = d3.selectAll(".timeMarker2");
    let timeMarkerPlay = d3.selectAll(".timeMarkerPlay");
    let markedRect = d3.selectAll(".markedRect");

    timeDot12D.style("display", "block")
        .attr("cx", xScale2d(startTime));

    timeDot22D.style("display", "block")
        .attr("cx", xScale2d(endTime));

    timeMarker12D.style("display", "block")
        .attr("x1", xScale2d(startTime))
        .attr("x2", xScale2d(startTime));

    timeMarker22D.style("display", "block")
        .attr("x1", xScale2d(endTime))
        .attr("x2", xScale2d(endTime));

    timeMarkerPlay.style("display", "block")
        .attr("x1", xScale2d(startTime))
        .attr("x2", xScale2d(startTime));

    markedRect.style("display", "block")
        .attr("x", xScale2d(startTime))
        .attr("width", xScale2d(endTime) - xScale2d(startTime));

    index = nearestValue(timestamps, startTime);
}

let isMade = false;
function drawCustomEventline(name, value1, value2) {
    eventNames = customEventList;
    let eventline;
    eventlineContainer = d3.select("#customEvents");
    if (!isMade) {
        d3.selectAll('#customEvents').selectAll("svg").remove();
        eventline = eventlineContainer.append("svg")
            .attr("id", "customEventslineId")
            .attr("class", "eventlineView")
            .style("height", "40px");
        isMade = true;
    }
    else {
        eventline = d3.select("#customEventslineId");
    }

    eventline.append("line")
        .style("stroke", "red")
        .style("stroke-width", 2)
        .style("display", "none")
        .attr("class", "eventMarkerPlay")
        .attr("x1", 10)
        .attr("y1", 0)
        .attr("x2", 10)
        .attr("y2", 50);


    // d3.selectAll('#eventlineContainer').selectAll("svg").selectAll("rect").remove();

    // d3.selectAll('#eventlineContainer').selectAll("svg").selectAll("rect").remove();
    let eventViewWidth = document.getElementById("timelineView").getBoundingClientRect().width;
    let eventViewHeight = document.getElementById("timelineView").getBoundingClientRect().height;

    let eventWidth = eventViewWidth - margin.left - margin.right;


    let xScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, eventWidth]);

    customEvents.push({ "eventName": name, "startTime": xScale.invert(value1), "endTime": xScale.invert(value2) });

    // for (let j = 0; j < columnCount; j++) {

    let svg = d3.selectAll(".eventlineView")
        .attr("transform", `translate(0, 20)`)
        .attr("width", eventViewWidth)
        .call(d3.axisBottom(xScale));

    let newRec = true;
    let widthX;
    let startX;


    var usedDashes = [];

    let test = "test";
    if (value1 < value2) {
        startX = value1;
        widthX = value2 - value1;
    }
    else {
        startX = value2;
        widthX = value1 - value2;
    }


    // svg.append("image")
    //     .attr("width", "10")
    //     .attr("height", "10")
    //     .attr("x", "10")
    //     .attr("y", 10 * f)
    //     .attr("href", function () {
    //         return getComputedStyle(document.documentElement)
    //             .getPropertyValue('--' + symbolList[f] + '').split('"')[1]

    //     });
    d3.selectAll("#customEventslineId").append("rect")
        .attr("class", function () { return test + " eventRec" })
        .attr("y", function () { return 10 })
        .attr("x", function () {
            return startX;
        })
        .attr("width", function () {
            return widthX;
        })
        .attr("height", 10)
        .attr("title", function () {
            return name;
        })
        .attr("fill", function () {
            return "white";
        })
        // .attr("onmouseover", "highlight3d(this)")
        // .attr("onmouseout", "unhighlight3d(this)")
        .style("display", function () {
            return "inline";
        })
        .on("click", function () {
            highlightCustomEvent(this);
        })
        .style("opacity", 0.5);

    d3.selectAll("#customEventslineId").append("text")
        .style("pointer-events", "none")
        .attr("y", function () { return 10 + 8 })
        .attr("x", function () {
            let textxPosition = parseFloat(startX) + parseFloat((widthX / 2));
            return textxPosition;
        })
        .attr("fill", function () {
            return "black";
        })
        .attr("font-family", "Roboto")
        .attr("font-weight", "bold")
        .text(function () {
            return name;
        })
        .style("display", function () {

            return "inline";

        });
    d3.selectAll("#customEventslineId").append("image")
        .attr("width", "10")
        .attr("height", "10")
        .attr("x", "10")
        .attr("y", 10 * 1)
        .attr("href", function () {
            return getComputedStyle(document.documentElement)
                .getPropertyValue('--' + symbolList[6] + '').split('"')[1]

        });


}

// function maxStack() {
//     let stackCount = 0;
//     let eventRects = document.getElementsByClassName("eventRec");
//     eventRects = Array.from(eventRects).filter(function (e) { return (!e.classList.contains("Neutral") && !e.classList.contains("Nothing")) });
//     let counterArray = [];
//     console.log(eventRects);
//     for (let i = 0; i < eventRects.length; i++) {
//         let startPos = parseFloat(eventRects[i].getAttribute("x"));
//         let endPos = startPos + parseFloat(eventRects[i].getAttribute("width"));
//         let currenCount = 0;
//         for (let j = i + 1; j < eventRects.length; j++) {
//             let startPosThis = parseFloat(eventRects[j].getAttribute("x"));
//             let endPosThis = startPosThis + parseFloat(eventRects[j].getAttribute("width"));
//             if ((startPosThis > startPos) && (startPosThis < endPos)) {
//                 currenCount++;
//             } else if ((endPosThis > startPos) && (endPosThis < endPos)) {
//                 currenCount++;
//             }
//             else if ((startPosThis < startPos) && (endPosThis > endPos)) {
//                 currenCount++;
//             }
//         }
//         console.log(eventRects[i].getAttribute("class") + currenCount);
//         if (currenCount > stackCount) {
//             stackCount = currenCount;
//         }
//         counterArray.push(currenCount);
//     }



//     for (let i = 0; i < eventRects.length; i++) {
//         eventRects[i].setAttribute("y", counterArray[i] * 5);
//     }
// }