
// var items = [
//     { w: 11, h: 2, content: 'my first widget' }, // will default to location (0,0) and 1x1
//     { w: 11, h: 2, content: 'another longer widget!' } // will be placed next at (1,0) and 2x1
// ];

var mainOptions = {
    acceptWidgets: true,
    // dragOut: true,
    dragIn: true,
    column: 24,
    layout: 'moveScale',
    cellHeight: 'auto',
    // float: true,
    resizable: {
        handles: 'e,se,s,sw,w,n,nw'
    },
    draggable: {
        handle: '.dragBar'
    },
    margin: 3
};
var subOptions = {
    acceptWidgets: true,
    dragOut: true,
    // dragIn: true,
    minRow: 1,
    maxRow: 1,
    column: 24,
    //resizable: false, 
    resizable: {
        handles: "false"
    },
    layout: 'move',
    cellHeight: 40,
    cellWidth: 'auto',
    disableOneColumnMode: "true",
    draggable: {
        helper: "clone"
    },
    margin: "1px 4px 1px 4px"
};
var grid;
var gridside;

// function setupTopBarGrid() {
//     gridside = GridStack.init(subOptions, $("#sidebarIcons")[0]);
// }

function setupGridFirst() {
    grid = GridStack.init(mainOptions, $("#gridAll")[0]);
}


async function setupGrid() {
    return new Promise((resolve, reject) => {
        // grid = GridStack.init(mainOptions, $("#gridAll")[0]);

        // grid.load(items);


        // gridside = GridStack.init(subOptions, $("#sidebarIcons")[0]);


        // grid.load(items);

        // grid.column(24);
        var sameGrid = false;

        grid.on('resizestart', async function (event, el) {
            if (event.target.id == "3dView") {
                document.getElementById("webGlContentDiv").style.pointerEvents = "none";
            }
        })


        grid.on('resizestop', async function (event, el) {
            if (event.target.id == "lineGraphs") {
                d3.selectAll(".demoGraph").selectAll("circle").remove();
                setTimeout(drawMultiple, 200);
                // h = 0;
            } else if (event.target.id == "3dView") {
                resizeCanvas3d();
                document.getElementById("webGlContentDiv").style.pointerEvents = "auto";
                // startthis(graph3dtype, graph3dmode);
            } else if (event.target.id == "timelineView") {
                drawTimelineView();
                if (eventslided) {
                    await drawEventsView("events.json", eventList);
                    if (isExploded) {
                        isExploded = false;
                        addonclickforEvents();
                        isExploded = true;
                    }
                } else {
                    grid.update(event.target, { h: 3 });
                    drawEventsGrid("events.json", eventList);
                    //slideoutEventView();
                }
            }
            else if (event.target.id == "sidebarleft") {
                updatechildrengridside();
            }
            else if (event.target.id == "eventView") {
                // grid.update(event.target, { h: 1 });
            }
        });

        // gridside.on('dragstart', function (event, el) {

        //     sameGrid = true;
        //     console.log("dragstart");

        //     gridside.update(el, { w: 1, h: 1 });

        // });

        // gridside.on('drag', function (event, el) {
        //     console.log("1");
        //     sameGrid = true;
        //     gridside.update(el, { w: 4, h: 1 });
        //     event.stopPropagation();
        // });

        // gridside.on('dragstop', function (event, el) {
        //     // 
        //     // let childs = $("#sidebarIcons")[0].children;
        //     // for (let i = 0; i < childs.length; i++) {
        //     //     gridside.update(childs[i], { w: 4, h: 1 });
        //     // }
        //     updatechildrengridside();
        //     // for (let i = 0; i < childs.length; i++) {
        //     //     gridside.update(childs[i], { w: 4, h: 1 });
        //     // }
        //     gridside.load($("#sidebarIcons")[0].children, false);
        // });

        grid.on('dragstart', function (event, el) {
            sameGrid = false;
            // setOldWandH(event.target);
        });

        // gridside.on('dropped', function (event, el) {
        //     console.log(el);
        //     // let childs = $("#sidebarIcons")[0].children;
        //     // for (let i = 0; i < childs.length; i++) {
        //     //     gridside.update(childs[i], { w: 4, h: 1 });
        //     // }
        //     // for (let i = 0; i < childs.length; i++) {
        //     //     gridside.update(childs[i], { w: 4, h: 1 });
        //     // }
        //     updatechildrengridside();
        //     // addToSideBar(el.el);
        //     $(".iconGraph").click(function (e) {
        //         e.stopImmediatePropagation();

        //         var parenttarget = e.target.parentElement.parentElement;
        //         console.log(parenttarget);
        //         sameGrid = false;
        //         $(parenttarget.children[0].children[0]).css("display", "block");
        //         $(parenttarget.children[0].children[1]).css("display", "none");
        //         grid.resizable(parenttarget, true);
        //         gridside.removeWidget(parenttarget);
        //         grid.addWidget(parenttarget);

        //         grid.update(parenttarget, { x: parseInt(parenttarget.getAttribute("oldx")), y: parseInt(parenttarget.getAttribute("oldy")), w: parseInt(parenttarget.getAttribute("oldw")), h: parseInt(parenttarget.getAttribute("oldh")) });
        //         drawMultiple();

        //     });

        // });



        grid.on('dropped', function (event, el) {
            sameGrid = false;
            // addWidgetGrid(el.el);
            let ele = $(".content").find("#" + el.el.id)[0];
            $(ele.children[0].children[0]).css("display", "block");
            $(ele.children[0].children[1]).css("display", "none");
            grid.resizable(ele, true);
            grid.update(ele, { w: parseInt(ele.getAttribute("oldw")), h: parseInt(ele.getAttribute("oldh")) });
            drawMultiple();
        });

        var widget = $("#sidebarleft");

        $(document).ready(function () {
            addListenersforWidgets();
            $(".closeWidgetButton").click(function () {
                closeWidgetGrid(this);
                $(".topBar").find("#icon" + this.closest(".grid-stack-item").id)[0].children[0].classList.toggle("highlightIcons");
                $(".topBar").find("#icon" + this.closest(".grid-stack-item").id)[0].children[0].children[0].classList.toggle("iconActivated");
            });

            $(".lockWidgetButton").click(function () {
                // let widget = this.parentElement.parentElement.parentElement.parentElement;
                let widget = this.closest(".grid-stack-item");
                this.classList.toggle("locked");
                this.parentElement.parentElement.classList.toggle("lockedDragbar");
                if (widget.id == "sidebarleft") {
                    if (widget.getAttribute("gs-no-move") == "true") {
                        grid.update(widget, { noMove: false });
                    }
                    else {
                        grid.update(widget, { noMove: true });
                    }
                }
                else {
                    if (widget.getAttribute("gs-locked") == "true") {
                        grid.update(widget, { noResize: false, noMove: false, locked: false });
                    } else {
                        grid.update(widget, { noResize: true, noMove: true, locked: true });
                    }
                }


            });
            $(".pageLock").click(function () {
                let widget = this.closest(".grid-stack-item");
                this.classList.toggle("pageUnlock");
                this.parentElement.classList.toggle("lockedDragbar");
                if ($(widget).css("z-index") == "10000") {
                    $(widget).css("position", "absolute");
                    $(widget).css("top", "");
                    $(widget).css("z-index", "auto");
                    if (widget.id == "sidebarleft") {
                        grid.update(widget, { noMove: false });
                    } else {
                        grid.update(widget, { noResize: false, noMove: false });
                    }
                } else {
                    $(widget).css("position", "fixed");
                    $(widget).css("top", "50px");
                    $(widget).css("z-index", "10000");
                    grid.update(widget, { noResize: true, noMove: true });
                }

                //widget.classList.toggle("gridItemPageLock");
            });
        });

        function updateGrid() {
            var allWidgets = $(".grid-stack-item");
            grid.load(allWidgets);
        }

        function addClass() {

            // switch ()
        }
        function removeClass() {

        }
        resolve();
    });
}


function addListenersforWidgets() {
    $(".closeWidgetButton").click(function () {
        closeWidgetGrid(this);
        $(".topBar").find("#icon" + this.closest(".grid-stack-item").id)[0].children[0].classList.toggle("highlightIcons");
    });

    $(".lockWidgetButton").click(function () {
        // let widget = this.parentElement.parentElement.parentElement.parentElement;
        let widget = this.closest(".grid-stack-item");
        this.classList.toggle("locked");
        this.parentElement.classList.toggle("lockedDragbar");
        if (widget.id == "sidebarleft") {
            if (widget.getAttribute("gs-no-move") == "true") {
                grid.update(widget, { noMove: false });
            }
            else {
                grid.update(widget, { noMove: true });
            }
        }
        else {
            if (widget.getAttribute("gs-locked") == "true") {
                grid.update(widget, { noResize: false, noMove: false, locked: false });
            } else {
                grid.update(widget, { noResize: true, noMove: true, locked: true });
            }
        }


    });
    $(".pageLock").click(function () {
        let widget = this.closest(".grid-stack-item");
        this.classList.toggle("pageUnlock");
        this.parentElement.classList.toggle("lockedDragbar");
        if ($(widget).css("z-index") == "10000") {
            $(widget).css("position", "absolute");
            $(widget).css("top", "");
            $(widget).css("z-index", "auto");
            if (widget.id == "sidebarleft") {
                grid.update(widget, { noMove: false });
            } else {
                grid.update(widget, { noResize: false, noMove: false });
            }
        } else {
            $(widget).css("position", "fixed");
            $(widget).css("top", "50px");
            $(widget).css("z-index", "10000");
            grid.update(widget, { noResize: true, noMove: true });
        }

        //widget.classList.toggle("gridItemPageLock");
    });
}

function cloneWidget(ele) {
    let widget = ele.closest(".grid-stack-item");


    let widget2 = ($("#" + widget.id).clone("true", "true"))[0];
    widget2.style.display = "block";
    sameGrid = false;
    gridside.addWidget(widget2);
    addToSideBar(widget2);

    setOldWandH(widget, widget2);

    // if (widget2.getAttribute("gs-locked") == "true") {
    //     widget2.children[0].children[0].children[0].children[1].classList.toggle("locked");
    //     // gridside.update(widget, { noMove: false, locked: false });
    // }

    widget2.children[0].classList.toggle("highlightIcons");
    updatechildrengridside();
    $(".iconGraph").click(function (e) {
        e.stopImmediatePropagation();

        e.target.parentElement.classList.toggle("highlightIcons");
        e.target.classList.toggle("iconActivated");
        var parenttarget = e.target.parentElement.parentElement;
        if ($(".content").find("#" + parenttarget.id)[0] == null) {
            addWidgetGrid(parenttarget);
        } else {
            closeWidgetGrid($(".content").find("#" + parenttarget.id)[0]);
        }
    });
}

function addWidgetGrid(ele) {
    let widgetClone = ($(ele).clone("true", "true"))[0];
    new Promise((resolve, reject) => {

        widgetClone.children[0].classList.toggle("highlightIcons");
        widgetClone.children[0].children[1].classList.toggle("iconActivated");

        sameGrid = false;
        $(widgetClone.children[0].children[0]).css("display", "block");
        $(widgetClone.children[0].children[1]).css("display", "none");
        grid.resizable(widgetClone, true);

        // gridside.removeWidget(parenttarget);
        grid.addWidget(widgetClone);
        grid.update(widgetClone, { x: parseInt(widgetClone.getAttribute("oldx")), y: parseInt(widgetClone.getAttribute("oldy")), w: parseInt(widgetClone.getAttribute("oldw")), h: parseInt(widgetClone.getAttribute("oldh")) });
        resolve();
    }).then(function (data) {
        setupInsertedWindow(widgetClone.id);
    });
}

function setupTopbarOnClick() {
    $(".iconGraph").click(function (e) {
        e.stopImmediatePropagation();

        e.target.parentElement.classList.toggle("highlightIcons");
        e.target.classList.toggle("iconActivated");
        let parenttargettest = e.target.parentElement.parentElement.id;
        let parenttarget = parenttargettest.split("icon")[1];
        if ($(".content").find("#" + parenttarget)[0] == null) {
            //addWidgetGrid(parenttarget);
            addWidgetOutOfList(e.target.parentElement.parentElement.id)
        } else {
            closeWidgetGrid($(".content").find("#" + parenttarget)[0]);
        }
    });
}

function addWidgetOutOfList(id) {
    //let ids = document.getElementById(id);
    let gridid = id.split("icon")[1];
    grid.addWidget(widgetList[returnIndexofWidgetList(gridid)]);
    let videoFrame = document.getElementById("videoFrame");
    videoFrame.currentTime = savedTime;
    if (play) {
        playVid();
    }
}


function setupInsertedWindow(id) {
    switch (id) {
        case "lineGraphs":

            drawMultiple();

            initDroppable();
            setupSortable();


            break;
        case "3dView":
            $("#webGlContentDiv").load("./WebGL/index.html", async function () {
                resizeCanvas3d();
            });
            break;
        case "meta":
            setUpMetadata("E4Data.json");
            break;
        case "settings":
            setupSettingsWindow("events.json");

            break;
        case "eventView":

            break;
        case "timelineView":
            drawTimelineView();
            drawEventsGrid("events.json");
            $("#eventViewContainer").click(function () { openEventView() });
            addeventclicker();
            break;
    }
}

function closeWidgetGrid(ele) {
    let widget = ele.closest(".grid-stack-item");
    let widgetTop = $(".topBar").find("#" + "icon" + widget.id)[0];
    setOldWandH(widget, widgetTop);
    grid.removeWidget(widget);
}

function setOldWandH(oldEle, newEle) {
    var oldW = oldEle.getAttribute("gs-w");
    var oldH = oldEle.getAttribute("gs-h");
    var oldX = oldEle.getAttribute("gs-x");
    var oldY = oldEle.getAttribute("gs-y");
    newEle.setAttribute("oldw", oldW);
    newEle.setAttribute("oldh", oldH);
    newEle.setAttribute("oldx", oldX);
    newEle.setAttribute("oldy", oldY);
}

function addToSideBar(el) {

    if (!sameGrid) {

        $(el.children[0].children[0]).css("display", "none");
        $(el.children[0].children[1]).css("display", "block");
        gridside.resizable(el, false);
    }
    // gridside.update(el, { w: 1, h: 1 });
    updatechildrengridside();
    sameGrid = false;
}



function updatechildrengridside() {
    let childs = $("#sidebarIcons")[0].children;
    for (let i = 0; i < childs.length; i++) {
        gridside.update(childs[i], { w: 3, h: 1, x: i * 4 });
    }

    // let sideWidth = 1//(parseInt(document.getElementById("sidebarleft").getAttribute("gs-w"));
    // let sideHeight = 24//parseInt(document.getElementById("sidebarleft").getAttribute("gs-h"));
    // console.log(sideWidth);
    // console.log(sideHeight);
    // let childs = $("#sidebarIcons")[0].children;
    // for (let i = 0; i < childs.length; i++) {
    //     // console.log(childs[i]);

    //     if (sideWidth < 2 && sideHeight > 3) {
    //         gridside.update(childs[i], {
    //             x: 0, y: i
    //         });
    //         console.log("sidew kleiner 2 height größer 3")
    //     }
    //     if (sideWidth > 2 && sideHeight < 3) {
    //         gridside.update(childs[i], {
    //             x: i * Math.ceil(24 / sideWidth), y: 0
    //         });
    //         console.log("sidew größer 2 height kleiner 3")
    //     }
    //     gridside.update(childs[i], {
    //         w: Math.ceil(24 / sideWidth), h: Math.ceil(24 / sideWidth)
    //     });
    // }
}

function resizeCanvas3d() {

    if (!isAllreadyLoaded) {
        let canvas = document.getElementById("webGlContentDiv").contentWindow.document.getElementById("unity-canvas");
        let contaienrWidth3d = document.getElementById("webGlContentDiv").getBoundingClientRect().width;
        let contaienrHeight3d = document.getElementById("webGlContentDiv").getBoundingClientRect().height - 25;
        canvas.style.width = contaienrWidth3d + "px";
        canvas.style.height = contaienrHeight3d + "px";
    }
}




