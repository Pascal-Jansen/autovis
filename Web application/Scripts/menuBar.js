function load3DPath() {
    window.location.replace("3DPath.html");
}
async function loadIndex() {
    window.location.replace("index.html");
}

async function oneColumn() {
    if (window.location.pathname != "/index.html") {
        await loadIndex();
    }
    if (document.querySelector(".demoGraphDiv") == null) {
        $(".content").load("horizontalView.html", async function () {
            document.getElementById("graphContainer").style.gridTemplateColumns = "1fr";
            document.getElementById("graphContainerFixed").style.gridTemplateColumns = "1fr";
            document.getElementById("timelineContainer").style.gridTemplateColumns = "1fr";
            document.getElementById("eventlineContainer").style.gridTemplateColumns = "1fr";
            await drawMultiple();
            updatePinPosition();
            updateMarkers();
        });
    } else {
        document.getElementById("graphContainer").style.gridTemplateColumns = "1fr";
        document.getElementById("graphContainerFixed").style.gridTemplateColumns = "1fr";
        document.getElementById("timelineContainer").style.gridTemplateColumns = "1fr";
        document.getElementById("eventlineContainer").style.gridTemplateColumns = "1fr";
        await drawMultiple();
        updatePinPosition();
        updateMarkers();
    }
}

async function twoColumns() {
    if (window.location.pathname != "/index.html") {
        await loadIndex();
    }
    document.getElementById("graphContainer").style.gridTemplateColumns = "1fr 1fr";
    document.getElementById("graphContainerFixed").style.gridTemplateColumns = "1fr 1fr";
    document.getElementById("timelineContainer").style.gridTemplateColumns = "1fr 1fr";
    document.getElementById("eventlineContainer").style.gridTemplateColumns = "1fr 1fr";
    await drawMultiple();
    updatePinPosition();
    updateMarkers();
}

async function threeColumns() {
    if (window.location.pathname != "/index.html") {
        await loadIndex();
    }
    document.getElementById("graphContainer").style.gridTemplateColumns = "1fr 1fr 1fr";
    document.getElementById("graphContainerFixed").style.gridTemplateColumns = "1fr 1fr 1fr";
    document.getElementById("timelineContainer").style.gridTemplateColumns = "1fr 1fr 1fr";
    document.getElementById("eventlineContainer").style.gridTemplateColumns = "1fr 1fr 1fr";
    await drawMultiple();
    updatePinPosition();
    updateMarkers();
}

function changeToScatterView() {
    window.location.replace("scatter.html");
}

function changeToClusterView() {
    window.location.replace("cluster.html");
}

function changeToBubbleView() {
    window.location.replace("bubble.html");
}

function changeToEventView() {
    $(".content").load("eventView.html", function () {
        // drawEvents("events.json", "");
        drawEventsView("events.json", eventList);
    });
    // window.location.replace("eventView.html");
    // drawEvents("events.json", "");
}

function updatePinPosition() {
    var fixed = document.getElementsByClassName("fixed");
    // check how many graphs there are per column
    let columnCount = document.getElementById("graphContainer").style.gridTemplateColumns.split(' ').length;
    let startColumn = columnCount;

    // define the column for all fixed graphs starting on the far right
    for (let i = (fixed.length - 1); i >= 0; i--) {
        if (startColumn == 0) {
            startColumn = columnCount;
        }
        fixed[i].style.gridColumn = startColumn + " / span 1";
        startColumn--;
    };
}

function clickZoomIn() {
    if (isFixed2) {
        if (!document.getElementById("layout").checked) {
            //var containerWidth = document.getElementById("graph0").getBoundingClientRect().width;
            var containerWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width;
            var markedRects = document.getElementsByClassName("markedRect");
            var timeDots = document.getElementsByClassName("timeDot");
            var timeMarkers = document.getElementsByClassName("timeMarker");

            d3.selectAll(".demoGraph").selectAll("circle").remove();

            zoomIn();
            var width = containerWidth - 60;
            document.getElementById("button8").style.display = "inline";
            for (let i = 0; i < timeMarkers.length; i++) {
                if (timeMarkers[i].classList.contains("timeMarker1")) {
                    timeMarkers[i].setAttribute("x1", 30);
                    timeMarkers[i].setAttribute("x2", 30);
                } else if (timeMarkers[i].classList.contains("timeMarker2")) {
                    timeMarkers[i].setAttribute("x1", width);
                    timeMarkers[i].setAttribute("x2", width);
                }
            }

            for (let i = 0; i < markedRects.length; i++) {
                markedRects[i].style.display = "none";
            }

            for (let i = 0; i < timeDots.length; i++) {
                // timeDots[i].style.display = "none";
                if (timeDots[i].classList.contains("timeDot1")) {
                    timeDots[i].setAttribute("x1", 30);
                    timeDots[i].setAttribute("x2", 30);
                } else if (timeDots[i].classList.contains("timeDot2")) {
                    timeDots[i].setAttribute("x1", width);
                    timeDots[i].setAttribute("x2", width);
                }
            }
        } else {
            var containerWidth = document.getElementById("graphV0").getBoundingClientRect().height;
            var markedRects = document.getElementsByClassName("markedRectVertical");
            var timeDots = document.getElementsByClassName("timeDot");
            var timeMarkers = document.getElementsByClassName("timeMarker");
            var width = containerWidth - 60;
            zoomIn();
            document.getElementById("button8").style.display = "inline";
            for (let i = 0; i < timeMarkers.length; i++) {
                if (timeMarkers[i].classList.contains("timeMarker1")) {
                    timeMarkers[i].setAttribute("y1", 30);
                    timeMarkers[i].setAttribute("y2", 30);
                    timeMarkers[i].setAttribute("x2", 200);
                } else if (timeMarkers[i].classList.contains("timeMarker2")) {
                    timeMarkers[i].setAttribute("y1", width);
                    timeMarkers[i].setAttribute("y2", width);
                    timeMarkers[i].setAttribute("x2", 200);
                }
            }

            for (let i = 0; i < markedRects.length; i++) {
                markedRects[i].style.display = "none";
            }

            for (let i = 0; i < timeDots.length; i++) {
                // timeDots[i].style.display = "none";
                if (timeDots[i].classList.contains("timeDot1")) {
                    timeDots[i].setAttribute("y1", 30);
                    timeDots[i].setAttribute("y2", 30);
                    timeDots[i].setAttribute("x2", 200);
                } else if (timeDots[i].classList.contains("timeDot2")) {
                    timeDots[i].setAttribute("y1", width);
                    timeDots[i].setAttribute("y2", width);
                    timeDots[i].setAttribute("x2", 200);
                }
            }
        }
    } else {
        displayWarningPopUp("Select an area first!");
    }
}

function clickZoomOut() {
    if (zoomed) {
        d3.selectAll(".demoGraph").selectAll("circle").remove();
        zoomOut();
    }
}


function setupMenuBar() {


    const checkbox = document.getElementById('colorTheme')

    checkbox.addEventListener('change', (event) => {
        document.documentElement.classList.toggle("dark-mode");
    })

    const checkbox2 = document.getElementById('layout')

    checkbox2.addEventListener('change', (event) => {

        toggleDrawDirection();
        doDragandDrop();
    })

}
