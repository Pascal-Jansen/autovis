/**
 * Pins graphs and removes them from pinning
 * @param {string} id - the id of the clicked pin element
 */
function pinGraph() {
    let dotMenu = document.getElementById("dotMenu");
    let id = dotMenu.getAttribute("clickedcontainer");
    dotMenu.style.display = "none";
    let graph = document.getElementById(id);
    // check if clicked graph is already pinned
    if (graph.classList.contains("fixed")) {
        // remove "fixed" class, change pin icon and move the graph to the "#graphContainer" element
        graph.classList.remove("fixed");
        // document.getElementById(id).classList.remove("pinned");
        document.getElementsByClassName("graphContainer")[0].appendChild(graph);
        graph.style.gridColumn = "";
    } else {
        // add "fixed" class, change pin icon and move the graph to the "#graphContainerFixed" element
        graph.classList.add("fixed");
        // document.getElementById(id).classList.add("pinned");
        document.getElementById("graphContainerFixed").prepend(graph);
    }

    var fixed = document.getElementsByClassName("fixed");
    // check how many graphs there are per column
    let columnCount = graph.parentElement.style.gridTemplateColumns.split(' ').length;
    let startColumn = columnCount;

    // define the column for all fixed graphs starting on the far right
    for (let i = (fixed.length - 1); i >= 0; i--) {
        if (startColumn == 0) {
            startColumn = columnCount;
        }
        fixed[i].style.gridColumn = startColumn + " / span 1";
        startColumn--;
    };
}



document.addEventListener('dragstart', function (event) {

    // event.dataTransfer.setData('Text', event.target.id);
    // document.getElementById('anzeige1').innerHTML = 'dragstart: ausgelöst';
    // document.getElementById('anzeige1').className = 'start';
});

function showMenu(t) {

    var dotMenu = document.getElementById("dotMenu");
    dotMenu.style.display = "block";
    dotMenu.setAttribute("clickedcontainer", t.parentElement.id);
    dotMenu.style.left = t.getBoundingClientRect().left - 100 + "px";
    dotMenu.style.top = t.getBoundingClientRect().top + "px";

    if (t.parentElement.classList.contains("fixed")) {
        document.getElementById("fixGraph").innerHTML = "Unpin Graph";
    } else {
        document.getElementById("fixGraph").innerHTML = "Pin Graph";
    }
}

$(document).click((event) => {
    if (document.getElementById("dotMenu") != null) {
        if (!$(event.target).closest('.more').length) {
            var dotMenu = document.getElementById("dotMenu");
            dotMenu.style.display = "none";
        }
    }
});


function deleteThisGraph() {
    let dotMenu = document.getElementById("dotMenu");
    let id = dotMenu.getAttribute("clickedcontainer");
    var graphContainerAll = document.getElementById("graphContainerAll");
    var chosenGraph = document.getElementById(id);
    var chosenGraphContainer = document.getElementById(chosenGraph.parentElement.id);
    chosenGraph.remove();
    if ($(chosenGraphContainer.children).hasClass("demoGraphDiv")) {

    }
    else {
        chosenGraphContainer.remove();
        var currentColums = graphContainerAll.style.gridTemplateColumns.length / 4;
        var columns = "";
        for (var i = 1; i <= currentColums; i++) {
            if (i == 1) {
                columns += "1fr";
            }
            else {
                columns += " 1fr";
            }
        }
        graphContainerAll.style.gridTemplateColumns = columns;
    }

    let dropdowns = document.getElementsByClassName("dragSingleId");

    for (let i = 0; i < dropdowns.length; i++) {
        if (dropdowns[i].getAttribute("nameid") == chosenGraph.getAttribute("datatype")) {
            dropdowns[i].style.backgroundColor = "#383838";
            break;
        }
    }
}