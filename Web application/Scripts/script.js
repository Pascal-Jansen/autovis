// if (!document.documentElement.classList.contains("dark-mode")) {
//     document.documentElement.classList.toggle("dark-mode");
// }
var margin = { top: 30, right: 0, bottom: 30, left: 30 };
var counter = 0;
var maxX = Number.MIN_VALUE;
var minX = Number.MAX_VALUE;
var width;
var height;
var columnCount;
var oldMin;
var oldMax;
var containerWidth;
var containerHeight;
// var eventline;
var eventlineContainer;
var isHorizontal = true;
var timeset = true;
var graphColor = "#00ffff";
var colors = [graphColor, "green", "red"];
var allDataTypes = [];
var allEventTypes = [];
var eventList = [];
var customEventList = [];
var addToGraphsList = [];
var containers = document.getElementsByClassName("eventDiv");
var containerTop;
var containerLeft;
var eventslided = false;
var zoomed = false;
var idSetup = 0;
var drawid = 0;
var graph3dtype = "scatter3d";
var graph3dmode = "lines";
var isAllreadyLoaded = true;
var widgetList = [];
var metaDataList = [];
var metaDataListId = [];
var isInList = false;
var outlierArray = [];
var userColors = ["#00ffff", "#5555FF", "#FF55FF", "#FFAA00", "#55FFFF"];
var symbolList = ["circle", "triangle", "star", "rhomb", "pentagon", "heart", "annotation", "mute", "speaker"];
var h = 0;
var voiceEventTimes = [176.1462254, 184.0802373];
var passangerEventTimes = [278.1436057, 294.0656912];
var crossingEventTimes = [485.1247754, 494.0964179];
var pointingEventTimes = [545.114947, 551.087279];
const usedTimeIntervalStart = 113.062;
const usedTimeIntervalEnd = 560;
// var usedTimeIntervalStart = 113.06214; / 134,0656113
// var usedTimeIntervalEnd = 539.2617865; / 560,2652578
//var unityGameInstance = null;
const url = new URL("http://localhost:8000");

/* ------------------------------------------------ Setup ---------------------------------------------------- */



$(document).ready(function () {
    setupStart();
});


function setupStart() {
    setupTopBarGrid();
    loadSetup();
    setupMenuBar();
    if (window.vuplex) {
        console.log("THIS ONE");
        addMessageListener();
    } else {
        console.log(" NO THIS ONE");
        window.addEventListener('vuplexready', addMessageListener);
    }
}


async function setUp() {

    await setupGrid();
    //cloneAllWidgets();


    await setValues("E4_Part1.json");
    await setValues("E4_Part2.json");
    await setValues("E4_Part3.json");

    await drawMultiple();
    if (!isAllreadyLoaded) {
        $("#button11").css("display", "block");
    }
    setupWidgets();

    setUpMetadata("E4_Part1.json");
    setupSettingsWindow("Events_Part1.json");

    generateDropdown();
    // document.getElementById("eventViewContainer").addEventListener('click', openEventView);
    // $("#eventViewContainer0").click(function () { openEventView() });
    drawTimelineView();
    document.getElementById("button7").addEventListener('click', clickZoomIn);
    document.getElementById("button8").addEventListener('click', clickZoomOut);
    // document.getElementById("button10").addEventListener('click', toggleTime);
    doDragandDrop();
    drawEventsGrid("Events_Part1.json", 0);
    drawEventsGrid("Events_Part2.json", 1);
    drawEventsGrid("Events_Part3.json", 2);

    drawAudioEventLine("audio_Part1.json", 0, true);
    drawAudioEventLine("audio_Part2.json", 1, false);
    drawAudioEventLine("audio_Part3.json", 2, false);

    // $("#webGlContentDiv").load("./WebGL/index.html", async function () {
    //     resizeCanvas3d();
    // });

    let myIframe = document.getElementById("webGlContentDiv");

    // let p = new Promise((resolve, reject) => {
    //     myIframe.src = "./WebGL/index.html";
    //     resolve();
    // });

    // p.then(() => { resizeCanvas3d(); });

    myIframe.addEventListener("load", function () {
        resizeCanvas3d();
    });

    myIframe.src = "./WebGL/index.html";


    fillTimestamps();

    updatechildrengridside();
    // var getallGridItems = $(".content").find(".grid-stack-item");
    // for (let i = 0; i < getallGridItems.length; i++) {
    //     grid.update(getallGridItems[i], { x: selectedGridValues[i].x, y: selectedGridValues[i].y, w: selectedGridValues[i].w, h: selectedGridValues[i].h });
    // }


}
/**
 * shows the metadata of the participant
 * @param {number} value The id of the participant
 * @returns 
 */
function onChangeMeta(value) {
    let metaContainer = d3.select(".metaContainer");
    metaContainer.selectAll("div").remove();
    counterforRecursive = 0;
    switch (value) {
        case "personal0":
            setUpMetadata("E4_Part1.json");
            return;
        case "personal1":
            setUpMetadata("E4_Part2.json");
            return;
        case "personal2":
            setUpMetadata("E4_Part3.json");
            return;
    }
    addMetaObject(metaDataList[parseInt(value)]);
}

/**
 * clones all wdigets
 */
function cloneAllWidgets() {
    if (isAllreadyLoaded) {

        cloneWidget(document.getElementById("lineGraphs"));
        cloneWidget(document.getElementById("3dView"));
        cloneWidget(document.getElementById("meta"));
        cloneWidget(document.getElementById("settings"));
        //cloneWidget(document.getElementById("eventView"));
        cloneWidget(document.getElementById("timelineView"));

    }
}

/**
 * pushes widget into widgetList for creating/removing widgets later.
 */
function setupWidgets() {
    let gridall = document.getElementById("gridAll");

    for (let i = 0; i < gridall.children.length; i++) {
        widgetList.push(gridall.children[i]);
    }
}


/**
 * blurs/ unblurs screen and enables/disable pop up for changing page layout.
 */
function setupPopup() {
    if (!isAllreadyLoaded) {
        $("#setupPopUp").load("./setup.html", async function () {
            if ($("#setupPopUp").css("display") == "none") {
                $("#setupPopUp").css("display", "block");
            }
            else {
                $("#setupPopUp").css("display", "none");
                $(".content").css("filter", "blur(0px)");
                $(".topBar").css("filter", "blur(0px)");
            }

        });
    }
}

var firstOffset = true;
var offsetValueX;
/**
 * sets values that need to be set at the beginning.
 */
async function setValues(fileName) {
    return new Promise((resolve, reject) => {
        d3.json("./Data/" + fileName).then(function (data) {
            // get the max and min x-value of the dataset
            data.forEach(dataset => {
                oldMax = maxX;
                oldMin = minX;
                let outlier = [];
                let sum;
                let mean;
                let minY = Number.MAX_VALUE;
                let maxY = Number.MIN_VALUE;

                if (dataset.dimension == "2d") {
                    if (firstOffset) {
                        offsetValueX = dataset.data[0].valueX;
                        firstOffset = false;
                    }
                    let valueArray = [];
                    let firstValue = dataset.data[0].valueX;
                    dataset.data.forEach(value => {
                        // maxX = Math.max(maxX, Math.max(value.valueX));
                        // minX = Math.min(minX, Math.min(value.valueX));

                        minX = 0;
                        maxX = Math.max(maxX, Math.max(value.valueX - firstValue));
                        valueArray.push(value.valueY);
                    });

                    outlier = findOutlier(valueArray);

                    outlier[1].forEach(value => {
                        maxY = Math.max(maxY, Math.max(value));
                        minY = Math.min(minY, Math.min(value));
                    });

                    sum = valueArray.reduce((pv, cv) => pv + cv, 0);
                    mean = sum / valueArray.length;
                }
                outlierArray.push({ "datatype": dataset.dataType, "outlier": outlier[0], "filteredData": outlier[1], "mean": mean, "minY": minY, "maxY": maxY });
            });
        });
        getAllDataTypes("E4_Part1.json");
        getAllEventTypes("Events_Part1.json");
        resolve();
    });
}
/**
 * Calls other function after loaded once.
 */
function loadSetup() {
    if (isAllreadyLoaded) {
        $(".content").load("setup.html", async function () {
            //applyValuesonGrid();
        });
        isAllreadyLoaded = false;
    }
    else {
        closeWidgetGrid($(".content").find("#lineGraphs")[0]);
        addWidgetGrid($(".topBar").find("#lineGraphs")[0]);

        addListenersforWidgets();

    }

}

/**
 * Gets the names of all data types that appear in a given file.
 * @param {String} fileName The name of the data file inside the "Data" folder.
 */
function getAllDataTypes(fileName) {
    allDataTypes = [];
    d3.json("./Data/" + fileName).then(function (data) {
        data.forEach(dataset => {
            if (dataset.dimension == "2d") {
                allDataTypes.push(dataset.dataType);
                addToGraphsList.push(dataset.dataType);
            }
        });
    });
}

/**
 * Gets the names of all event types that appear in a given file.
 * @param {String} fileName The name of the data file inside the "Data" folder.
 */
function getAllEventTypes(fileName) {
    allDataTypes = [];
    d3.json("./Data/" + fileName).then(function (data) {
        data.forEach(dataset => {
            allEventTypes.push(dataset.dataType);
            eventList.push(dataset.dataType);
        });
    });
}

/**
 * Gets the meta data data type and writes it on the meta data window.
 * @param {String} fileName The name of the data file inside the "Data" folder.
 */
function setUpMetadata(fileName) {
    d3.json("./Data/" + fileName).then(function (data) {
        var id = 0;
        let metaContainer = d3.select(".metaContainer");
        data.forEach(dataset => {
            if (dataset.dataType == "Meta Data") {
                if (!isInList) {
                    metaDataList.push(dataset.data);
                    isInList = true;
                }


                for (let i = 0; i < Object.keys(dataset.data).length; i++) {
                    metaContainer.append("div")
                        .attr("class", "metatextLabel boldText")
                        .html(Object.keys(dataset.data)[i]);

                    metaContainer.append("div")
                        .attr("class", "metatext")
                        .html(dataset.data[Object.keys(dataset.data)[i]]);
                }
            }
        });
    });
}

/**
 * Gets the events from the given json file and creates the settings window.
 * @param {String} fileName The name of the event data file inside the "Data" folder.
 */
function setupSettingsWindow(fileName) {
    d3.json("./Data/" + fileName).then(function (data) {
        var id = 0;
        data.forEach(dataset => {
            let eventItem = d3.select("#eventList").append("div")
                .attr("class", "eventItem");
            eventItem.append("input")
                .attr("type", "checkbox")
                .attr("name", dataset.dataType)
                .attr("class", "eventInput")
                .attr("id", "checkbox" + id)
                .attr("checked", "true")
                .on("click", reloadEvents);
            eventItem.append("div")
                .attr("class", symbolList[id] + " eventIcon");
            eventItem.append("label")
                .attr("class", "eventLabel")
                .attr("for", "checkbox" + id)
                .html(dataset.dataType);
            id++;
        });
    });
    let eventItem = d3.select("#eventList").append("div")
        .attr("class", "eventItem")
        .attr("id", "customEventDiv")
    eventItem.append("input")
        .attr("type", "checkbox")
        .attr("name", "customevent")
        .attr("class", "eventInput")
        .attr("id", "customEventInput")
        .attr("checked", "true")
        .on("click", toggleCustomEvents);
    eventItem.append("div")
        .attr("class", "customEventListing" + " eventIcon");
    eventItem.append("label")
        .attr("class", "eventLabel")
        .attr("for", "checkbox")
        .html("Custom Events");
    // document.getElementById("graphColor").value = graphColor;
    // document.getElementById("graphColor").addEventListener('change', changeColorOfGraph);
    document.getElementById("options3d").addEventListener('change', outLineAllTypeObjects);

}

/**
 * Creates all elements that are needed for the 2d view.
 * @param {Array} dataTypeList An array containing the names of all data types that should be drawn.
 * @param {Boolean} redraw A boolean signaling if all current graphs should be removed. If false container for all data types of the array will be added.
 * @returns 
 */
async function setupHorizontal(dataTypeList, redraw) {
    if (redraw) {
        d3.selectAll('.demoGraphDiv').remove();
        idSetup = 0;
    }
    return new Promise((resolve, reject) => {
        d3.json("./Data/E4_Part1.json").then(function (data) {
            data.forEach(dataset => {
                if (dataTypeList.includes(dataset.dataType)) {
                    // create the graph box
                    let div = d3.select("#" + document.getElementsByClassName("graphContainer")[0].id)
                        .append("div")
                        .attr("class", "demoGraphDiv")
                        .attr("dataType", dataset.dataType)
                        .attr("id", "graph" + idSetup)
                        .style("grid-column-start", 1)
                        .style("z-index", 3);

                    // create the svg element the graph will be drawn on
                    let svg = div
                        .append("svg")
                        .attr("class", "demoGraph");

                    // add the dot menu icon to the ".demoGraphDiv" element
                    div
                        .append("div")
                        .attr("class", "more")
                        .attr("onClick", "showMenu(this)")
                        .attr("id", idSetup);

                    // add the red highlighting lines
                    for (let i = 1; i <= 3; i++) {
                        let classNames = "timeMarker timeMarker" + i;
                        svg.append("line")
                            .style("stroke", "orange")
                            .style("stroke-width", 2)
                            .style("display", "none")
                            .attr("class", classNames)
                            .attr("x1", 10)
                            .attr("y1", 0)
                            .attr("x2", 10);
                    }
                    //hover marker
                    svg.append("line")
                        .style("stroke", "red")
                        .style("stroke-width", 2)
                        .style("display", "none")
                        .attr("class", "timeMarkerPlay")
                        .attr("x1", 10)
                        .attr("y1", 0)
                        .attr("x2", 10);

                    // add the red highlighting rect
                    svg.append("rect")
                        .attr("class", "markedRect")
                        .attr("fill", "orange")
                        .attr("fill-opacity", 0.2);

                    // add the graph label
                    svg.append("text")
                        .attr("x", (100))
                        .attr("y", margin.top / 1.5)
                        .attr("text-anchor", "left")
                        .attr("class", "labelText mediumText")
                        .style("font-size", margin.top / 2 + "px")
                        .style("fill", "var(--font)")
                        .text(function (d) {
                            return dataset.dataType;
                        });

                    // add the unit label
                    svg.append("text")
                        .attr("x", (5))
                        .attr("y", 25)
                        .attr("text-anchor", "left")
                        .attr("class", "unitLabel mediumText")
                        .style("font-size", margin.top / 3 + "px")
                        .style("fill", "lightblue")
                        .text(function (d) {
                            if (dataset.unit != "None") {
                                return dataset.unit;
                            } else {
                                return "No unit given";
                            }
                        });

                    idSetup++;
                    columnCount = document.getElementById("graphContainerAll").style.gridTemplateColumns.split(' ').length;
                    resolve();
                }
            });

        });
    });
}

/* ------------------------------------------------ Timeline ---------------------------------------------------- */

/**
 * Draws the timeline inside the 2d view window.
 */
function drawTimeline() {
    d3.selectAll('#timelineContainer').selectAll("svg").remove();

    // containerWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width;
    // width = containerWidth - margin.left - margin.right;
    let timelineContainer = d3.select("#timelineContainer");


    for (var i = 0; i < columnCount; i++) {
        if (i == 0) {
            document.getElementById("timelineContainer").style.gridTemplateColumns = "1fr";
        }
        else {
            document.getElementById("timelineContainer").style.gridTemplateColumns += " 1fr";
        }

        var timeline = timelineContainer.append("svg")
            .attr("class", "timeline")
            .attr("onmouseover", "mouseover()")
            .attr("onmouseout", "mouseout()")
            .attr("onclick", "fixBars(event)");

        addHighlightCircles(timeline);
    }
    let ticksCount = 2;
    // if (columnCount > 1) {
    //     ticksCount = 2
    // }
    // else {
    //     ticksCount = 4;
    // }
    // define x-scale
    let xScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, width]);

    let newTicks;
    newTicks = xScale.ticks(ticksCount)
    newTicks.unshift(xScale.domain()[0]);
    newTicks.push(maxX);


    let xAxis = d3.axisBottom(xScale)
        .tickValues(newTicks)
        .tickFormat(function (d, i, n) {
            return createTickFormat(d, i, n);
        });

    d3.selectAll(".timeline")
        .attr("transform", `translate(0, 5)`)
        .attr("width", containerWidth)

        .call(xAxis);

    addMouseListenerTimeline();
}

/**
 * Draws the timeline inside the timeline window.
 */
function drawTimelineView() {
    d3.selectAll('#timelineViewContainer').selectAll("svg").remove();


    let timeViewWidth = document.getElementById("timelineView").getBoundingClientRect().width;
    let timeViewHeight = document.getElementById("timelineView").getBoundingClientRect().height;

    let timeWidth = timeViewWidth - margin.left - margin.right;
    let timeHeight = timeViewHeight - 15;

    d3.select("#timelineViewContainer").style("height", timeHeight);
    // document.getElementById("timelineViewContainer").style.height = timeHeight;

    let timelineContainer = d3.select("#timelineViewContainer");

    var timeline = timelineContainer.append("svg")
        .attr("class", "timeView")
        .attr("onmouseover", "mouseoverTimeView()")
        .attr("onmouseout", "mouseout()")
        .attr("onclick", "fixBars(event)")
        .attr("onmousedown", "mouseState(event)")
        .attr("onmouseup", "mouseState(event)");

    addHighlightCircles(timeline);

    var ticksCount = 12;
    // define x-scale
    let xScale = d3.scaleLinear()
        // .domain(d3.extent(values, function (d) { return d.valueX; }))
        .domain([minX, maxX])
        .range([margin.left, timeWidth]);

    let newTicks;
    newTicks = xScale.ticks(ticksCount)
    newTicks.unshift(xScale.domain()[0]);
    newTicks.push(maxX);

    let xAxis = d3.axisBottom(xScale)
        .tickValues(newTicks)
        .tickFormat(function (d, i, n) {
            return createTickFormat(d, i, n);
        });

    d3.selectAll(".timeView")
        .attr("transform", `translate(0, 0)`)
        .attr("width", timeViewWidth)
        .attr("height", timeViewHeight)
        .call(xAxis);

    addMouseListenerTimelineView();
}

/**
 * Adds the red circles to the timeline.
 * @param {*} svg The d3 selection of the svg element the circles are drawn on.
 */
function addHighlightCircles(svg) {
    svg.append("circle")
        .attr("class", "timeDot1 timeDot")
        .attr("cx", "50")
        .attr("cy", "0")
        .attr("r", "5")
        .attr("fill", "orange");

    svg.append("circle")
        .attr("class", "timeDot2 timeDot")
        .attr("cx", "50")
        .attr("cy", "0")
        .attr("r", "5")
        .attr("fill", "orange");

    svg.append("circle")
        .attr("class", "timeDot3 timeDot")
        .attr("cx", "50")
        .attr("cy", "0")
        .attr("r", "5")
        .attr("fill", "orange");

    svg.append("circle")
        .attr("class", "timeDotPlay")
        .style("display", function () {
            if (svg.node().classList.contains("timeView")) {
                return "block";
            }
            return "none";
        })
        .attr("cx", margin.left)
        .attr("cy", "0")
        .attr("r", "5")
        .attr("fill", "red");

}

/**
 * Creates the tick format for a timeline.
 * @param {*} d 
 * @param {*} i 
 * @param {*} n 
 * @returns The tick format of the given timestamp.
 */
function createTickFormat(d, i, n) {
    let tickFormat;
    if (timeset) {
        tickFormat = changeTimeFormatDifference(d, oldMin);
    } else {
        tickFormat = changeTimeToHMS(d);
    }
    if (!zoomed) {
        if (!n[i + 1]) {
            return tickFormat + "    End";
        } else if (!n[i - 1]) {
            return "Start    " + tickFormat;
        } else {
            return tickFormat;
        }
    } else {
        return tickFormat;
    }
}

/**
 * Converts a given unix-timestamp into the h:m:s format.
 * @param {Number} timestamp The unix-timestamp that should be convertet.
 * @returns The convertet timestamp in the h:m:s format.
 */
function changeTimeToHMS(timestamp) {
    // Multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(timestamp * 1000);
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();

    return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
}

/**
 * Computes the difference of two unix-timestamps and converts it into the h:m:s format.
 * @param {Number} firstTimestemp The first unix-timestamp the difference chould be calculated from.
 * @param {Number} secondTimestemp The second unix-timestamp the difference chould be calculated from.
 * @returns A String containing the h:m:s format of the unix Timestamp
 */
function changeTimeFormatDifference(firstTimestemp, secondTimestemp) {
    // Multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date_difference = new Date((firstTimestemp - secondTimestemp) * 1000);

    //date_difference = date_difference.getTimezoneOffset();
    var hours_difference = date_difference.getUTCHours();
    // Minutes part from the timestamp
    var minutes_difference = "0" + date_difference.getUTCMinutes();
    // Seconds part from the timestamp
    var seconds_difference = "0" + date_difference.getUTCSeconds();

    return hours_difference + ':' + minutes_difference.substr(-2) + ':' + seconds_difference.substr(-2);
}

/* ------------------------------------------------ Draw ---------------------------------------------------- */

/**
 * changes the graph orientation and starts drawing the graphs
 */
async function toggleDrawDirection() {
    if (isHorizontal) {
        isHorizontal = false;
        document.getElementById("metadata").style.height = ($(window).height() - 40) + "px";
        $(".content").load("verticalView.html", async function () {
            drawMultiple();
        });
    } else {
        isHorizontal = true;
        document.getElementById("metadata").style.height = ($(window).height() - 40 - 50 - 25 + 17) + "px";
        $(".content").load("horizontalView.html", async function () {
            eventslided = false;
            document.getElementById("buttonslider2").addEventListener('click', openEventView);
            drawMultiple();
            setupSortable();
            initDroppable();
        });
    }
}


/**
 * draws mutiple data sheets into the graphs
 */
async function drawMultiple() {
    d3.selectAll('.demoGraph').selectAll("path").remove();
    d3.selectAll('.demoGraphVertical').selectAll("path").remove();
    counter = 0;
    if (isHorizontal) {
        if (document.querySelector(".demoGraphDiv") == null) {
            await setupHorizontal(addToGraphsList, true);
            await draw("E4_Part1.json", addToGraphsList, true, 0);
            await draw("E4_Part2.json", addToGraphsList, false, 1);
            await draw("E4_Part3.json", addToGraphsList, false, 2);
            drawTimelineView();
            if (eventslided) {
                drawEventsView("Events_Part1.json", eventList);
            } else {
                h = 0;
                drawEvents("Events_Part1.json", eventList, 0);
                drawEvents("Events_Part2.json", eventList, 1);
                drawEvents("Events_Part3.json", eventList, 2);
            }
        } else {
            await draw("E4_Part1.json", addToGraphsList, true, 0);
            await draw("E4_Part2.json", addToGraphsList, false, 1);
            await draw("E4_Part3.json", addToGraphsList, false, 2);
            drawTimelineView();
            if (eventslided) {
                drawEventsView("Events_Part1.json", eventList);
            } else {
                h = 0;
                drawEvents("Events_Part1.json", eventList, 0);
                drawEvents("Events_Part2.json", eventList, 1);
                drawEvents("Events_Part3.json", eventList, 2);
            }
        }
    } else {
        if (document.querySelector(".demoGraphDivVertical") == null) {
            await setupVertical(addToGraphsList, true);
            await drawVertical("E4_Part1.json", addToGraphsList);
            drawTimelineView();
            drawEventsVertical("Events_Part1.json", eventList);
        } else {
            await drawVertical("E4_Part1.json", addToGraphsList);
            drawTimelineView();
            drawEventsVertical("Events_Part1.json", eventList);
        }
    }
}
/**
 * 
 * @param {Array} data Array of values of the json
 * @returns Array without outlier
 */
function findOutlier(data) {
    data.sort(function (a, b) {
        return a - b;
    });
    let outlier = [];
    let q1 = data[Math.floor(data.length / 4)];
    let q3 = data[Math.ceil(3 * (data.length / 4))];

    let iqr = q3 - q1;

    let errorData = [];

    data.forEach(dataPoint => {
        if (dataPoint > q1 - 1.5 * iqr && dataPoint < q3 + 1.5 * iqr) {
            // if (dataPoint < q1 - 1.5 * iqr || dataPoint > q3 + 1.5 * iqr) {
            outlier.push(dataPoint);
            errorData.push(dataPoint);
        }
    });
    return [findOutlier2(outlier), errorData];
    // return outlier;
}

/**
 * 
 * @param {Array} data Array of values of the json
 * @returns Array without outlier
 */
function findOutlier2(data) {
    data.sort(function (a, b) {
        return a - b;
    });
    let outlier = [];
    let q1 = data[Math.floor(data.length / 4)];
    let q3 = data[Math.ceil(3 * (data.length / 4))];

    let iqr = q3 - q1;

    data.forEach(dataPoint => {
        if (dataPoint < q1 - 1.5 * iqr || dataPoint > q3 + 1.5 * iqr) {
            outlier.push(dataPoint);
        }
    });
    return outlier;
}


/**
 * Draws horizontal linegraphs to the given data.
 * @param {*} fileName The name of the file inside the "Data" folder that the data is stored in.
 * @param {*} dataTypeList An array containing the names pf all data types that should be drawn.
 * @param {*} redraw  A boolean signaling if all current graphs should be removed. If false all data types of the array will be added as new graphs.
 * @param {*} partNumber  The number of the participant.
 * @returns Promise
 */
async function draw(fileName, dataTypeList, redraw, partNumber) {

    if (redraw) {
        drawid = 0;
        d3.selectAll('.demoGraph').selectAll("path").remove();
    }

    return new Promise((resolve, reject) => {
        counter++;
        d3.selectAll('#timelineContainer').selectAll("svg").remove();

        columnCount = document.getElementById("graphContainerAll").style.gridTemplateColumns.split(' ').length;

        // drawTimeline();

        d3.json("./Data/" + fileName).then(function (data) {

            // get the pixel width and height of a graph
            containerWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width;
            containerHeight = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().height;

            width = containerWidth - margin.left - margin.right;
            height = containerHeight - margin.top - margin.bottom;

            drawTimeline();


            data.forEach(dataset => {
                let values = dataset.data;
                let name = dataset.dataType;

                if (dataTypeList.includes(name)) {

                    let svgAll = d3.selectAll(".demoGraphDiv")
                        .filter(function () {
                            return d3.select(this).attr("dataType") == dataset.dataType; // filter by dataType
                        })
                    svgAll.each(function (d, i) {
                        // select the svg element the current graph will be drawn onto.
                        let svg = d3.select(this).select("svg");

                        svg.selectAll("g").remove();
                        // svg.selectAll("path").remove();
                        // svg.selectAll("circle").remove();

                        d3.selectAll(".timeMarker").attr("y2", containerHeight);
                        d3.selectAll(".timeMarkerPlay").attr("y2", containerHeight);
                        d3.selectAll(".markedRect").attr("height", containerHeight);

                        drawid++;

                        var filter = outlierArray.filter(function (a) { return (a.datatype == name) });


                        let minY = filter.reduce((pv, cv) => Math.min(pv, cv.minY), Number.MAX_VALUE);
                        let maxY = filter.reduce((pv, cv) => Math.max(pv, cv.maxY), Number.MIN_VALUE);

                        // define x-scale
                        let xScale = d3.scaleLinear()
                            .domain([minX, maxX])
                            .range([margin.left, width]);

                        // define y-scale
                        let yScale = d3.scaleLinear()
                            .domain([minY, maxY])
                            .range([height + margin.top, margin.top]);

                        let yAxis = d3.axisLeft(yScale)
                            .tickSize(containerWidth)
                            .ticks(8);

                        // draw y-axis
                        svg.append("g")
                            .attr("transform", `translate(${margin.left + containerWidth}, 0)`)
                            .call(yAxis);


                        if ((dataset.dimension != "2d")) {
                            let dim = parseInt(dataset.dimension.split('d')[0]);

                            let minMaxY;
                            new Promise((resolve, reject) => {
                                minMaxY = findMinMax(values);
                                resolve();
                            }).then(function () {
                                // define y-scale
                                const yScale = d3.scaleLinear()
                                    .domain([minMaxY.minY, minMaxY.maxY])
                                    .range([height + margin.top, margin.top]);

                                yAxis = d3.axisLeft(yScale)
                                    .tickSize(containerWidth)
                                    .ticks(8);

                                svg.append("g")
                                    .attr("transform", `translate(${margin.left + containerWidth}, 0)`)
                                    .call(yAxis);

                                for (let i = 0; i < dim; i++) {
                                    let yValues = Object.values(values[0].valueY);
                                    let path = "M" + xScale(values[0].valueX) + "," + yScale(yValues[i]);
                                    values.shift();

                                    values.forEach(point => {
                                        yValues = Object.values(point.valueY);
                                        path += "L" + xScale(point.valueX) + "," + yScale(yValues[i]);
                                    });

                                    svg.append("path")
                                        .style("fill", "none")
                                        .style("stroke", colors[i])
                                        .attr("stroke-width", 1.5)
                                        .attr("d", path)
                                        .style("opacity", 0.3);
                                }
                            });
                        } else {

                            var filter = outlierArray.filter(function (a) { return (a.datatype == name) });

                            let firstValue = values[0].valueX;
                            // build graph path
                            let paths = [];
                            paths[0] = "M" + xScale(firstValue - firstValue) + "," + yScale(values[0].valueY)
                            values.shift();

                            let count = 0;
                            // let eventValues = [voiceEventTimes[0], voiceEventTimes[1], passangerEventTimes[0], passangerEventTimes[1], crossingEventTimes[0], crossingEventTimes[1], pointingEventTimes[0], pointingEventTimes[1]];
                            let eventValues = [usedTimeIntervalStart, usedTimeIntervalEnd]
                            let valuesSplit = [[values[0]]];

                            values.forEach(point => {
                                if (filter[partNumber].filteredData.includes(point.valueY)) {
                                    if (point.valueX - firstValue < eventValues[count] || count == eventValues.length) {
                                        paths[count] += "L" + xScale(point.valueX - firstValue) + "," + yScale(point.valueY);
                                        valuesSplit[count].push(point);
                                    } else {
                                        paths[count + 1] = "M" + xScale(point.valueX - firstValue) + "," + yScale(point.valueY);
                                        valuesSplit[count + 1] = [point];
                                        count++;
                                    }
                                }
                            });

                            // ((point.valueX - firstValue > 155.1462254 && point.valueX - firstValue < 163.0802373) || (point.valueX - firstValue > 257.1436057 && point.valueX - firstValue < 273.0656912) || (point.valueX - firstValue > 464.1247754 && point.valueX - firstValue < 473.0964179) || (point.valueX - firstValue > 524.114947 && point.valueX - firstValue < 530.087279))

                            // add graph path
                            paths.forEach((path, i) => {
                                svg.append("path")
                                    .style("fill", "none")
                                    .style("stroke", userColors[partNumber])
                                    .attr("stroke-width", 1.5)
                                    .attr("d", path)
                                    .attr("class", "part" + partNumber)
                                    .style("opacity", function () {
                                        return (i % 2) * 0.35 + 0.05
                                    });
                            });

                            if (document.getElementById("showOutlier").getAttribute("checked") == "true") {
                                valuesSplit.forEach((valuesArray, i) => {
                                    valuesArray.forEach(value => {
                                        if (filter[partNumber].outlier.includes(value.valueY)) {
                                            svg.append("circle")
                                                .style("fill", "yellow")
                                                .style("opacity", (i % 2) + 0.02)
                                                .attr("r", 1)
                                                .attr("cx", xScale(value.valueX - firstValue))
                                                .attr("cy", yScale(value.valueY))
                                                .attr("class", "part" + partNumber);
                                        }
                                    });
                                });
                            }
                        }
                        resolve();
                    });
                }
            });
            drawMean();
        });
    });
}

/**
 * Draws three mean lines for each graph in the 2d window. The first is the lowest mean of the participants, the second is the highest mean of the participants and the third one is the
 * mean of all of them.
 */
function drawMean() {
    d3.selectAll(".meanLine").remove();
    let svgs = d3.selectAll(".demoGraph");
    for (let i = 0; i < svgs._groups[0].length; i++) {

        let name = svgs._groups[0][i].parentElement.getAttribute("datatype");

        if (addToGraphsList.includes(name)) {
            var filter = [];
            for (let j = 0; j < outlierArray.length; j++) {
                if (outlierArray[j].datatype == name) {
                    filter.push(outlierArray[j]);
                }
            }
            // var filter = outlierArray.filter(function (a) { return (a.datatype == name) });

            let usedParticipants = [];

            let parts = document.getElementsByClassName("participantOption");
            for (let j = 0; j < parts.length; j++) {
                // let thisOne = parts[j].parentElement.children[1];
                if (parts[j].style.backgroundColor == "green") {
                    usedParticipants.push(j);
                }
            }

            let maxMean = Number.MIN_VALUE;
            let minMean = Number.MAX_VALUE;
            let meansMean = 0;
            let means = [];

            for (let j = 0; j < usedParticipants.length; j++) {
                maxMean = Math.max(maxMean, filter[usedParticipants[j]].mean);
                minMean = Math.min(minMean, filter[usedParticipants[j]].mean);
                meansMean += filter[usedParticipants[j]].mean;
            }

            meansMean /= usedParticipants.length;
            means = [minMean, maxMean, meansMean];


            let minY = filter.reduce((pv, cv) => Math.min(pv, cv.minY), Number.MAX_VALUE);
            let maxY = filter.reduce((pv, cv) => Math.max(pv, cv.maxY), Number.MIN_VALUE);

            // define x-scale
            let xScale = d3.scaleLinear()
                .domain([minX, maxX])
                .range([margin.left, width]);

            // define y-scale
            let yScale = d3.scaleLinear()
                .domain([minY, maxY])
                .range([height + margin.top, margin.top]);

            let svg = svgs.filter(function (d, j) { return j == i });
            for (let j = 0; j < 3; j++) {
                svg.append("line")
                    .attr("x1", xScale(minX))
                    .attr("x2", xScale(maxX))
                    .attr("y1", yScale(means[j]))
                    .attr("y2", yScale(means[j]))
                    .style("stroke", function () {
                        if (j == 2) {
                            return "red";
                        } else {
                            return "darkred";
                        }
                    })
                    .style("stroke-width", "2")
                    .style("display", function () {
                        if (document.getElementById("showMean") != null) {
                            if (document.getElementById("showMean").getAttribute("checked") == "true") {
                                return "block";
                            } else {
                                return "none";
                            }
                        }
                    })
                    .attr("class", "meanLine");
            }
        }
    }
}

/**
 * Finds the min and max y-value of a dataset containing multiple y-values for each x-value.
 * @param {*} values An array of the given values in the following format: [x, {y1, y2, ...}].
 * @returns Returns an Object containing minY and maxY.
 */
function findMinMax(values) {
    let maxY = Number.MIN_VALUE;
    let minY = Number.MAX_VALUE;
    for (let i = 0; i < Object.values(values[0].valueY).length; i++) {
        values.forEach(point => {
            let yValues = Object.values(point.valueY);
            maxY = Math.max(maxY, yValues[i]);
            minY = Math.min(minY, yValues[i]);
        });
    }
    return { minY: minY, maxY: maxY }
}

/* ------------------------------------------------ Events ---------------------------------------------------- */

/**
 * Draws the events on the event lines of the 2d view.
 * @param {string} filename The name of the file that the event data is stored in.
 * @param {Array} eventNames The names of the event types that should be drawn.
 */
function drawEvents(filename, eventNames, partNumber) {
    d3.selectAll('#eventlineContainer').selectAll("svg").remove();

    // create an svg for each column
    for (let i = 0; i < columnCount; i++) {
        if (i == 0) {
            document.getElementById("eventlineContainer").style.gridTemplateColumns = "1fr";
        }
        else {
            document.getElementById("eventlineContainer").style.gridTemplateColumns += " 1fr";
        }
        d3.select("#eventlineContainer").append("svg")
            .attr("class", "eventline")
            .style("height", "100%");
    }
    d3.json("./Data/" + filename).then(function (data) {
        data.forEach(dataset => {
            let values = dataset.data;
            let name = dataset.dataType;
            if (eventNames.includes(name)) {

                let xScale = d3.scaleLinear()
                    .domain([minX, maxX])
                    .range([margin.left, width]);

                d3.selectAll(".eventline")
                    .attr("transform", `translate(0, 0)`)
                    .attr("width", containerWidth)
                    .call(d3.axisBottom(xScale));

                let newRec = true;
                let widthX;
                let startX
                let firstValue = values[0].valueX;
                for (let i = 0; i < values.length; i++) {
                    if (i < values.length - 1) {
                        if (newRec) {
                            startX = values[i].valueX;
                            widthX = 0;
                        }
                        // check if the next value is the same event
                        if (values[i].valueY == values[i + 1].valueY) {
                            // update the width of the rect
                            widthX += Math.abs(xScale(values[i + 1].valueX - firstValue) - xScale(values[i].valueX - firstValue));
                            newRec = false;
                        } else {
                            // draw the rect and draw the rect
                            widthX += Math.abs(xScale(values[i + 1].valueX - firstValue) - xScale(values[i].valueX - firstValue));
                            newRec = true;
                            d3.selectAll(".eventline").append("rect")
                                .attr("class", function () { return values[i].valueY + " " + "rect" + partNumber })
                                .attr("y", h)
                                .attr("x", function () {
                                    return xScale(startX - firstValue);
                                })
                                .attr("width", function () {
                                    return widthX;
                                })
                                .attr("height", 3)
                                .attr("fill", function () {
                                    if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                        return userColors[partNumber];
                                    }
                                    // switch (values[i].valueY) {
                                    //     case "Happy":
                                    //         return "green";
                                    //     case "Anger":
                                    //         return "red"
                                    //     case "ButtonInteraction":
                                    //         return "blue";
                                    //     case "StopSign":
                                    //         return "white";
                                    //     case "Car":
                                    //         return "black";
                                    //     case "Motorcycle":
                                    //         return "yellow";
                                    // }
                                })
                                .attr("onmouseover", "showEventText(this)")
                                .attr("onmouseout", "hideEventText(this)")
                                .style("opacity", 0.5);
                        }
                    } else {
                        newRec = true;
                        d3.selectAll(".eventline").append("rect")
                            .attr("class", function () { return values[i].valueY + " " + "rect" + partNumber })
                            .attr("y", h)
                            .attr("x", function () {
                                return xScale(startX - firstValue);
                            })
                            .attr("width", function () {
                                return widthX;
                            })
                            .attr("height", 3)
                            .attr("fill", function () {
                                if (values[i].valueY != "Nothing" && values[i].valueY != "Neutral") {
                                    return userColors[partNumber];
                                }
                                // switch (values[i].valueY) {
                                //     case "Happy":
                                //         return "green";
                                //     case "Anger":
                                //         return "red"
                                //     case "ButtonInteraction":
                                //         return "blue";
                                //     case "StopSign":
                                //         return "white";
                                //     case "Car":
                                //         return "black";
                                //     case "Motorcycle":
                                //         return "yellow";
                                // }
                            })
                            .attr("onmouseover", "showEventText(this)")
                            .attr("onmouseout", "hideEventText(this)")
                            .style("opacity", 0.5);
                    }

                }
                h += 3;
            }

        });
    });
}

/**
 * zooms the graphs to the marked area
 */
function zoomIn() {
    zoomed = true;
    let oldMax2 = maxX;
    let oldMin2 = minX;
    let xScale = d3.scaleLinear()
        .domain([oldMin2, oldMax2])
        .range([margin.left, width]);

    //var containerWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width;
    var x1 = document.getElementsByClassName("demoGraphDiv")[0].getElementsByClassName("timeMarker1")[0].getAttribute("x1");
    var x2 = document.getElementsByClassName("demoGraphDiv")[0].getElementsByClassName("timeMarker2")[0].getAttribute("x1");

    let timeDots = d3.select(".timeView").selectAll(".timeDot3");
    timeDots.style("display", "none");

    minX = xScale.invert(x1);
    maxX = xScale.invert(x2);

    if (minX > maxX) {
        let save = maxX;
        maxX = minX;
        minX = save;
    }
    reloadEvents();
    drawMultiple();
    drawAudioEventLine("audio_Part1.json", 0, true);
    drawAudioEventLine("audio_Part2.json", 1, false);
    drawAudioEventLine("audio_Part3.json", 2, false);
    redrawCustomEvents();
}

/**
 * zooms out the graphs
 */
function zoomOut() {
    zoomed = false;
    minX = oldMin;
    maxX = oldMax;
    reloadEvents();
    drawMultiple();
    let timeMarkers = document.getElementsByClassName("timeMarker");
    for (let i = 0; i < timeMarkers.length; i++) {
        timeMarkers[i].style.display = "none";
    }
    isFixed1 = false;
    isFixed2 = false;
    drawAudioEventLine("audio_Part1.json", 0, true);
    drawAudioEventLine("audio_Part2.json", 1, false);
    drawAudioEventLine("audio_Part3.json", 2, false);
    redrawCustomEvents();
}

/**
 * Toggles the ticks of the timelines between the time of day and the time difference to the first time value.
 */
function toggleTime() {
    timeset = !timeset;
    if (isHorizontal) {
        drawTimeline();
        drawTimelineView();
        drawEventsView("Events_Part1.json", eventList);
    } else {
        drawTimelineVertical();
    }
}

/**
 * Updates the position of the red markers after a layout change.
 */
function updateMarkers() {
    var newContainerWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width;
    var oldContainerWidth = document.getElementsByClassName("timeMarker1")[0].getAttribute("data-graph-width");
    var newWidth = newContainerWidth - margin.left - margin.right;
    var oldWidth = oldContainerWidth - margin.left - margin.right;

    let newXScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, newWidth]);

    let oldXScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, oldWidth]);

    let pos1 = oldXScale.invert(document.getElementsByClassName("timeMarker1")[0].getAttribute("x1"));
    d3.selectAll(".timeDot1")
        .attr("x1", newXScale(pos1))
        .attr("x2", newXScale(pos1));

    d3.selectAll(".timeMarker1")
        .attr("x1", newXScale(pos1))
        .attr("x2", newXScale(pos1))
        .attr("data-graph-width", newContainerWidth);


    let pos2 = oldXScale.invert(document.getElementsByClassName("timeMarker2")[0].getAttribute("x1"));
    d3.selectAll(".timeMarker2")
        .attr("x1", newXScale(pos2))
        .attr("x2", newXScale(pos2));

    d3.selectAll(".timeDot2")
        .attr("x1", newXScale(pos2))
        .attr("x2", newXScale(pos2))
        .attr("data-graph-width", newContainerWidth);


    if (pos1 < pos2) {
        d3.selectAll(".markedRect")
            .attr("x", newXScale(pos1))
            .attr("width", Math.abs(newXScale(pos2) - newXScale(pos1)));
    } else {
        d3.selectAll(".markedRect")
            .attr("x", newXScale(pos2))
            .attr("width", Math.abs(newXScale(pos2) - newXScale(pos1)));

    }
}

/**
 * Is called after changes to the color input.
 * Changes the color of the 2d graphs.
 */
function changeColorOfGraph(t) {
    let index = Array.from(t.parentElement.parentElement.children).indexOf(t.parentElement);
    // var aRgbHex = t.value.split("#")[1].match(/.{1,2}/g);
    // var aRgb = [
    //     parseInt(aRgbHex[0], 16),
    //     parseInt(aRgbHex[1], 16),
    //     parseInt(aRgbHex[2], 16)
    // ];

    // if (aRgb[0] > 200 && aRgb[1] < 100 && aRgb[2] < 100) {
    //     t.value = userColors[index];
    // } else {
    //     // if (parseInt(t.value.split("#")[1], 16) < 8716288 || parseInt(t.value.split("#")[1], 16) > 16680582) {
    //     graphColor = t.value;
    //     d3.selectAll(".demoGraph").selectAll("path.part" + index).style("stroke", graphColor);
    //     d3.selectAll(".demoGraph").selectAll("line.part" + index + ".lineColor").style("stroke", graphColor);
    //     userColors[index] = graphColor;
    //     if (t.parentElement.classList.contains("mainParticipant")) {
    //         pickParticipantsEvents(index)
    //     }

    //     d3.select("#eventlineId" + index).selectAll("rect").attr("fill", function () {
    //         if (this.classList.contains("Nothing") || this.classList.contains("Neutral")) {
    //         }
    //         else {
    //             return graphColor;
    //         }
    //     });

    //     d3.selectAll(".audioP" + index).attr("fill", graphColor);

    //     d3.selectAll(".rect" + index).attr("fill", function () {
    //         if (this.classList.contains("Nothing") || this.classList.contains("Neutral")) {

    //         }
    //         else {
    //             return graphColor;
    //         }
    //     });


    // d3.selectAll(".demoGraphVertical").selectAll("path").style("stroke", graphColor);
    change3DColor(index);

    //document.getElementsByClassName("participantIcon")[index].style.backgroundColor = graphColor;
    //}
}

/**
 * Toggles the current participant status
 * @param {*} t Clicked participant
 */
function selectParticipant(t) {
    let participantOptions = Array.from(t.parentElement.parentElement.children);
    let index = participantOptions.indexOf(t.parentElement);
    if (t.style.backgroundColor == "green") {
        t.style.backgroundColor = "#383838";
        t.parentElement.style.backgroundColor = "#383838";
        document.getElementsByClassName("participantIcon")[index].style.display = "none";
        document.getElementById("eventViewContainer" + index).style.display = "none";
        d3.selectAll(".rect" + index).attr("display", "none");
        // participantOptions.forEach(participantOption => {
        //     if (participantOption.style.backgroundColor == "green") {
        //         let videoFrame = document.getElementById("videoFrame");
        //         videoFrame.setAttribute("src", "./Videos/Video - Mark.mkv")
        //     }
        // });
        disableParticipant(index);
    } else {
        t.style.backgroundColor = "green";
        t.parentElement.style.backgroundColor = "green";
        document.getElementsByClassName("participantIcon")[index].style.display = "block";
        document.getElementById("eventViewContainer" + index).style.display = "block";
        d3.selectAll(".rect" + index).attr("display", "block");
        enableParticipant(index);
    }

    toggleGraphs(t);
    updateheading();
}

function updateheading() {
    let parts = document.getElementsByClassName("participantIcon");
    let counter = 0;
    for (let i = 0; i < parts.length; i++) {
        if (parts[i].style.display != "none") {
            counter++;
        }
    }

    let heading = document.getElementById("participantText");
    if (counter == 1) {
        heading.innerHTML = "Single Participant";
    } else if (counter == 0) {
        heading.innerHTML = "Select Participant";
    } else {
        heading.innerHTML = "Multiple Participants";
    }
}

/**
 * 
 * @param {number} index 
 */
function pickParticipantsEvents(index) {
    if (index == 0) {
        drawEventsGrid("Events_Part1.json", 0);
    } else {
        drawEventsGrid("Events_Part" + (index + 1) + ".json", 0);
    }
}

/**
 * toggles graphs on and off and redraws the mean.
 * @param {*} t 
 */
function toggleGraphs(t) {
    let index = Array.from(t.parentElement.parentElement.children).indexOf(t.parentElement);
    if (d3.selectAll(".demoGraph").selectAll(".part" + index).style("display") == "none") {
        d3.selectAll(".demoGraph").selectAll(".part" + index).style("display", "block");
    } else {
        d3.selectAll(".demoGraph").selectAll(".part" + index).style("display", "none");
    }
    drawMean();
}

/**
 * Changes the color of the 3d objects.
 * @param {number} index  index of the participant
 */
function change3DColor(index) {
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'changeTrajectoyColor', index + ";" + graphColor);
    }
    //console.log("TEST");
    sendColorToCSharp(index);
}


/**
 * Changes the Main Participant
 * @param {*} t 
 */
function changeMainParticipant(t) {
    let index = Array.from(t.parentElement.parentElement.children).indexOf(t.parentElement);
    let mains = document.getElementsByClassName("mainParticipant");
    for (let i = 0; i < mains.length; i++) {
        mains[i].classList.toggle("mainParticipant");
    }

    graphColor = t.parentElement.children[2].value;
    if (index == 0) {
        drawEventsGrid("Events_Part1.json", 0);
    } else {
        drawEventsGrid("events" + index + ".json", 0);
    }


    t.parentElement.classList.toggle("mainParticipant");
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'changeMainParticipant', index);
    }
}

/**
 * Is called after a checkbox in the settings view is clicked.
 * Redraws all event lines with the now checked event types.
 */
function reloadEvents() {
    h = 0;
    eventList = [];
    // var listArr = document.getElementsByClassName("eventInput");
    var listArr = $(".content").find(".eventInput");
    for (let i = 0; i < listArr.length; i++) {
        if (listArr[i].checked) {
            eventList.push(listArr[i].getAttribute("name"));
        }
    }
    if (isHorizontal) {

        if (eventslided) {
            h = 0;
            drawEvents("Events_Part1.json", eventList, 0);
            drawEvents("Events_Part2.json", eventList, 1);
            drawEvents("Events_Part3.json", eventList, 2);
            drawEventsView("Events_Part1.json", eventList);
        } else {
            h = 0;
            drawEvents("Events_Part1.json", eventList, 0);
            drawEvents("Events_Part2.json", eventList, 1);
            drawEvents("Events_Part3.json", eventList, 2);
            drawEventsGrid("Events_Part1.json", 0);
            drawEventsGrid("Events_Part2.json", 1);
            drawEventsGrid("Events_Part3.json", 2);
        }

    } else {
        drawEventsVertical("Events_Part1.json", eventList)
    }
}

/**
 * Increments the counter of columns inside of the 2d view by one.
 */
function addColumn() {
    columnCount++;
}

/**
 * Decreases the counter of columns inside of the 2d view by one.
 */
function removeColumn() {
    columnCount--;
}

/**
 * Disable/Enables dark mode
 */
function toggleDarkMode() {
    document.documentElement.classList.toggle("dark-mode");
}

/**
 * Returns the index depending on the input string
 * @param {String} entry Name of Widget
 * @returns returns index of widget
 */
function returnIndexofWidgetList(entry) {
    switch (entry) {

        case "lineGraphs":
            return 0;
            break;
        case "3dView":
            return 1;
            break;
        case "meta":
            return 2;
            break;
        case "settings":
            return 3;
            break;
        case "video":
            return 4;
            break;
        case "timelineView":
            return 5;
            break;
    }
}



function toggleContext(t) {
    let nextElement = t.nextElementSibling;
    if (nextElement.style.display == "none") {
        nextElement.style.display = "block";
    } else {
        nextElement.style.display = "none";
    }

}

function toggleOutlier(t) {
    // document.getElementsByClassName("demoGraph").getElementsByTagName("circle"););
    if (d3.selectAll(".demoGraph").selectAll("circle").style("display") == "none") {
        d3.selectAll(".demoGraph").selectAll("circle").style("display", "block");
    } else {
        d3.selectAll(".demoGraph").selectAll("circle").style("display", "none");
    }
    if (t.getAttribute("checked") == "true") {
        t.setAttribute("checked", "false");
    } else {
        t.setAttribute("checked", "true");
    }
}

function toggleMean(t) {
    // document.getElementsByClassName("demoGraph").getElementsByTagName("circle");
    if (d3.selectAll(".demoGraph").selectAll(".meanLine").style("display") == "none") {
        d3.selectAll(".demoGraph").selectAll(".meanLine").style("display", "block");
    } else {
        d3.selectAll(".demoGraph").selectAll(".meanLine").style("display", "none");
    }
    if (t.getAttribute("checked") == "true") {
        t.setAttribute("checked", "false");
    } else {
        t.setAttribute("checked", "true");
    }
}

/**
 * Currently Obsolete due to WebGL disabled.
 * @param {*} t 
 */
function toggleDrivers(t) {
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'toggleDriver');
    }
    sendOptionSettingToCSharp("Avatar", 0);

}
/**
 * Currently Obsolete due to WebGL disabled.
 * @param {*} t 
 */
function togglePassengers(t) {
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'togglePassenger');
    }
    sendOptionSettingToCSharp("Passenger", 0);
}

/**
 * Calls WebGL function for slicing the Car in half. Currently Obsolete due to WebGL disabled.
 */
function sliceCar() {
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'ToggleSlice');
    }
}

function changeFocusParticipant(t) {
    let oldIndex = Array.from(t.parentElement.parentElement.children).indexOf(document.getElementsByClassName("crownSelected")[0].parentElement);
    document.getElementsByClassName("crownSelected")[0].classList.toggle("crownSelected");
    t.classList.toggle("crownSelected");
    let videoFrame = document.getElementById("videoFrame");
    videoFrame.setAttribute("src", "./Videos/" + t.getAttribute("data-videoFile"));
    videoFrame.currentTime = savedTime;
    if (play) {
        videoFrame.play();
    }

    let index = Array.from(t.parentElement.parentElement.children).indexOf(t.parentElement);
    videoFrame.style.borderColor = userColors[index];

    if (videoFrame.muted) {
        document.getElementById("audioImage" + index).setAttribute("href", getComputedStyle(document.documentElement).getPropertyValue('--' + symbolList[7] + '').split('"')[1])
    } else {
        document.getElementById("audioImage" + index).setAttribute("href", getComputedStyle(document.documentElement).getPropertyValue('--' + symbolList[8] + '').split('"')[1])
    }
    document.getElementById("audioImage" + oldIndex).classList.toggle("hidden");
    document.getElementById("audioImage" + index).classList.toggle("hidden");

    document.getElementById("optionsMeta").value = "personal" + index;
    onChangeMeta("personal" + index);

}

/**
 * Currently Obsolete due to WebGL disabled
 * @param {*} t 
 */
function trajectoryControll(t) {
    let x = getChildIndex(t.parentElement);
    let numb = (x - 1) / 2;
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'toggleTrajectoy', numb);
    }
    sendOptionSettingToCSharp("Trajectory", numb);
}

function getChildIndex(node) {
    return Array.prototype.indexOf.call(node.parentNode.childNodes, node);
}

/**
 * Currently Obsolete due to WebGL disabled
 * @param {*} t 
 */
function heatmapControll(t) {
    let x = getChildIndex(t.parentElement);
    let numb = (x - 1) / 2;
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'toggleHeatmap', numb);
    }
    sendOptionSettingToCSharp("Heatmaps", numb);
}

/**
 * Enables Participant
 * @param {number} index Index of Participant
 */
function enableParticipant(index) {
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'enableParticipant', index);
    }
    console.log("Test");
    sendOptionSettingToCSharp("enableParticipant", index);
}

/**
 * Disables Participant
 * @param {number} index Index of Participant
 */
function disableParticipant(index) {
    if (typeof (unityGameInstance) !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'disableParticipant', index);
    }
    sendOptionSettingToCSharp("disableParticipant", index);
}

/**
 * Displays a warning Pop-up for 3 seconds with inputed string.
 * @param {String} text 
 */
function displayWarningPopUp(text) {
    warningPopUp = document.getElementById("warningPopUp");
    warningPopUp.innerHTML = text;
    warningPopUp.style.display = "block";

    setTimeout(() => { $("#warningPopUp").fadeOut(3000) }, 1000);
}

/**
 * Currently Obsolete. Function for sending JSON to server.
 * @param {*} timestamp 
 */
async function postTimestampToServer(timestamp) {
    fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "timestamp": timestamp })
    });
    // .then(response => response.json());
}

/**
 * Currently Obsolete. Getting JSON from server.
 * @returns 
 */
async function getTimestampFromServer() {
    let ret;
    await fetch(url + "/VR.json")
        .then((response) => response.json())
        .then((data) => ret = data);
    return ret;
}

/**
 * Obsolete
 */
function sendDataToVR() {
    //postTimestampToServer(savedTime + offsetValueX);
}

/**
 * Currently Obsolete
 */
async function getDataFromVR() {
    let timestampJson = await getTimestampFromServer();

    let timelineWidth = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;

    let timelineXScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, timelineWidth]);

    let pos = timestampJson["timestamp"] - offsetValueX;

    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'jumptoFrame', (pos + offsetValueX).toString());
    }

    d3.selectAll(".eventMarkerPlay").attr("x1", timelineXScale(pos));
    d3.selectAll(".eventMarkerPlay").attr("x2", timelineXScale(pos));

    d3.selectAll(".timeDotPlay").attr("cx", timelineXScale(pos));

    if (!document.getElementsByClassName("iconGraph Graph2d")[0].classList.contains("iconActivated")) {
        let twoDWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width - margin.right - margin.left;

        let twoDXScale = d3.scaleLinear()
            .domain([minX, maxX])
            .range([margin.left, twoDWidth]);

        d3.selectAll(".timeMarkerPlay").attr("x1", twoDXScale(pos));
        d3.selectAll(".timeMarkerPlay").attr("x2", twoDXScale(pos));
    }


    index = nearestValue(timestamps, pos);
    savedTime = pos;
    isFixed1 = false;
    isFixed2 = false;
    endTime = -1;
    if (document.getElementById("videoFrame") != null) {
        document.getElementById("videoFrame").currentTime = index * 0.05;
    }
}

let postInterval;
let getInterval;
//postRepeatedly();
function postRepeatedly() {
    clearInterval(getInterval);
    postInterval = setInterval(() => {
        // if (document.getElementsByClassName("timeView")[0] !== undefined && document.getElementById("loadingScreen").style.display === "none") {
        //     sendDataToVR();
        // }
        if (document.getElementsByClassName("timeView")[0] !== undefined) {
            sendDataToVR();
        }
    }, 1000);
}

function getRepeatedly() {
    pauseTimeline();
    clearInterval(postInterval);
    getInterval = setInterval(() => {
        getDataFromVR();
    }, 1000);
}

function togglePostGet(t) {
    // if (t.getAttribute("data-mode") === "post") {
    //     t.setAttribute("data-mode", "get");
    //     t.style.backgroundColor = "green";
    //     getRepeatedly();
    // } else {
    //     t.setAttribute("data-mode", "post");
    //     t.style.backgroundColor = "";
    //     postRepeatedly();
    // }
}

/**
 * Sends Option Setting to Unity Application.
 * @param {String} typeofOption 
 * @param {number} numb 
 */
function sendOptionSettingToCSharp(typeofOption, numb) {
    window.vuplex.postMessage({ type: 'Option', optiontype: typeofOption, number: numb });
}

/**
 * Sends Color in hexadecimal to Unity Application.
 * @param {String} color 
 */
function sendColorToCSharp(color) {
    window.vuplex.postMessage({ type: 'Color', message: color });
}


/**
 * Obsolete
 */
function addMessageListener() {
    console.log("Listener Initialised");
    window.vuplex.addEventListener('message', function (event) {
        let json = event.data;
        // > JSON received: { "type": "greeting", "message": "Hello from C#!" }
        changeColorOfParticipant(json);
        console.log('JSON received: ' + json);
    });
}

/**
 * Changes all Visualisation colors depending on the received JSON.
 * @param {String} receivedJson 
 */
function changeColorOfParticipant(receivedJson) {
    console.log(receivedJson);
    let fixedJson = JSON.parse(receivedJson);
    console.log(fixedJson.participant);
    console.log(fixedJson.message);
    let participantNumber = fixedJson.participant;
    let color = fixedJson.message;

    let participants = document.getElementsByClassName("participantIcon");
    let graphColors = document.getElementsByClassName("graphColor");

    console.log(participants[participantNumber]);
    console.log(graphColors[participantNumber]);
    participants[participantNumber].style.backgroundColor = color;
    graphColors[participantNumber].setAttribute("value", color);

    d3.selectAll("path.part" + participantNumber).style("stroke", color);

    d3.select("#eventlineId" + participantNumber).selectAll("rect").attr("fill", function () {
        if (this.classList.contains("Nothing") || this.classList.contains("Neutral")) {
        }
        else {
            return color;
        }
    });


}