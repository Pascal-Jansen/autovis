


var CustomGridsValues = [];
var CustomGridsNames = [];
var selectedGridValues = [];
var customGeneratorGrid;

var customGridOptions = {
    acceptWidgets: true,
    dragOut: true,
    dragIn: true,
    column: 24,
    layout: 'moveScale',
    cellHeight: 'auto',
    // float: true,
    resizable: {
        handles: 'e,se,s,sw,w,n,nw'
    },
    draggable: {
        handle: '.dragBar'
    },
    margin: 3
};

$(document).ready(function () {
    CustomGridsValues = [];
    // localStorage.clear();
    loadCustomGrids();
    initiatePredefinedGrids();
    document.getElementById("CustomGridLayout").classList.toggle("hidden");
});

/**
 * Initiates the preset grids, as well as the grid of the custom grid generator.
 */
async function initiatePredefinedGrids() {
    customGeneratorGrid = GridStack.init(customGridOptions, $("#customGrid")[0]);
    GridStack.init(customGridOptions, $("#presetGrid1")[0].children[0]);
    GridStack.init(customGridOptions, $("#presetGrid2")[0].children[0]);
    GridStack.init(customGridOptions, $("#presetGrid3")[0].children[0]);
    GridStack.init(customGridOptions, $("#presetGrid4")[0].children[0]);
}


/**
 * Loads the previously created custom grids from local storage.
 */
function loadCustomGrids() {
    if (localStorage.getItem("CustomGridsValues") != "" && localStorage.getItem("CustomGridsValues") != null) {
        CustomGridsValues = JSON.parse(localStorage.getItem("CustomGridsValues"));
    }

    if (localStorage.getItem("CustomGridsNames") != "" && localStorage.getItem("CustomGridsNames") != null) {
        CustomGridsNames = JSON.parse(localStorage.getItem("CustomGridsNames"));
    }
    addCustomGrids();
}

/**
 * Adds a new grid to the page for each custom grid created.
 */
function addCustomGrids() {
    new Promise((resolve, reject) => {
        for (let i = 0; i < CustomGridsValues.length; i++) {
            let $clone = $("#presetGrid4").clone("true", "true");
            let clone = $clone[0];
            clone.querySelector('.grid-stack').classList.add("cloneColor");
            clone.querySelector('.deleteGrid').style.display = "block";
            clone.querySelector('.gridtext').style.display = "none";
            clone.querySelector('.customgridtext').style.display = "block";
            clone.querySelector('.customgridtext').value = CustomGridsNames[i];
            clone.setAttribute("id", "clone" + i);
            document.getElementById("gridLayoutContainer").appendChild(clone);
        }
        document.getElementsByClassName("setupcustomgridtext")[0].value = "My Grid ";
        resolve();
    }).then(function () {
        setupCustomGrids();
    });
}

/**
 * Sets the position and size of the widgets for each custom grid.
 */
async function setupCustomGrids() {
    for (let i = 0; i < CustomGridsValues.length; i++) {
        let gridDiv = document.getElementById("clone" + i).querySelector(".grid-stack");
        let cloneGrid = GridStack.init(customGridOptions, gridDiv);
        let thisGridValues = CustomGridsValues[i];
        let customGridItems = [];
        for (let j = 0; j < gridDiv.children.length; j++) {
            gridDiv.children[j].setAttribute("gs-x", thisGridValues[j].x);
            gridDiv.children[j].setAttribute("gs-y", thisGridValues[j].y);
            gridDiv.children[j].setAttribute("gs-w", thisGridValues[j].w);
            gridDiv.children[j].setAttribute("gs-h", thisGridValues[j].h);
            customGridItems.unshift(gridDiv.children[j]);
        }
        cloneGrid.load((customGridItems), true);
    }
}

/**
 * Is called after the user changes the name of a custom grid.
 * Changes the name of a custom grid in the local storage.
 * @param {String} t The new Name that will be stored in the local storage.
 */
function changeGridName(t) {
    let thisGrid = t.parentElement;
    let gridContainer = t.parentElement.parentElement;
    for (let i = 0; i < gridContainer.children.length; i++) {
        if (thisGrid == gridContainer.children[i]) {
            CustomGridsNames[i - 4] = t.value;
            localStorage.setItem("CustomGridsNames", JSON.stringify(CustomGridsNames));
        }
    }
}

/**
 * Is called after the user saves a new custom grid.
 * Gets the position and size of each widget.
 */
function saveValues() {
    let customGridValues = [];
    let customGridName = document.getElementsByClassName("setupcustomgridtext")[0].value;
    let items = document.getElementsByClassName("customGrids");
    new Promise((resolve, reject) => {
        for (let i = 0; i < items.length; i++) {
            customGridValues[i] = { x: items[i].getAttribute("gs-x"), y: items[i].getAttribute("gs-y"), w: items[i].getAttribute("gs-w"), h: items[i].getAttribute("gs-h") }
        }
        resolve();
    }).then(function () {
        saveCustomGrids(customGridName, customGridValues);
    });
}

/**
 * Saves the position and size of each widget of a newly created grid into the local storage.
 * @param {String} gridName The name of the Grid that should be stored.
 * @param {String} gridValues An array containing the position andy syze of each widget.
 */
function saveCustomGrids(gridName, gridValues) {

    CustomGridsValues.push(gridValues);
    CustomGridsNames.push(gridName);

    localStorage.setItem("CustomGridsValues", JSON.stringify(CustomGridsValues));
    localStorage.setItem("CustomGridsNames", JSON.stringify(CustomGridsNames));
    valuesCustomGrid = [];
    window.location.reload();
}

/**
 * Is called when the user clicks on a grid.
 * The grid will be selected and can be applyed. If it already was selected it will get unselected.
 * @param {*} ele The grid element that was clicked.
 */
function selectGridLayout(ele) {
    let grids = document.getElementById("gridLayoutContainer").getElementsByClassName("grid-stack");
    if (ele.classList.contains("selectedGraphLayout")) {
        ele.classList.toggle("selectedGraphLayout");
    }
    else {
        for (let i = 0; i < grids.length; i++) {
            grids[i].classList.remove("selectedGraphLayout");
        }
        ele.classList.add("selectedGraphLayout");
    }
    saveSelectedGridValues(ele);
};

/**
 * Saves the size and position of all widgets of the selected grid into the "selectedGridValues" array, so they can be loaded later.
 * @param {*} ele The selected grid element.
 */
function saveSelectedGridValues(ele) {
    selectedGridValues = [];
    let items = ele.children;
    let customGridItems = document.getElementById("customGrid").children;
    let listGrid = [];
    for (let i = 0; i < items.length; i++) {
        selectedGridValues[i] = { x: items[i].getAttribute("gs-x"), y: items[i].getAttribute("gs-y"), w: items[i].getAttribute("gs-w"), h: items[i].getAttribute("gs-h") };
        customGridItems[i].setAttribute("gs-x", items[i].getAttribute("gs-x"));
        customGridItems[i].setAttribute("gs-y", items[i].getAttribute("gs-y"));
        customGridItems[i].setAttribute("gs-w", items[i].getAttribute("gs-w"));
        customGridItems[i].setAttribute("gs-h", items[i].getAttribute("gs-h"));
        listGrid.unshift(customGridItems[i]);
    }
    customGeneratorGrid.load((listGrid), true);
}

/**
 * Is called after the user clickes the close-button on a custom grid.
 * Delets the custom grid from the page and the local storage.
 * @param {*} ele The close-button element that has been clicked.
 */
function deleteGridfromSetup(ele) {
    let gridContainer = document.getElementById("gridLayoutContainer");
    let parentthing = ele.parentElement;
    let childthing = parentthing.children[1];

    for (var i = 0; i < gridContainer.children.length; ++i) {
        if (parentthing === gridContainer.children[i]) {
            child_index = i;
            break;
        }
    }

    CustomGridsValues.splice((child_index - 4), 1);
    CustomGridsNames.splice((child_index - 4), 1);
    localStorage.setItem("CustomGridsValues", JSON.stringify(CustomGridsValues));
    localStorage.setItem("CustomGridsNames", JSON.stringify(CustomGridsNames));
    ele.parentElement.remove();
}

/**
 * Is called after the user clicks the "Open Custom Grid Generator" button.
 * Displays or hides the panel to create custom grids.
 * @param {*} t 
 */

function openCustomGrid(t) {
    if (document.getElementById("CustomGridLayout").classList.contains("hidden")) {
        t.innerHTML = "Close Custom Grid Generator"
    } else {
        t.innerHTML = "Open Custom Grid Generator"
    }
    document.getElementById("CustomGridLayout").classList.toggle("hidden");
}

/**
 * Is called after the user clicks the apply-button.
 * Loads the new index.html page.
 */
function applyGrid() {
    // $(".content").css("filter", "blur(4px)");
    // $(".content").css("pointer-events", "none");
    // $(".topBar").css("filter", "blur(4px)");
    // $(".topBar").css("pointer-events", "none");
    // $("#loadingScreen").css("display", "block");
    // $("#loadingSpinner").css("display", "block");
    $(".content").load("main.html", async function () {
        setupGridFirst();
        applyValuesonGrid();
        await setUp();
        addListenersforWidgets();
        setupTopbarOnClick();
        $("#setupPopUp").css("display", "none");
        // $(".content").css("filter", "blur(0px)");
        // $(".topBar").css("filter", "blur(0px)");
    });
    $(".topBar").css("display", "grid");
}

/**
 * Applyes the values of the previously selected grid onto the grid of the index.html page.
 */
function applyValuesonGrid(list) {

    if (arguments.length > 0) {
        selectedGridValues = list;
    }
    // if (!isAllreadyLoaded) {
    // for (let i = 0; i < selectedGridValues.length; i++) {
    //     console.log(selectedGridValues[i]);
    // }
    //selectedGridValues.shift(selectedGridValues[selectedGridValues.length - 1]);
    //console.log(selectedGridValues[0]);
    // var getallGridItems = $(".content").find(".grid-stack-item");
    // console.log(getallGridItems[getallGridItems.length - 1]);
    // var gridall = document.getElementById("gridAll");
    // gridall.insertBefore(getallGridItems[getallGridItems.length - 1], getallGridItems[0]);
    $("#lineGraphs").insertBefore("#3dView");
    var getallGridItems = $(".content").find(".grid-stack-item");
    //getallGridItems.unshift(getallGridItems[getallGridItems.length - 1]);
    //var getallGridItems = document.getElementsByClassName("grid-stack-item");
    for (let i = 0; i < getallGridItems.length; i++) {
        getallGridItems[i].setAttribute("gs-x", selectedGridValues[i].x);
        getallGridItems[i].setAttribute("gs-y", selectedGridValues[i].y);
        getallGridItems[i].setAttribute("gs-w", selectedGridValues[i].w);
        getallGridItems[i].setAttribute("gs-h", selectedGridValues[i].h);
        var copy = getallGridItems[i];
        copy.style.display = "block";
        grid.removeWidget(getallGridItems[i]);
        grid.addWidget(copy);
        sendWindowToCSharp();
        //grid.update(getallGridItems[i], { x: selectedGridValues[i].x, y: selectedGridValues[i].y, w: selectedGridValues[i].w, h: selectedGridValues[i].h });
        // grid.update(getallGridItems[i], selectedGridValues[i].x, selectedGridValues[i].y, selectedGridValues[i].w, selectedGridValues[i].h);
    }





    //updateGridAll();

    // }
}

function sendWindowToCSharp() {
    if (window.vuplex) {
        window.vuplex.postMessage({ type: 'Window', message: "Open" });
    }
}
function sendClose3dToCSharp() {
    if (window.vuplex) {
        //window.vuplex.postMessage({ type: 'CloseWindows', message: "Open" });
    }
}
