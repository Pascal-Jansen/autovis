var isFixed1 = false;
var isFixed2 = false;
var zoomed = false;
var play = false;
var savedTime = 0;
let left;
var playInterval;
var playUnityInterval;
var isHover = false;
var fps = 20;
var speedMultip = 1;
var index = 0;
var cameraNames = ["Free Cam", "Dash Cam", "First Person Cam", "Hood Cam", "Third Person Cam"];
var camindex = 0;
var startTime;
var endTime;
var jsonData;
// d3.json("./Recordings/Demo_Recording_2022-09-03_14-33.json").then(function (data) {
//     jsonData = data;
//     console.log(data);
// });
/**
 * Show the red circle and red highlighting lines when exiting the timeline
 */

//  WebGL build path:    ./WebGL/
function mouseover() {
    pauseTimeline();
    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'Pause');
    }
    let timeDots1 = document.getElementById("lineGraphs").getElementsByClassName("timeDot1");
    let timeDots2 = document.getElementById("lineGraphs").getElementsByClassName("timeDot2");
    let timeDots3 = document.getElementById("lineGraphs").getElementsByClassName("timeDot3");
    if (!zoomed) {
        for (let i = 0; i < timeDots1.length; i++) {
            timeDots1[i].style.display = "block";
        }
        for (let i = 0; i < timeDots2.length; i++) {
            timeDots2[i].style.display = "block";
        }
    }
    for (let i = 0; i < timeDots3.length; i++) {
        timeDots3[i].style.display = "block";
    }

    let timeMarkers = document.getElementsByClassName("timeMarker");
    for (let i = 0; i < timeMarkers.length; i++) {
        timeMarkers[i].style.display = "block";
    }

    let markedRects = document.getElementsByClassName("markedRect");
    if (!zoomed) {
        for (let i = 0; i < markedRects.length; i++) {
            markedRects[i].style.display = "block";
        }
    }
}

function mouseoverTimeView() {
    pauseTimeline();
    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'Pause');
    }

    // savedTime = 1646783527.3804169;
    // let timeDots1 = document.getElementsByClassName("timeDot1");
    // let timeDots2 = document.getElementsByClassName("timeDot2");
    let timeDots3 = d3.select(".timeView").selectAll(".timeDot3");

    if (document.getElementsByClassName("eventlineView")[0] != undefined) {
        let alleventmarkers = document.getElementsByClassName("eventMarker");
        for (let i = 0; i < alleventmarkers.length; i++) {
            alleventmarkers[i].style.display = "block";
        }
    }
    // if (!zoomed) {
    //     for (let i = 0; i < timeDots1.length; i++) {
    //         timeDots1[i].style.display = "block";
    //     }
    //     for (let i = 0; i < timeDots2.length; i++) {
    //         timeDots2[i].style.display = "block";
    //     }
    // }
    // for (let i = 0; i < timeDots3.length; i++) {
    //     timeDots3[i].style.display = "block";
    // }

    timeDots3.style("display", "block");

    let timeMarkers = document.getElementsByClassName("timeMarker");
    for (let i = 0; i < timeMarkers.length; i++) {
        timeMarkers[i].style.display = "block";
    }

    let markedRects = document.getElementsByClassName("markedRect");
    if (!zoomed) {
        for (let i = 0; i < markedRects.length; i++) {
            markedRects[i].style.display = "block";
        }
    }

    var timelineText = document.getElementById("timelineText");
    //timelineText.style.display = "block";
}
/**
 * Hide the red circle and red highlighting lines when exiting the timeline
 */
function mouseout() {
    isHover = false;

    if (play) {
        play = false;
        playVid();
        playUnity();
    }

    let videoFrame = document.getElementById("videoFrame");
    if (videoFrame != null) {
        videoFrame.currentTime = savedTime;
    }

    // if (play) {
    //     pauseTimeline();
    //     playTimeline();
    // }

    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'jumptoFrame', (savedTime + 1662208381.8918169).toString());
    }
    sendTimeStampToCSharp((savedTime + 1662208381.8918169).toString());

    let timeDots = document.getElementsByClassName("timeDot");
    let timeMarkers = document.getElementsByClassName("timeMarker");
    let timeDots2 = document.getElementsByClassName("timeDot2");
    let timeDots3 = document.getElementsByClassName("timeDot3");
    let timeMarkers2 = document.getElementsByClassName("timeMarker2");
    let timeMarkers3 = document.getElementsByClassName("timeMarker3");
    let markedRects = document.getElementsByClassName("markedRect");
    if (document.getElementsByClassName("eventlineView")[0] != undefined) {
        let alleventmarkers = document.getElementsByClassName("eventMarker");
        for (let i = 0; i < alleventmarkers.length; i++) {
            alleventmarkers[i].style.display = "none";
        }
        // document.getElementsByClassName("eventMarker")[0].style.display = "none";
    }
    if (!isFixed1) {
        for (let i = 0; i < timeDots.length; i++) {
            timeDots[i].style.display = "none";
        }
        for (let i = 0; i < timeMarkers.length; i++) {
            timeMarkers[i].style.display = "none";
        }
    } else if (!isFixed2) {
        for (let i = 0; i < timeDots2.length; i++) {
            timeDots2[i].style.display = "none";
        }
        for (let i = 0; i < timeDots3.length; i++) {
            timeDots3[i].style.display = "none";
        }
        for (let i = 0; i < timeMarkers2.length; i++) {
            timeMarkers2[i].style.display = "none";
        }
        for (let i = 0; i < timeMarkers3.length; i++) {
            timeMarkers3[i].style.display = "none";
        }
        for (let i = 0; i < markedRects.length; i++) {
            markedRects[i].style.display = "none";
        }
    } else {
        for (let i = 0; i < timeDots3.length; i++) {
            timeDots3[i].style.display = "none";
        }
        for (let i = 0; i < timeMarkers3.length; i++) {
            timeMarkers3[i].style.display = "none";
        }
    }
    var timelineText = document.getElementById("timelineText");
    //timelineText.style.display = "none";

    if (play) {
        // document.getElementsByClassName("eventMarker")[0].style.display = "block";
        // document.getElementsByClassName("timeView")[0].getElementsByClassName("timeDot3")[0].style.display = "block";
        // let marker2d = document.getElementsByClassName("timeMarker");
        // for (let i = 0; i < marker2d.length; i++) {
        //     marker2d[i].style.display = "block";
        // }
    }
}

function fixBars(e) {

    if (e.shiftKey) {
        if (!isFixed1 && !isFixed2) {
            isFixed1 = true;
            d3.selectAll(".timeDot1").style("display", "block");

        } else if (isFixed1 && !isFixed2) {
            isFixed2 = true;
            d3.selectAll(".timeDot2").style("display", "block");
            // document.getElementById("button7").style.display = "inline";
            setStartandEndTime();
            // enableAddButton();
            savedTime = startTime;

        } else {
            // disableAddButton();
            isFixed1 = false;
            isFixed2 = false;
            d3.selectAll(".timeDot1").style("display", "none");
            d3.selectAll(".timeDot2").style("display", "none");
            // zoomed = false;
            endTime = -1;
            // document.getElementById("button7").style.display = "none";
        }
    } else {
        if (!isHold) {
            let timelineWidth = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;

            // disableAddButton();
            let timelineXScale = d3.scaleLinear()
                .domain([minX, maxX])
                .range([margin.left, timelineWidth]);

            let pos = timelineXScale.invert(e.offsetX);
            if (typeof unityGameInstance !== "undefined") {
                unityGameInstance.SendMessage('ReplayManager', 'jumptoFrame', (pos + 1662208381.8918169).toString());
            }
            sendTimeStampToCSharp((pos + 1662208381.8918169).toString());

            d3.selectAll(".eventMarkerPlay").attr("x1", timelineXScale(pos));
            d3.selectAll(".eventMarkerPlay").attr("x2", timelineXScale(pos));
            // d3.selectAll(".eventMarkerPlay").style("display", "block");

            d3.selectAll(".timeDotPlay").attr("cx", timelineXScale(pos));

            if (!document.getElementsByClassName("iconGraph Graph2d")[0].classList.contains("iconActivated")) {
                let twoDWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width - margin.right - margin.left;

                let twoDXScale = d3.scaleLinear()
                    .domain([minX, maxX])
                    .range([margin.left, twoDWidth]);

                d3.selectAll(".timeMarkerPlay").attr("x1", twoDXScale(pos));
                d3.selectAll(".timeMarkerPlay").attr("x2", twoDXScale(pos));
                // document.getElementById("button7").style.display = "none";
                // d3.selectAll(".timeMarkerPlay").style("display", "block");
            }


            index = nearestValue(timestamps, pos);
            savedTime = pos;
            isFixed1 = false;
            isFixed2 = false;
            // zoomed = false;
            endTime = -1;
            if (document.getElementById("videoFrame") != null) {
                document.getElementById("videoFrame").currentTime = index * 0.05;
            }
        }
        isHold = false;
    }
}

let isHold = false;
let holdInterval;
function mouseState(e) {
    let left = e.offsetX;

    let widthX2d = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width - margin.right - margin.left;
    let xScale2d = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, widthX2d]);

    let widthX = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;
    let xScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, widthX]);

    if (e.type == "mousedown") {
        if (!e.shiftKey) {
            isFixed1 = false;
            isFixed2 = false;
        }
        holdInterval = setTimeout(() => {

            isHold = true;
            let timeDot1 = d3.select(".timeView").selectAll(".timeDot1");
            let timeDot12D = d3.select(".timeline").selectAll(".timeDot1");
            let timeMarker12D = d3.selectAll(".timeMarker1");

            timeDot1.style("display", "block")
                .attr("cx", left);
            timeDot12D.style("display", "block")
                .attr("cx", xScale2d(xScale.invert(left)));
            timeMarker12D.style("display", "block")
                .attr("x1", xScale2d(xScale.invert(left)))
                .attr("x2", xScale2d(xScale.invert(left)));

            if (!e.shiftKey) {
                isFixed1 = true;
                isFixed2 = false;
                posFirstBarPercent = xScale2d(xScale.invert(left)) / (widthX2d + margin.left + margin.right);
            }
        }, 150);
    } else if (e.type == "mouseup") {
        clearInterval(holdInterval);
        if (isHold) {
            let timeDot2 = d3.select(".timeView").selectAll(".timeDot2");
            let timeDot22D = d3.select(".timeline").selectAll(".timeDot2");
            let timeMarker22D = d3.selectAll(".timeMarker2");
            let timeDotPlay = d3.select(".timeView").selectAll(".timeDotPlay");
            let eventMarkerPlay = d3.selectAll(".eventMarkerPlay");
            let timeMarkerPlay = d3.selectAll(".timeMarkerPlay");

            let timeDot1X = d3.select(".timeView").selectAll(".timeDot1").attr("cx");
            if (timeDot1X > left) {
                startTime = xScale.invert(left);
                endTime = xScale.invert(timeDot1X);
                savedTime = startTime;
            } else {
                startTime = xScale.invert(timeDot1X);
                endTime = xScale.invert(left);
                savedTime = startTime;
            }

            timeDot2.style("display", "block")
                .attr("cx", left);
            timeDot22D.style("display", "block")
                .attr("cx", xScale2d(xScale.invert(left)));
            timeMarker22D.style("display", "block")
                .attr("cx", xScale2d(xScale.invert(left)));
            timeDotPlay.style("display", "block")
                .attr("cx", xScale(startTime));
            eventMarkerPlay.style("display", "block")
                .attr("x1", xScale(startTime))
                .attr("x2", xScale(startTime));
            timeMarkerPlay.style("display", "block")
                .attr("x1", xScale2d(startTime))
                .attr("x2", xScale2d(startTime));

            isFixed2 = true;
        }
    }
};

function disableAddButton() {
    $("#addCustomEvent").css("display", "none");
}

function enableAddButton() {
    $("#addCustomEvent").css("display", "block");
}

function displayInputFieldButton() {
    if (isFixed2) {
        $("#inputForCustomEvents").css("display", "block");
        $("#gridAll").css("filter", "blur(4px)");
        $(".topBar").css("filter", "blur(4px)");
        $("#inputForCustomEvents").css("filter", "blur(0px)");
    } else {
        displayWarningPopUp("Select an area first!");
    }
}

function addToCustomEvent() {
    $("#inputForCustomEvents").css("display", "none");
    $("#gridAll").css("filter", "blur(0px)");
    $(".topBar").css("filter", "blur(0px)");
    let name = document.getElementById("customEventText").value;
    startDot = d3.selectAll(".timeView").selectAll(".timeDot1")._groups[0][0];
    endDot = d3.selectAll(".timeView").selectAll(".timeDot2")._groups[0][0];
    drawCustomEventline(name, parseFloat(startDot.getAttribute("cx")), parseFloat(endDot.getAttribute("cx")));
    //$("#customEventDiv").css("display", "block");
}

function toggleCustomEvents() {
    if ($("#customEvents").css("display") == "none") {
        $("#customEvents").css("display", "block");
    }
    else {
        $("#customEvents").css("display", "none");
    }

}
function closeCustomEventInput() {
    $("#inputForCustomEvents").css("display", "none");
    $("#gridAll").css("filter", "blur(0px)");
    $(".topBar").css("filter", "blur(0px)");

}

function nearestValue(array, goal) {
    var closest = array.reduce(function (prev, curr) {
        return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
    });
    return array.indexOf(closest);
}

//TODO
// setTimeout(function () {
//     document.getElementById("button8").addEventListener('click', function (e) {
//         isFixed1 = false;
//         isFixed2 = false;
//     })
// }, 500);

// setTimeout(function () {
//     document.getElementById("button7").addEventListener('click', function (e) {
//         zoomed = true;
//     })
// }, 500);


window.onload = function () {
    const checkbox2 = document.getElementById('layout')

    checkbox2.addEventListener('change', (event) => {
        isFixed1 = false;
        isFixed2 = false;
    })
}


var posFirstBar;
var posSecondBar;
// Adds an event to position the red circle and all red lines if the mouse moves over the timeline

function addMouseListenerTimeline() {
    const ele = document.getElementsByClassName("timeline");
    for (let elementCounter = 0; elementCounter < ele.length; elementCounter++) {

        ele[elementCounter].addEventListener('mousemove', function (e) {
            let left = e.offsetX;
            let top = e.offsetY;
            let left2 = e.pageX;
            let top2 = e.pageY;
            let width;
            let rightbound;
            let timeMarkers = document.getElementsByClassName("timeMarker3");
            let timeDots = document.getElementsByClassName("timeDot3");
            // let posFirstBarPercent;
            // let posSecondBarPercent;

            if (document.getElementsByClassName("demoGraphDiv").length > 0 && document.getElementsByClassName("timeline").length > 0) {
                if (!document.getElementById("layout").checked) {
                    width = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width;
                    rightbound = document.getElementsByClassName("timeline")[0].getBoundingClientRect().width - margin.right - margin.left;

                    // check if mous is on the timeline
                    if (left > 29 && left < rightbound) {
                        // position red circle
                        for (let i = 0; i < timeDots.length; i++) {
                            timeDots[i].setAttribute("cx", left);
                        }
                        // position all red lines
                        for (let i = 0; i < timeMarkers.length; i++) {
                            timeMarkers[i].setAttribute("x1", left);
                            timeMarkers[i].setAttribute("x2", left);
                        };
                    }

                    if (!isFixed1) {
                        timeDots = document.getElementsByClassName("timeDot1");
                        timeMarkers = document.getElementsByClassName("timeMarker1");

                        // check if mous is on the timeline
                        if (left > 29 && left < rightbound) {
                            posFirstBarPercent = left / width;
                            // position red circle
                            for (let i = 0; i < timeDots.length; i++) {
                                timeDots[i].setAttribute("cx", left);
                                timeDots[i].setAttribute("data-graph-width", width);
                            }
                            // position all red lines
                            for (let i = 0; i < timeMarkers.length; i++) {
                                timeMarkers[i].setAttribute("x1", left);
                                timeMarkers[i].setAttribute("x2", left);
                                timeMarkers[i].setAttribute("data-graph-width", width);
                            };
                        }
                    }

                    if (!isFixed2) {
                        timeDots = document.getElementsByClassName("timeDot2");
                        timeMarkers = document.getElementsByClassName("timeMarker2");

                        // check if mous is on the timeline
                        if (left > 29 && left < rightbound) {
                            posSecondBarPercent = left / width;
                            // position red circle
                            for (let i = 0; i < timeDots.length; i++) {
                                timeDots[i].setAttribute("cx", left);
                                timeDots[i].setAttribute("data-graph-width", width);
                            }
                            // position all red lines
                            for (let i = 0; i < timeMarkers.length; i++) {
                                timeMarkers[i].setAttribute("x1", left);
                                timeMarkers[i].setAttribute("x2", left);
                                timeMarkers[i].setAttribute("data-graph-width", width);
                            };
                        }
                        let rects = document.getElementsByClassName("markedRect");
                        posFirstBar = posFirstBarPercent * width;
                        posSecondBar = posSecondBarPercent * width;
                        for (let i = 0; i < rects.length; i++) {
                            rects[i].setAttribute("width", Math.abs(posSecondBar - posFirstBar));
                            if (posSecondBar > posFirstBar) {
                                rects[i].setAttribute("x", posFirstBar);
                            } else {
                                rects[i].setAttribute("x", posSecondBar);
                            }
                        }
                    }
                } else {
                    width = document.getElementsByClassName("demoGraphDivVertical")[0].getBoundingClientRect().height;
                    rightbound = document.getElementsByClassName("timelineVertical")[0].getBoundingClientRect().height - 25;
                    // check if mous is on the timeline
                    if (top > 29 && top < rightbound) {
                        // position red circle
                        for (let i = 0; i < timeDots.length; i++) {
                            timeDots[i].setAttribute("cy", top);
                        }
                        // position all red lines
                        for (let i = 0; i < timeMarkers.length; i++) {
                            timeMarkers[i].setAttribute("y1", top);
                            timeMarkers[i].setAttribute("y2", top);
                            timeMarkers[i].setAttribute("x1", 0)
                            timeMarkers[i].setAttribute("x2", 200)
                        };
                    }

                    if (!isFixed1) {
                        timeDots = document.getElementsByClassName("timeDot1");
                        timeMarkers = document.getElementsByClassName("timeMarker1");

                        // check if mous is on the timeline
                        if (top > 29 && top < rightbound) {
                            posFirstBarPercent = top / width;
                            // position red circle
                            for (let i = 0; i < timeDots.length; i++) {
                                timeDots[i].setAttribute("cy", top);
                                // timeDots[i].setAttribute("data-graph-width", width);
                            }
                            // position all red lines
                            for (let i = 0; i < timeMarkers.length; i++) {
                                timeMarkers[i].setAttribute("y1", top);
                                timeMarkers[i].setAttribute("y2", top);
                                timeMarkers[i].setAttribute("x1", 0)
                                timeMarkers[i].setAttribute("x2", 200)
                                // timeMarkers[i].setAttribute("data-graph-width", width);
                            };
                        }
                    }

                    if (!isFixed2) {
                        timeDots = document.getElementsByClassName("timeDot2");
                        timeMarkers = document.getElementsByClassName("timeMarker2");

                        // check if mous is on the timeline
                        if (top > 29 && top < rightbound) {
                            posSecondBarPercent = top / width;
                            // position red circle
                            for (let i = 0; i < timeDots.length; i++) {
                                timeDots[i].setAttribute("cy", top);
                                // timeDots[i].setAttribute("data-graph-width", width);
                            }
                            // position all red lines
                            for (let i = 0; i < timeMarkers.length; i++) {
                                timeMarkers[i].setAttribute("y1", top);
                                timeMarkers[i].setAttribute("y2", top);
                                timeMarkers[i].setAttribute("x1", 0)
                                timeMarkers[i].setAttribute("x2", 200)
                                // timeMarkers[i].setAttribute("data-graph-width", width);
                            };
                        }

                        let rects = document.getElementsByClassName("markedRectVertical");
                        posFirstBar = posFirstBarPercent * width;
                        posSecondBar = posSecondBarPercent * width;
                        for (let i = 0; i < rects.length; i++) {
                            rects[i].setAttribute("height", Math.abs(posSecondBar - posFirstBar));
                            if (posSecondBar > posFirstBar) {
                                rects[i].setAttribute("y", posFirstBar);
                            } else {
                                rects[i].setAttribute("y", posSecondBar);
                            }
                        }
                    }
                }
                let eventText = document.getElementById("eventText");
                if (document.getElementsByClassName("eventline")[0].parentElement.getAttribute("id") == "eventlineContainerVertical") {
                    eventText.style.top = (top2 - 32) + "px"
                } else {
                    // eventText.style.bottom = "55px"
                }
                // eventText.style.left = (left2 - 50) + "px"
            }

        });
    }
}

// function pressed() {
//     console.log("pressed");
// }

// function released() {
//     console.log("released");
// }

function addMouseListenerTimelineView() {
    const ele = document.getElementsByClassName("timeView");

    ele[0].addEventListener('mousemove', function (e) {
        isHover = true;

        left = e.offsetX;
        let top = e.offsetY;
        let left2 = e.pageX;
        let top2 = e.pageY;
        let width;
        let rightbound;
        let timeMarkers = document.getElementsByClassName("timeMarker3");
        let eventMarker = document.getElementsByClassName("eventMarker")[0];

        let alleventmarkers = d3.selectAll(".eventMarker");


        let timeDots = d3.select(".timeView").selectAll(".timeDot3");
        // let posFirstBarPercent;
        // let posSecondBarPercent;


        if (document.getElementsByClassName("timeView").length > 0) {
            timeDots.attr("cx", left);

            if (alleventmarkers.size() > 0) {
                alleventmarkers.attr("x1", left)
                    .attr("x2", left)
                d3.select(".eventMarker").raise();
            }
        }

        if (document.getElementsByClassName("demoGraphDiv").length > 0 && document.getElementsByClassName("timeView").length > 0) {
            let newWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width - margin.right - margin.left;
            let oldWidth = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;

            let newXScale = d3.scaleLinear()
                .domain([minX, maxX])
                .range([margin.left, newWidth]);

            let oldXScale = d3.scaleLinear()
                .domain([minX, maxX])
                .range([margin.left, oldWidth]);



            let pos = oldXScale.invert(left);
            let newXPos = newXScale(pos);

            videoFrame = document.getElementById("videoFrame");

            if (videoFrame != null) {
                videoFrame.currentTime = pos;
            }

            if (typeof unityGameInstance !== "undefined") {
                unityGameInstance.SendMessage('ReplayManager', 'jumptoFrame', (pos + 1662208381.8918169).toString());
            }
            sendTimeStampToCSharp((pos + 1662208381.8918169).toString());
            var timelineText = document.getElementById("timelineText");
            var newTime = calculateTime(oldXScale.invert(left));
            timelineText.innerHTML = newTime;
            // timelineText.style.left = (left - 35) + "px";


            // if (!document.getElementById("layout").checked) {
            width = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width;
            rightbound = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;

            // check if mous is on the timeline
            if (left > 29 && left < rightbound) {
                // position red circle
                for (let i = 0; i < timeDots.length; i++) {
                    if ($(timeDots[i].parentElement).hasClass("timeline")) {
                        timeDots[i].setAttribute("cx", newXPos);
                    } else {
                        // timeDots[i].setAttribute("cx", left);
                    }
                    //
                }
                // position all red lines
                for (let i = 0; i < timeMarkers.length; i++) {
                    timeMarkers[i].setAttribute("x1", newXPos);
                    timeMarkers[i].setAttribute("x2", newXPos);
                };


            }


            if (!isFixed1) {
                timeDots = document.getElementsByClassName("timeDot1");
                timeMarkers = document.getElementsByClassName("timeMarker1");

                // check if mous is on the timeline
                if (left > 29 && left < rightbound) {
                    posFirstBarPercent = newXPos / width;
                    // position red circle
                    for (let i = 0; i < timeDots.length; i++) {
                        if ($(timeDots[i].parentElement).hasClass("timeline")) {
                            timeDots[i].setAttribute("cx", newXPos);
                        } else {
                            timeDots[i].setAttribute("cx", left);
                        }
                        timeDots[i].setAttribute("data-graph-width", width);
                    }
                    // position all red lines
                    for (let i = 0; i < timeMarkers.length; i++) {
                        timeMarkers[i].setAttribute("x1", newXPos);
                        timeMarkers[i].setAttribute("x2", newXPos);
                        timeMarkers[i].setAttribute("data-graph-width", width);
                    };
                }
            }

            if (!isFixed2) {
                timeDots = document.getElementsByClassName("timeDot2");
                timeMarkers = document.getElementsByClassName("timeMarker2");

                // check if mous is on the timeline
                if (left > 29 && left < rightbound) {
                    posSecondBarPercent = newXPos / width;
                    // position red circle
                    for (let i = 0; i < timeDots.length; i++) {
                        if ($(timeDots[i].parentElement).hasClass("timeline")) {
                            timeDots[i].setAttribute("cx", newXPos);
                        } else {
                            timeDots[i].setAttribute("cx", left);
                        }
                        timeDots[i].setAttribute("data-graph-width", width);
                    }
                    // position all red lines
                    for (let i = 0; i < timeMarkers.length; i++) {
                        timeMarkers[i].setAttribute("x1", newXPos);
                        timeMarkers[i].setAttribute("x2", newXPos);
                        timeMarkers[i].setAttribute("data-graph-width", width);
                    };
                }
                let rects = document.getElementsByClassName("markedRect");
                posFirstBar = posFirstBarPercent * width;
                posSecondBar = posSecondBarPercent * width;
                for (let i = 0; i < rects.length; i++) {
                    rects[i].setAttribute("width", Math.abs(posSecondBar - posFirstBar));
                    if (posSecondBar > posFirstBar) {
                        rects[i].setAttribute("x", posFirstBar);
                    } else {
                        rects[i].setAttribute("x", posSecondBar);
                    }
                }
            }
            // }
            // } else {
            //     width = document.getElementsByClassName("demoGraphDivVertical")[0].getBoundingClientRect().height;
            //     rightbound = document.getElementsByClassName("timelineVertical")[0].getBoundingClientRect().height - 25;
            //     // check if mous is on the timeline
            //     if (top > 29 && top < rightbound) {
            //         // position red circle
            //         for (let i = 0; i < timeDots.length; i++) {
            //             timeDots[i].setAttribute("cy", top);
            //         }
            //         // position all red lines
            //         for (let i = 0; i < timeMarkers.length; i++) {
            //             timeMarkers[i].setAttribute("y1", top);
            //             timeMarkers[i].setAttribute("y2", top);
            //             timeMarkers[i].setAttribute("x1", 0)
            //             timeMarkers[i].setAttribute("x2", 200)
            //         };
            //     }

            //     if (!isFixed1) {
            //         timeDots = document.getElementsByClassName("timeDot1");
            //         timeMarkers = document.getElementsByClassName("timeMarker1");

            //         // check if mous is on the timeline
            //         if (top > 29 && top < rightbound) {
            //             posFirstBarPercent = top / width;
            //             // position red circle
            //             for (let i = 0; i < timeDots.length; i++) {
            //                 timeDots[i].setAttribute("cy", top);
            //                 // timeDots[i].setAttribute("data-graph-width", width);
            //             }
            //             // position all red lines
            //             for (let i = 0; i < timeMarkers.length; i++) {
            //                 timeMarkers[i].setAttribute("y1", top);
            //                 timeMarkers[i].setAttribute("y2", top);
            //                 timeMarkers[i].setAttribute("x1", 0)
            //                 timeMarkers[i].setAttribute("x2", 200)
            //                 // timeMarkers[i].setAttribute("data-graph-width", width);
            //             };
            //         }
            //     }

            //     if (!isFixed2) {
            //         timeDots = document.getElementsByClassName("timeDot2");
            //         timeMarkers = document.getElementsByClassName("timeMarker2");

            //         // check if mous is on the timeline
            //         if (top > 29 && top < rightbound) {
            //             posSecondBarPercent = top / width;
            //             // position red circle
            //             for (let i = 0; i < timeDots.length; i++) {
            //                 timeDots[i].setAttribute("cy", top);
            //                 // timeDots[i].setAttribute("data-graph-width", width);
            //             }
            //             // position all red lines
            //             for (let i = 0; i < timeMarkers.length; i++) {
            //                 timeMarkers[i].setAttribute("y1", top);
            //                 timeMarkers[i].setAttribute("y2", top);
            //                 timeMarkers[i].setAttribute("x1", 0)
            //                 timeMarkers[i].setAttribute("x2", 200)
            //                 // timeMarkers[i].setAttribute("data-graph-width", width);
            //             };
            //         }

            //         let rects = document.getElementsByClassName("markedRectVertical");
            //         posFirstBar = posFirstBarPercent * width;
            //         posSecondBar = posSecondBarPercent * width;
            //         for (let i = 0; i < rects.length; i++) {
            //             rects[i].setAttribute("height", Math.abs(posSecondBar - posFirstBar));
            //             if (posSecondBar > posFirstBar) {
            //                 rects[i].setAttribute("y", posFirstBar);
            //             } else {
            //                 rects[i].setAttribute("y", posSecondBar);
            //             }
            //         }
            //     }
            // }
            let eventText = document.getElementById("eventText");
            if (document.getElementsByClassName("eventline")[0].parentElement.getAttribute("id") == "eventlineContainerVertical") {
                eventText.style.top = (top2 - 32) + "px"
            } else {
                // eventText.style.bottom = "55px"
            }
            // eventText.style.left = (left2 - 50) + "px"
        }

    });
}


function showEventText(thisElement) {
    let eventText = document.getElementById("eventText");
    eventText.style.display = "block";
    eventText.innerHTML = thisElement.className.baseVal;
}

function hideEventText(thisElement) {
    let eventText = document.getElementById("eventText");
    eventText.style.display = "none";
}

function calculateTime(time) {
    let minXnew = oldMin;

    // var date_beginning = new Date(minXnew * 1000);
    // // Hours part from the timestamp
    // var hours_beginning = date_beginning.getHours();
    // // Minutes part from the timestamp
    // var minutes_beginning = "0" + date_beginning.getMinutes();
    // // Seconds part from the timestamp
    // var seconds_beginning = "0" + date_beginning.getSeconds();

    // let unix_timestamp = time;
    // Create a new JavaScript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(time * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    var dateoffset = new Date();
    var date_difference = new Date((time - minXnew) * 1000);

    //date_difference = date_difference.getTimezoneOffset();
    var hours_difference = date_difference.getUTCHours();
    // Minutes part from the timestamp
    var minutes_difference = "0" + date_difference.getUTCMinutes();
    // Seconds part from the timestamp
    var seconds_difference = "0" + date_difference.getUTCSeconds();


    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

    var formattedTime_difference = hours_difference + ':' + minutes_difference.substr(-2) + ':' + seconds_difference.substr(-2);

    if (timeset) {
        return formattedTime_difference;
    }
    else {
        return formattedTime;
    }
}
// export { isFixed1, isFixed2 };

function playUnity(el) {
    if (arguments.length > 0) {

        el.classList.toggle("pauseUnity");

    }
    if (!play) {
        playTimeline();
        if (typeof unityGameInstance !== "undefined") {
            // unityGameInstance.SendMessage('ReplayManager', 'Play', 1);
        }
        play = true;
    }
    else {
        //unityGameInstance.SendMessage('ReplayManager', 'resetFastForward');
        pauseTimeline();
        if (typeof unityGameInstance !== "undefined") {
            // unityGameInstance.SendMessage('ReplayManager', 'Pause');
        }
        play = false;
    }
}

function fastforwardUnity() {
    speedMultip = 2;

    if (!play) {
        $(".playUnity")[0].classList.toggle("pauseUnity");
    }
    clearInterval(playInterval);
    clearInterval(playUnityInterval);
    playTimeline();
    play = true;
    if (typeof unityGameInstance !== "undefined") {
        //unityGameInstance.SendMessage('ScenePlayer', 'Play');
        //unityGameInstance.SendMessage('ReplayManager', 'Play', 2);
    }

}

function setPlayMarker(timeStamp) {
    let timelineWidth = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;
    let timelineXScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, timelineWidth]);

    d3.selectAll(".eventMarkerPlay").attr("x1", timelineXScale(timeStamp));
    d3.selectAll(".eventMarkerPlay").attr("x2", timelineXScale(timeStamp));
    d3.selectAll(".timeDotPlay").attr("cx", timelineXScale(timeStamp));

    if (!document.getElementsByClassName("iconGraph Graph2d")[0].classList.contains("iconActivated")) {
        let twoDWidth = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width - margin.right - margin.left;
        let twoDXScale = d3.scaleLinear()
            .domain([minX, maxX])
            .range([margin.left, twoDWidth]);

        d3.selectAll(".timeMarkerPlay").attr("x1", twoDXScale(timeStamp));
        d3.selectAll(".timeMarkerPlay").attr("x2", twoDXScale(timeStamp));
    }
}

function nextFrameUnity() {
    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'SkipFrames', 1);
    }
    videoFrame = document.getElementById("videoFrame")
    if (videoFrame != null) {
        if (videoFrame.currentTime < videoFrame.duration) {
            videoFrame.currentTime = videoFrame.currentTime + 0.05;
        }
    }

    let timelineWidth = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;

    let timelineXScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, timelineWidth]);

    newX = timelineXScale.invert(document.getElementsByClassName("eventMarkerPlay")[0].getAttribute("x1")) + 0.05;

    setPlayMarker(newX);
}

function prevFrameUnity() {
    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'SkipFrames', -1);
    }
    if (videoFrame != null) {
        if (videoFrame.currentTime > 0) {
            videoFrame.currentTime = videoFrame.currentTime - 0.05;
        }
    }

    let timelineWidth = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;

    let timelineXScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, timelineWidth]);

    newX = timelineXScale.invert(document.getElementsByClassName("eventMarkerPlay")[0].getAttribute("x1")) - 0.05;

    setPlayMarker(newX);
}

function nextCamUnity() {
    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'SwitchToCamera', 1);
        camindex++;
        camindex = camindex % cameraNames.length;
        document.getElementById("currentCamUnityText").innerHTML = cameraNames[camindex];
    }
    if (window.vuplex) {
        sendMessageToCSharp(1);
    } else {
        window.addEventListener('vuplexready', sendMessageToCSharp);
    }
}
function prevCamUnity() {
    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'SwitchToCamera', -1);
        camindex--;
        if (camindex < 0) {
            camindex = cameraNames.length - 1;
        }
        document.getElementById("currentCamUnityText").innerHTML = cameraNames[camindex];
    }
    if (window.vuplex) {
        sendMessageToCSharp(-1);
    } else {
        window.addEventListener('vuplexready', sendMessageToCSharp);
    }
}
function setCurrentCamText() {

}

function sendMessageToCSharp(cameraNumber) {
    // This object passed to postMessage() automatically gets serialized as JSON
    // and is emitted via the C# MessageEmitted event. This API mimics the window.postMessage API.
    window.vuplex.postMessage({ type: 'Camera', message: cameraNumber });
}

var timestamps = [];

function fillTimestamps() {
    let currentTime = minX;
    while (currentTime < maxX) {
        timestamps.push(currentTime);
        currentTime += 0.05;
    }
}

function playTimeline() {

    // unityGameInstance.SendMessage('ReplayManager', 'readWholeJson', "" + JSON.stringify(jsonData));
    let diff = maxX - minX;

    if (!document.getElementsByClassName("iconGraph Graph2d")[0].classList.contains("iconActivated")) {
        let widthX2d = document.getElementsByClassName("demoGraphDiv")[0].getBoundingClientRect().width - margin.right - margin.left;

        let xScale2d = d3.scaleLinear()
            .domain([minX, maxX])
            .range([margin.left, widthX2d]);

        let left2d = xScale2d(minX);
        let right2d = xScale2d(maxX);
        let length2d = right2d - left2d;

        if (savedTime != 0) {
            left2d = xScale2d(savedTime);
        }

        var marker2d = d3.selectAll(".timeMarkerPlay");

        marker2d.attr("x1", left2d)
            .attr("x2", left2d)
            .style("display", "block");

        var perSecond2d = length2d / diff;

    }

    let widthX = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;

    let xScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, widthX]);

    let left = xScale(minX);
    let right = xScale(maxX);
    let length = right - left;

    if (savedTime != 0) {
        left = xScale(savedTime);
    }

    let marker = d3.selectAll(".eventMarkerPlay");
    let dot = document.getElementsByClassName("timeView")[0].getElementsByClassName("timeDotPlay")[0];

    marker.attr("x1", left)
        .attr("x2", left)
        .style("display", "block");

    dot.setAttribute("cx", left);
    dot.style.display = "block";
    // for (let i = 0; i < marker2d.length; i++) {
    //     marker2d[i].setAttribute("x1", left2d);
    //     marker2d[i].setAttribute("x2", left2d);
    //     marker2d[i].style.display = "block";
    // }

    let perSecond = length / diff;



    // marker.style.display = "block";
    dot.style.display = "block";
    // marker2d[2].style.display = "block";
    // for (let i = 0; i < marker2d.length; i++) {
    //     marker2d[i].style.display = "block";
    // }

    let videoFrame = document.getElementById("videoFrame");
    // videoFrame.play();
    playVid();

    playInterval = setInterval(function () {
        let newX2d;
        if (!document.getElementsByClassName("iconGraph Graph2d")[0].classList.contains("iconActivated")) {
            let newPos2d = perSecond2d / fps;
            newX2d = parseFloat(marker2d.attr("x1")) + newPos2d;

            if (endTime != -1 && timestamps[index] > endTime) {
                newX2d = Math.min(parseFloat(d3.selectAll(".timeline").selectAll(".timeDot1")._groups[0][0].getAttribute("cx")), parseFloat(d3.selectAll(".timeline").selectAll(".timeDot2")._groups[0][0].getAttribute("cx")));
            }

            marker2d
                .attr("x1", newX2d)
                .attr("x2", newX2d);
        }

        let newPos = perSecond / fps;
        let newX;

        newX = parseFloat(marker.attr("x1")) + newPos;

        if (endTime != -1 && timestamps[index] > endTime) {
            newX = Math.min(parseFloat(d3.selectAll(".timeView").selectAll(".timeDot1")._groups[0][0].getAttribute("cx")), parseFloat(d3.selectAll(".timeView").selectAll(".timeDot2")._groups[0][0].getAttribute("cx")));
            if (videoFrame != null) {
                videoFrame.currentTime = startTime;
            }
        }
        marker.attr("x1", newX);
        marker.attr("x2", newX);

        dot.setAttribute("cx", newX);
        // for (let i = 0; i < marker2d.length; i++) {
        //     marker2d[i].setAttribute("x1", newX2d);
        //     marker2d[i].setAttribute("x2", newX2d);
        // }


        if (!isHover) {
            var newTime = calculateTime(xScale.invert(newX));
            timelineText.innerHTML = newTime;
            savedTime = xScale.invert(newX);

        }
    }, 50 / speedMultip);

    playUnityInterval = setInterval(function () {
        if (endTime != -1 && timestamps[index] > endTime) {
            index = nearestValue(timestamps, startTime);
        }
        if (typeof unityGameInstance !== "undefined") {
            let x = (timestamps[index] + 1662208381.8918169);
            unityGameInstance.SendMessage('ReplayManager', 'jumptoFrame', "" + x);
            // sendData(index);
        }
        let x = (timestamps[index] + 1662208381.8918169);
        sendTimeStampToCSharp(x);
        index++;
    }, 50 / speedMultip);
}

function sendTimeStampToCSharp(timestamp) {
    console.log(timestamp);
    // This object passed to postMessage() automatically gets serialized as JSON
    // and is emitted via the C# MessageEmitted event. This API mimics the window.postMessage API.
    if (window.vuplex) {
        window.vuplex.postMessage({ type: 'Timestamp', message: timestamp });
    }
}

function pauseTimeline() {
    let videoFrame = document.getElementById("videoFrame");
    pauseVid()
    clearInterval(playInterval);
    clearInterval(playUnityInterval);
    speedMultip = 1;
}

function setStartandEndTime() {
    startDot = d3.selectAll(".timeView").selectAll(".timeDot1")._groups[0][0];
    endDot = d3.selectAll(".timeView").selectAll(".timeDot2")._groups[0][0];
    let oldWidth = document.getElementsByClassName("timeView")[0].getBoundingClientRect().width - margin.right - margin.left;
    let oldXScale = d3.scaleLinear()
        .domain([minX, maxX])
        .range([margin.left, oldWidth]);
    startTime = Math.min(oldXScale.invert(startDot.getAttribute("cx")), oldXScale.invert(endDot.getAttribute("cx")));
    endTime = Math.max(oldXScale.invert(startDot.getAttribute("cx")), oldXScale.invert(endDot.getAttribute("cx")));
    index = nearestValue(timestamps, startTime);
}

function sendData(index) {
    snapshot = JSON.stringify(jsonData.snapshots[index]);
    //unityGameInstance.SendMessage('ReplayManager', 'handleTimeStampString', "" + snapshot);
    //unityGameInstance.SendMessage('ReplayManager', 'handleTimeStampString', "" + JSON.stringify(jsonData));
}


// Initializing values
var isPlaying = false;

// Play video function
async function playVid() {
    let videoFrame = document.getElementById("videoFrame");
    if (videoFrame != null) {
        videoFrame.playbackRate = speedMultip;
        if (videoFrame.paused && !isPlaying) {
            return videoFrame.play();
        }
    }
}

// Pause video function
function pauseVid() {
    let videoFrame = document.getElementById("videoFrame");
    if (videoFrame != null) {
        if (!videoFrame.paused && isPlaying) {
            videoFrame.pause();
        }
    }
}

// On video playing toggle values
function onPlay() {
    isPlaying = true;
};

// On video pause toggle values
function onPaused() {
    isPlaying = false;
};