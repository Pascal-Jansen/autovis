function showEventText(thisElement) {
    let eventText = document.getElementById("eventText");
    eventText.style.display = "block";
    eventText.innerHTML = thisElement.className.baseVal;
}

function hideEventText(thisElement) {
    let eventText = document.getElementById("eventText");
    eventText.style.display = "none";
}

document.addEventListener('mousemove', function (e) {
    let left2 = e.pageX;
    let top2 = e.pageY;
    let eventText = document.getElementById("eventText");
    eventText.style.left = (left2 - 50) + "px"
    eventText.style.top = (top2 - 32) + "px"
});