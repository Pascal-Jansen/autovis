

var keysforJson = ["id", "name", "type", "data"]
var counterforMetaDataList = 1;
var counterforRecursive = 0;
function updateMetadata(s) {
    counterforRecursive = 0;
    let json = JSON.parse(s);
    let metaContainer = parent.d3.select(".metaContainer");
    metaContainer.selectAll("div").remove();
    addOptionToMetaData(json);
    parent.metaDataList.push(json);
    parent.metaDataListId.push(json.id);
    addMetaObject(json);
}

var listAll = [];

var typeList = [];


function addMetaObject(obj) {
    let metaContainer = parent.d3.select(".metaContainer");
    for (let i = 0; i < Object.keys(obj).length; i++) {
        if (typeof obj[Object.keys(obj)[i]] == "object" || Object.keys(obj)[i] == "data") {

            let dataObject;
            if (Object.keys(obj)[i] == "data") {
                dataObject = JSON.parse(obj.data);
            } else {
                dataObject = obj[Object.keys(obj)[i]];
            }

            metaContainer.append("div")
                .attr("class", "metatextLabel boldText underline")
                .style("margin-left", 10 * counterforRecursive + "px")
                .html(Object.keys(obj)[i].replace(/_/gi, " "));

            counterforRecursive++;
            metaContainer.append("div")
                .attr("class", "metatext")
                .html("");

            addMetaObject(dataObject);

        } else {

            metaContainer.append("div")
                .attr("class", "metatextLabel boldText")
                .style("margin-left", 10 * counterforRecursive + "px")
                .html(Object.keys(obj)[i].replace(/_/gi, " "));

            metaContainer.append("div")
                .attr("class", "metatext")
                .html(function () {
                    if (typeof obj[Object.keys(obj)[i]] == "number") {
                        return obj[Object.keys(obj)[i]].toFixed(0);
                    } else {
                        return JSON.stringify(obj[Object.keys(obj)[i]]);
                    }
                });
        }
    }
    counterforRecursive--;
}

function addOptionToMetaData(json) {
    // if (!metaDataListId.includes(json.id)) {
    parent.d3.select("#optionsMeta").append("option")
        .attr("value", counterforMetaDataList)
        .attr("selected", "selected")
        .html(json.name);
    counterforMetaDataList++;
    // }
}

function handleModels(str) {
    let newJson = JSON.parse(str);

    for (let i = 0; i < newJson.length; i++) {
        parent.d3.select(".listOfObjects").append("div")
            .attr("class", "listObjectItem")
            .attr("id", newJson[i])
            // .style("display", "none")
            .style("border", "2px solid var(--font)")
            // .on("click", onclickforOutline(newJson[i]))
            .html(newJson[i].split("_")[2]);
    }

    for (let j = 0; j < newJson.length; j++) {
        if (!typeList.includes(newJson[j].split("_")[1])) {
            eval("var " + newJson[j].split("_")[1] + "list = []");
            typeList.push(newJson[j].split("_")[1]);

            listAll.push(eval(newJson[j].split("_")[1] + "list"));
        }

        for (let i = 0; i < typeList.length; i++) {
            if (typeList[i] == newJson[j].split("_")[1]) {
                listAll[i].push(newJson[j]);
            }
        };
    }

    updateAll3d();
}

function updateAll3d() {
    let options = parent.d3.select("#options3d");
    for (let i = 0; i < typeList.length; i++) {
        options.append("option")
            .attr("value", typeList[i])
            .html(typeList[i]);
    }

    parent.d3.selectAll(".listObjectItem").remove();

    let listOfObjects = parent.d3.select(".listOfObjects");

    for (let i = 0; i < typeList.length; i++) {
        listOfObjects.append("div")
            .attr("id", typeList[i] + "_group")
            .attr("class", "listObjectType")
            .on("click", function (e) {
                if (e.target.classList.contains("listObjectType")) {
                    openType(this);
                }
            })
            .attr("data-activated", false)
            .style("border", "1px solid green")
            .attr("data_type", i)
            .html(typeList[i]);
    }

}

function openType(t) {
    let index = t.getAttribute("data_type");
    if (t.getAttribute("data-activated") == "true") {
        parent.d3.selectAll("." + typeList[index] + "_item").remove();
        t.setAttribute("data-activated", false);
    } else {
        for (let i = 0; i < listAll[index].length; i++) {
            parent.d3.select("#" + typeList[index] + "_group").append("div")
                .attr("class", "listObjectItem " + typeList[index] + "_item")
                .attr("data-key", listAll[index][i])
                .style("border", "2px solid var(--font)")
                .style("margin-left", "20px")
                .style("margin-right", "20px")
                .on("click", function (e) {
                    if (e.target.classList.contains("listObjectItem")) {
                        onclickforOutline(this);
                    }
                })
                .html(listAll[index][i].split("_")[2]);
        }

        t.setAttribute("data-activated", true);
    }
}

function onclickforOutline(ele) {
    let key = ele.getAttribute("data-key");
    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'HighlightModel', key.toString());
    }
}

function outLineAllTypeObjects() {
    options3d = document.getElementById("options3d").value;
    if (typeof unityGameInstance !== "undefined") {
        unityGameInstance.SendMessage('ReplayManager', 'ToggleHighlightModelType', options3d.toString());
    }
}

function doneLoading() {
    parent.$(".content").css("filter", "blur(0px)");
    parent.$(".content").css("pointer-events", "auto");
    parent.$(".topBar").css("filter", "blur(0px)");
    parent.$(".topBar").css("pointer-events", "auto");
    parent.$("#loadingScreen").css("display", "none");
    // parent.$("#loadingSpinner").css("display", "none");
}

var progressCounter = 0;
var progressArrText = ["Loading First Participant", "Loading Scond Participant", "Loading Third Participant", "Initialize Merged Avatar"];

function progressLoadingBar() {
    console.log("test");
    if (progressCounter < 4) {
        progressCounter++;
        parent.$("#loadingBar").css("width", progressCounter * 60);
        parent.$("#loadingText").html(progressArrText[progressCounter]);

    }


}

