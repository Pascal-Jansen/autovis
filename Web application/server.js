// const http = require("http");
const host = 'localhost';
const port = 8000;
const fs = require('fs');
const express = require('express');
const server = express();
const path = require('path');
const cors = require('cors');

server.use(express.static(__dirname));
server.use(cors());
server.use(express.json());

server.get("/", function (request, response) {
    response.sendFile(path.join(__dirname, '/index.html'));
});

// server.get("/VR.json", function (request, response) {
//     response.sendFile(path.join(__dirname, '/VR.json'));
// });

// server.post("/", function (request, response) {
//     fs.readFile('VR.json', 'utf8', function readFileCallback(err, data) {
//         if (err) {
//             throw err;
//         } else {
//             const currentInfo = JSON.parse(data);
//             const newInfo = request.body;

//             const keys = Object.keys(newInfo);
//             keys.forEach(key => {
//                 currentInfo[key] = newInfo[key];
//             });

//             const json = JSON.stringify(currentInfo);
//             fs.writeFile('VR.json', json, 'utf8', () => { });
//         }
//     });
//     response.sendStatus(200);
// });

server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});